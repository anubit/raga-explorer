# Raga Explorer: 1.0 #

An Android app that enables Indian Classical Music learners to explore different ragas (scales). Currently supported pitch: A, C, D, E, F, G. Tempo speed: 1.0.

* Integrated SoundTouch open source library for audio processing: pitch & tempo shifting. 

* Interfaced with Java wrapper for native library (using NDK)

* Implemented raga database using SQLite library

* Interpreted JSON document format using Gson library

* Implemented other graphical elements such as a piano display to aid visualization

### * Summary of set up: ###
  - Download Android NDK, works on Android 5.1 and above.
  - Tested on GenyMotion 5.1, 6.0 
 
### * Dependencies: ###
 Android NDK, SoundTouch open source project, SQLite

# Supported Android Versions #
Android 5.1 and above
### Thanks ###
My sincere thanks to Steven Myers for his Java wrapper class for Sound Touch.
https://github.com/svenoaks

![ragaapp_gif1.gif](https://bitbucket.org/repo/oKebRA/images/3071816325-ragaapp_gif1.gif)