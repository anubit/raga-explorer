package com.examples.anu.sqliteexamplewithjson.activity;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.examples.anu.sqliteexamplewithjson.Helper.MyBus;
import com.examples.anu.sqliteexamplewithjson.Interface.OnHighLightedNoteEvent;
import com.examples.anu.sqliteexamplewithjson.Interface.OnUnHighLightedNoteEvent;
import com.examples.anu.sqliteexamplewithjson.MySingleton;

import com.examples.anu.sqliteexamplewithjson.Interface.OnFragmentDataEvent;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;
import com.examples.anu.sqliteexamplewithjson.fragment.PianoFragment;
import com.examples.anu.sqliteexamplewithjson.util.Utils;
import com.squareup.otto.Bus;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import soundtouch.OnProgressChangedListener;
import soundtouch.ST;
import soundtouch.SoundStreamAudioPlayer;

import static com.examples.anu.sqliteexamplewithjson.util.Utils.populateShruthi;

/**
 * Created by Anu on 8/15/15.
 */

// Appcompat activity extends from fragment activity
public class DetailActivity extends FragmentActivity implements OnProgressChangedListener {

    //TAGS, CONSTANTS,
    public static String tag = "Detail Activity";
    public static int numTextViews = 13;
    public static final String ITEM_RAGA = "SELECTED_RAGA";
    public static final int NUM_OF_PIANO_KEYS = 13;
    public static final String FAV_MELA = "FAVORITE_MELA";
    public static final String FAV_RAGA = "FAVORITE_RAGA";
    public static final int SET_AROHANAM_VALUE = 1;
    public static final int SET_AVOROHANAM_VALUE = 2;
    //TODO: figure out where to put the notesArray? It has to be an array or in the DB?

    // for testing purposes map NotesArray to text
    public  static String[] intToString = {"sa", "re1", "re2", "ga1", "ga2", "ma1", "ma2", "pa",
            "da1", "da2", "ne2", "ne3", "Saa"};

    // UI
    private TextView ragaName, melakarthaName, aroCaption, avoCaption;
    private TextView[] tvAroTextView = new TextView[numTextViews];
    private TextView[] tvAvoTextView = new TextView[numTextViews];
    private RippleView rippleView;
    private ProgressBar progressBar;
    private ImageButton btnPlay, btnStop;//,btnFav;
    private ImageView btnFav;
    private OnProgressChangedListener progressListener;
    private Drawable drawable;
    private Resources res;
    private Fragment fragment;


    //highlighting variables
    private static int currentHighlightedIndex = 0;
    private static int previousHighlightedIndex = -1;
    private static int highLightTextView = 0;
    private static int prevHighLightedTextView = 0;
    private static int currentNoteIndex;

    // Shared Prefs, Bundles, utils variables
    private  float[] durationArray;
    private boolean shruthiValue,firstTimePlaying = true;
    private String selectedRagaName;
    private int favorites = 0;
    private float[] microSecNotes;
    public static int recentList;
    private PreferencesManager prefManager;
    private String absolutePathOfOutputFile;
    private SharedPreferences pref;
    Bundle aromapBundle,avomapBundle;
    private Bus bus;

    private Handler handler;
    // Database and SoundTouch related declaration variables
    private RagaDBDAO ragaDBInstance;
    private ST soundTouchObject;
    private SoundStreamAudioPlayer player;
    private String pitch;
    private float freq;
    private Raga detailedRagaObject;

    // data for piano fragment
    private int totalCountOfNotes = 0; // actual notes to be played both arog/avo
    private String[] dataToPianoFragment = new String[NUM_OF_PIANO_KEYS]; // 13 keys total in piano
    private int index;
     // this is the index where to place the note on the piano ( for arog and avo)

    // Linked Hashmaps used in textview and piano key to place the note.
    private LinkedHashMap dataIndexAromap = new LinkedHashMap(13);
    private LinkedHashMap dataIndexAvomap = new LinkedHashMap(13);
    private LinkedHashMap durationHashMap = new LinkedHashMap(26); //(at most 26)

    // Player state variables
    public enum PlayerState {INITIAL, PLAYING, PAUSED, FINISHED}
    public static PlayerState myPlayerState;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        myPlayerState = PlayerState.INITIAL;

        // set up the UI
        setup();
        // get the raga object to display
        getParcelableData();

        // splitting the comma separated notes(in nos. as 1,3,5,7) from json document to Raga(model),
        // store the notes in an array
        detailedRagaObject.setAroNotesArray();
        detailedRagaObject.setAvoNotesArray();
        // Map the nos in the array to notes from the Original Static inttoString[] array
        createIndexHashMap();
        // Set up the piano fragment and its listeners
        setupPianoFragment(); // you want to set the piano fragment after updating the data to it
        setupListeners();

        // Set the UI
        ragaName.setText(detailedRagaObject.getRagaName());
        melakarthaName.setText(detailedRagaObject.getMelakartha());

        // Helper method for highlighting
        // This function will calculate the microsec duration of all the wav files
        Utils.genericDurationMicroSec();
        durationHashMap = (LinkedHashMap) Utils.createDurationIndexHashMap();
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressBar.setVisibility(View.INVISIBLE);
        btnStop.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // TODO: check if the favorites button has the ripple effect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.d(tag,"build version is 21");

        } else {
            btnFav.setBackground(getResources().getDrawable(R.drawable.fav_btn));
        }
       // playerThread = (Thread) getLastCustomNonConfigurationInstance();
        // Get the new settings for Shruthi, pitch and tempo.
        getPreferencesData();

        // Reason why this calculating byte file is in onResume() because
        // you are combining Shruthi after the user selected the preferences.
        // extract the bytes. Sound Touch player accepts only bytes
        // converting the asset file to byte file
        calculateBytesOutputFile();

        if (player == null) {
            try {
                //todo: change the tempo as variable, currently tempo is set as 1.0f
                // SoundStreamAudioPlayer(track,filename,tempo,pitch)
                // A scale = 3.0f
                player = new SoundStreamAudioPlayer(0, absolutePathOfOutputFile, 1.0f, freq);
            } catch (IOException e) {
                Log.d(tag, " creating player failed");
            }

            // This duration array is a single array that contains the entire
            // duration of one raga. A raga contains Arog and Avarog.
            // The second variable in this function lets us know that we
            // are still part of the same rag. After we are done calculating
            // the duration of the avarog. then we know one rag is one. so the "DONE"
            // variable is set to true.
            durationArray = new float[dataIndexAvomap.size() + dataIndexAvomap.size()];

            Thread durationThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    durationArray = Utils.calculateCumulativeDuration(dataIndexAromap, false);
                    durationArray = Utils.calculateCumulativeDuration(dataIndexAvomap, true);
                }
            });
            durationThread.start();

            // Update textviews of both ragams as well on the keys of the piano fragment
            updateTextViews(dataIndexAromap, SET_AROHANAM_VALUE);
            updateTextViews(dataIndexAvomap, SET_AVOROHANAM_VALUE);

            // This passes the bundle (aromap and avoromap values) to piano fragment
            bus.post(new OnFragmentDataEvent(dataIndexAromap));
            bus.post(new OnFragmentDataEvent(dataIndexAvomap));

            // Pass this raga  to favorites screen if favorite ripple_button is pressed
            btnFav.setEnabled(true);
            selectedRagaName = detailedRagaObject.getRagaName();
            favorites = detailedRagaObject.getFavourites();
        } // if (!player.isInitialized())
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void setup() {
        prefManager = MySingleton.getPrefManagerInstance();
        pref = prefManager.getPref();

        progressListener = this;

        absolutePathOfOutputFile = MySingleton.getAbsolutePathOfOutputFile();

        ragaDBInstance = MySingleton.getRagaDBInstance();
        bus = MyBus.getInstance(); //TODO: check if we create this as a singleton

        ragaName = (TextView) findViewById(R.id.tvRagaName);
        melakarthaName = (TextView) findViewById(R.id.tvMelaName);
        aroCaption = (TextView) findViewById(R.id.tvAroCaption);
        avoCaption = (TextView) findViewById(R.id.tvAvoCaption);

        // ripple view on favorite ripple_button icon
        rippleView = (RippleView) findViewById(R.id.imgRipple);

        // set up bundle
        aromapBundle = new Bundle(13);
        avomapBundle = new Bundle(13);

        setupTextViews();

        btnStop = (ImageButton) findViewById(R.id.btnStop);
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnPlay.setBackgroundResource(R.drawable.play_18dp);
        btnFav = (ImageView) findViewById(R.id.btnFav);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        progressBar.setProgress(0);
        progressBar.setMax(99);
        //disable stop in the beginning
        btnStop.setEnabled(false);
    }

    // set the piano fragment dynamically
    private void setupPianoFragment() {
        // These are Fragments from the app.v4 package, it's needed to make the app compatible with API < 11
        FragmentManager fm = this.getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragment_place_holder);

        if (fragment == null) {
            fragment = new PianoFragment();
        }

        fm.beginTransaction().replace(R.id.fragment_place_holder, fragment).commit();
        fm.beginTransaction().addToBackStack("PianoFragment");
    }


    // array of textviews to display the notes dynamically based on the raga
    private void setupTextViews() {
        tvAroTextView[0] = (TextView) findViewById(R.id.tvAroNote0);
        tvAroTextView[1] = (TextView) findViewById(R.id.tvAroNote1);
        tvAroTextView[2] = (TextView) findViewById(R.id.tvAroNote2);
        tvAroTextView[3] = (TextView) findViewById(R.id.tvAroNote3);
        tvAroTextView[4] = (TextView) findViewById(R.id.tvAroNote4);
        tvAroTextView[5] = (TextView) findViewById(R.id.tvAroNote5);
        tvAroTextView[6] = (TextView) findViewById(R.id.tvAroNote6);
        tvAroTextView[7] = (TextView) findViewById(R.id.tvAroNote7);
        tvAroTextView[8] = (TextView) findViewById(R.id.tvAroNote8);
        tvAroTextView[9] = (TextView) findViewById(R.id.tvAroNote9);
        tvAroTextView[10] = (TextView) findViewById(R.id.tvAroNote10);
        tvAroTextView[11] = (TextView) findViewById(R.id.tvAroNote11);
        tvAroTextView[12] = (TextView) findViewById(R.id.tvAroNote12);

        tvAvoTextView[0] = (TextView) findViewById(R.id.tvAvoNote0);
        tvAvoTextView[1] = (TextView) findViewById(R.id.tvAvoNote1);
        tvAvoTextView[2] = (TextView) findViewById(R.id.tvAvoNote2);
        tvAvoTextView[3] = (TextView) findViewById(R.id.tvAvoNote3);
        tvAvoTextView[4] = (TextView) findViewById(R.id.tvAvoNote4);
        tvAvoTextView[5] = (TextView) findViewById(R.id.tvAvoNote5);
        tvAvoTextView[6] = (TextView) findViewById(R.id.tvAvoNote6);
        tvAvoTextView[7] = (TextView) findViewById(R.id.tvAvoNote7);
        tvAvoTextView[8] = (TextView) findViewById(R.id.tvAvoNote8);
        tvAvoTextView[9] = (TextView) findViewById(R.id.tvAvoNote9);
        tvAvoTextView[10] = (TextView) findViewById(R.id.tvAvoNote10);
        tvAvoTextView[11] = (TextView) findViewById(R.id.tvAvoNote11);
        tvAvoTextView[12] = (TextView) findViewById(R.id.tvAvoNote12);
    }

    private void getPreferencesData() {
        shruthiValue = prefManager.getKeyShruthi();
        pitch = prefManager.getKeyPitchValue(); // A is default
        freq = Utils.pitchToFreq(pitch);
        prefManager.getKeyFav();
    }

    // get the data passed in from search screen
    private void getParcelableData() {
        detailedRagaObject = new Raga();
        detailedRagaObject = (Raga) getIntent().getParcelableExtra(ITEM_RAGA);
    }

    private void setupListeners() {
        rippleView.setOnClickListener(btnFavClickListener);
        btnPlay.setOnClickListener(btnPlayListener);
        btnStop.setOnClickListener(btnStopListener);
    }

    // The counter variable sets the textview to display the text,
    // if I use the index to set, then there will be blanks between
    // the text views
    private void updateTextViews(LinkedHashMap hashMap, int whichTextView) {

        Map<Integer, String> map = hashMap;
        int count = 0;

        if (map != null) {
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                String value = entry.getValue();

                switch (whichTextView) {
                    case 1:
                        tvAroTextView[count++].setText(value);
                        break;

                    case 2:
                        tvAvoTextView[count++].setText(value);
                        break;
                }// end of else
            } // end of for loop map
        }
    }

    // from aronotes and avonotes array get the original index of the actual note
    // create a hash map with index,note for aronotes and avonotes
    // This function cannot be created in Utils function I don't know the index of the note
    // with respect to the String array.
    private void createIndexHashMap() {
        int count = 0;
        // for aronotes
        for (int i = 0; i < detailedRagaObject.getAroNotesArray().length; i++) {
            if (detailedRagaObject.getAroNotesArray()[i] != -1) {
                // find the actual note in string from the array
                int indexarray = detailedRagaObject.getAroNotesArray()[i];
                String note = intToString[indexarray];
                for (index = 0; index < intToString.length; index++) {
                    if (intToString[index].equals(note)) {
                        dataIndexAromap.put(index, note);
                        count++; // this gives the actual wav file count
                    }
                }
            }
        }

        for (int j = 0; j < detailedRagaObject.getAvoNotesArray().length; j++) {
            if (detailedRagaObject.getAvoNotesArray()[j] != -1) {
                // find the actual note in string from the array
                int indexarray = detailedRagaObject.getAvoNotesArray()[j];
                String note = intToString[indexarray];

                for (index = 0; index < intToString.length; index++) {
                    if (intToString[index].equals(note)) {
                        dataIndexAvomap.put(index, note);
                        totalCountOfNotes++;
                    }
                }
            }
        }
    }

    // get duration hash map value - which will give the note of the file name ( all the wav files)
    // match that with avonotes array
    // 13 filenames filename[0] - sa, filename[1] - re1
    // duration map(0.622,sa)
    // if sa matches the note in aro and avorog put the index

    private static void printHashMap(LinkedHashMap hashMap) {
        Map<Integer, String> map = hashMap;

        if (map != null) {
            Log.d(tag,"printing the map values in details");
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                Log.d(tag,"Key = " + entry.getKey() + "Value = " + entry.getValue());
            }
        }
    }

    // Adding the highlight feature for the textviews
    private void highLightText(int i,int whichTextView) {

        String text = null;
        int start,end;
        Spannable spanText = null;
        int pianoIndexToHighlight;
        switch (whichTextView) {
            case 1:

                text = tvAroTextView[i].getText().toString();
                start = 0;
                end = text.length();
                spanText = Spannable.Factory.getInstance().newSpannable(text);
                spanText.setSpan(new BackgroundColorSpan(0xff0000ff), start, end,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvAroTextView[i].setText(spanText);
                // highlight the text on the piano key. trigger the highlight event
                // The notes to highlight for piano is in the same order as the given json document.
                pianoIndexToHighlight = detailedRagaObject.getAroNotesArray()[i];
                bus.post(new OnHighLightedNoteEvent(text, pianoIndexToHighlight, spanText));
                break;
            case 2:
                text = tvAvoTextView[i].getText().toString();
                start = 0;
                end = text.length();
                spanText = Spannable.Factory.getInstance().newSpannable(text);
                spanText.setSpan(new BackgroundColorSpan(0xff0000ff), start, end,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvAvoTextView[i].setText(spanText);
                pianoIndexToHighlight = detailedRagaObject.getAvoNotesArray()[i];
                bus.post(new OnHighLightedNoteEvent(text, pianoIndexToHighlight, spanText));
                break;
        }
    }

    //Unhighlight
    private void unHighLight(int i,int whichTextView) {

        String text = null;
        int start,end;
        Spannable spanText = null;
        int pianoIndexToUnhighlight;
        switch (whichTextView) {
            case 1:
                text = tvAroTextView[i].getText().toString();
                start = 0;
                end = text.length();
                spanText = Spannable.Factory.getInstance().newSpannable(text);
                spanText.setSpan(new BackgroundColorSpan(0xFF), start, end,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvAroTextView[i].setText(spanText);
                pianoIndexToUnhighlight = detailedRagaObject.getAroNotesArray()[i];
                bus.post(new OnUnHighLightedNoteEvent(text, pianoIndexToUnhighlight, spanText));
                break;
            case 2:
                text = tvAvoTextView[i].getText().toString();
                start = 0;
                end = text.length();
                spanText = Spannable.Factory.getInstance().newSpannable(text);
                spanText.setSpan(new BackgroundColorSpan(0xFF), start, end,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvAvoTextView[i].setText(spanText);
                pianoIndexToUnhighlight = detailedRagaObject.getAvoNotesArray()[i];
                bus.post(new OnUnHighLightedNoteEvent(text, pianoIndexToUnhighlight, spanText));
                break;

        }
    }

    private View.OnClickListener btnFavClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (player.isInitialized()) {
                player.pause();
                player.stop();
            }
            // save the db changes to this raga favorites table
            // increment the counter for favorites
            persistFavData(selectedRagaName, favorites++);
            prefManager.savePreferences("favoritesNotNull",true);
            MySingleton.favCount++;
            Toast.makeText(getBaseContext(),"Added to Favorites",Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener btnPlayListener;

    {
        btnPlayListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (myPlayerState) {
                    case INITIAL:
                        myPlayerState = PlayerState.PLAYING;
                        // in the initial state when it is playing, you want to show
                        // the next state in which the user can go into
                        btnPlay.setImageResource(android.R.color.transparent);
                        btnPlay.setBackgroundResource(R.drawable.pause_18dp);

                        firstTimePlaying = true;
                        try {
                            play();
                            //"player goes from INITIAL state to Playing success"

                        } catch (IOException e) {
                            //"player goes from INITIAL state to Playing failed"
                        }
                        break;

                    case PLAYING:
                        // if duration is more than 95% go to FINISHED in SoundStreamRunnable.java.sendProgressUpdate()
                        btnPlay.setImageResource(android.R.color.transparent);
                        btnPlay.setBackgroundResource(R.drawable.play_18dp);
                        myPlayerState = PlayerState.PAUSED;
                        Log.d(tag, "playing state - ripple_button should be pause");
                        firstTimePlaying = false;
                        //"player goes from PLAYING state to PAUSED success"
                        player.pause();
                        setProgressChangedListener(progressBar);
                        break;

                    case PAUSED:
                        btnPlay.setImageResource(android.R.color.transparent);
                        btnPlay.setBackgroundResource(R.drawable.pause_18dp);
                        myPlayerState = PlayerState.PLAYING;
                        firstTimePlaying = false;
                        try {
                            //"player goes from  PAUSED state to PLAYING success"
                            play();

                        } catch (IOException e) {

                        }
                        break;

                    case FINISHED:
                        // if duration is more than 95% go to FINISHED in SoundStreamRunnable.java.sendProgressUpdate()
                        btnPlay.setBackgroundResource(R.drawable.play_18dp);
                        myPlayerState = PlayerState.PLAYING;
                        firstTimePlaying = true;

                        // flush the data, create a new player, firsttime, pausestate = true, hit play()
                        try {
                            player.seekTo(0, true);
                            Log.d(tag, "creating  new player");
                            player = new SoundStreamAudioPlayer(0, absolutePathOfOutputFile, 1.0f, freq);

                        } catch (IOException e) {
                            Log.d(tag, " creating player failed");
                        }
                        try {
                            btnPlay.setBackgroundResource(R.drawable.pause_18dp);

                            play();
                            Log.d(tag, "player goes from  FINISHED state to PLAYING success");

                        } catch (IOException e) {
                            Log.d(tag, "player goes from  FINISHED state to PLAYING failure");
                        }
                        break;
                }
            }
        };
    }

    // In the soundStreamRunnable class if the tracks are done, then the player
    // is closed.
    private View.OnClickListener btnStopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(tag, "inside stop ripple_button");
            btnPlay.setImageResource(android.R.color.transparent);
            btnPlay.setBackgroundResource(R.drawable.play_18dp);
            progressBar.setVisibility(View.VISIBLE);
            myPlayerState = PlayerState.FINISHED;
            progressBar.setProgress(0);
            progressBar.setProgressDrawable(drawable);
            setProgressChangedListener(progressBar);
            progressBar.invalidate();
            player.pause();
            player.stop();
        }
    };


    private void setProgressChangedListener(ProgressBar progressBar) {
        player.setOnProgressChangedListener(progressListener);
    }


    public void play() throws IOException {

        // Feature to show the recently played raga
        // adding the feature of adding the last played raga in the database with the current time
        // get the current time in millisecs using joda library
        long millis = DateTime.now().getMillis();
        persistData(detailedRagaObject.getRagaName(), millis);

        // activate all the ImageButtons after the player has started
        btnStop.setEnabled(true);
        progressBar.setVisibility(View.VISIBLE);
      /*  try {
            Thread.sleep(1000);
        }catch (Exception e) {
            Log.d(tag,"Interrupted exception for the player thread to catch up");
        }*/

        if (!player.isInitialized()) {
            // changed from 3.0f to pitch
            player = new SoundStreamAudioPlayer(0, absolutePathOfOutputFile, 1.0f, freq);
        }

        Thread playerThread;
        // Note that player is  runnable.
        if (firstTimePlaying) {
            firstTimePlaying = false;
            playerThread = new Thread(player);
            playerThread.start();
            player.start();

        } else { // if paused, continue to use the player in the same thread
                Log.d(tag,"no thread created for the player");
                player.start();
           // }
        }
        // player in finished state
        if (myPlayerState == PlayerState.FINISHED) {
            player.setLoopStart(0);
            progressBar.setProgress(0);
            btnPlay.setBackgroundResource(R.drawable.play_18dp);
            progressBar.setProgressDrawable(drawable);
            progressBar.invalidate();
            myPlayerState = PlayerState.PLAYING;
            firstTimePlaying = true;
        }
        setProgressChangedListener(progressBar); // activate the listener for re-play
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        player.pause();
        player.stop();
    }

    // This function persists the last time the raga played and added the time
    // to recently played list
    private void persistData(String ragaName, long millisecs) {
        Log.d(tag, "call the database to update");
        ragaDBInstance.update(ragaName, millisecs);
    }

    private void persistFavData(String ragaName,int fav){
        ragaDBInstance.update(ragaName,fav);
    }

    /* Calculate when to highlight.
     * idea is to highlight at the end of each note
     * Known facts: 1 sample = 2 bytes
     * each wav file in bytes => no of samples
     * 1 sec = 16100 khz = samples
     * my recording is 1 sec for each note
     * But the duration in the progress bar expects in micro secs
     * convert the secs to micro secs 1 sec = 10^6 micro sec
     * convert the no of samples /sec = samples /micro secs
     * 1 sec = 16100 samples
     * 1/16100 sec = 1 sample
     * 1/16100 * 10^6 micro secs = 1 sample
     * 1000/16 = 62.5 micro sec = 1 sample
    */

    /* create an array for each note (end time in micro sec)
     * create an array of bytes for each note
     * no of bytes /2 = gives no of samples
     * currentPercentage is the current playing duration in microsecs
     * currentPercentage  = getPlayedduration(long) / duration (long) in micro sec
     * duration in progress bar from sound touch expects in micro seconds
     * I need the getPlayedDuration (without dividing by Duration)
     * convert that to micro sec for the progress bar to set in SoundStreamRunnable.java
     */

    /* for each duration in durationArray
     * if (cp < duration) {
     * highlight(currentnoteindex);
     */
    @Override
    public void onProgressChanged(int track, double currentPercentage, long position) {

        drawable = getBaseContext().getResources().getDrawable(R.drawable.progressbar_drawable);
        // convert from double currentPercentage to percentage to set the progress bar
        int percentage = (int) ((currentPercentage) * 100);
        float currentDurationInMicro = (float) currentPercentage * player.getDuration();

        if (durationArray != null) {
            int indexDur = 0;

            while (currentDurationInMicro > durationArray[indexDur]) {
                indexDur++;
            }
            // Highlight avorohanam
            if (indexDur >= dataIndexAromap.size()) {
                highLightTextView = 2;
                currentHighlightedIndex = indexDur - dataIndexAromap.size();
            } else {
                highLightTextView = 1;
                currentHighlightedIndex = indexDur;
            }

            if (!((currentHighlightedIndex == previousHighlightedIndex) &&
                    (highLightTextView == prevHighLightedTextView))) {
                if (previousHighlightedIndex >= 0 ) {
                    unHighLight(previousHighlightedIndex, prevHighLightedTextView);
                }
                highLightText(currentHighlightedIndex, highLightTextView);
                previousHighlightedIndex = currentHighlightedIndex;
                prevHighLightedTextView = highLightTextView;
            }

        } // if (durationArray != null)

        if (percentage < 100) {
            progressBar.setProgress(percentage);
            progressBar.setProgressDrawable(drawable);
        } else { // > 100 %
            progressBar.setProgress(0); // return the progress to 0
            progressBar.setProgressDrawable(drawable);
        }
        progressBar.invalidate();
    }

    @Override
    public void onTrackEnd(int track) {
        progressBar.setProgress(0);
        player.seekTo(0,true);
        progressBar.setVisibility(View.INVISIBLE);
        progressBar.invalidate();
    }

    @Override
    public void onExceptionThrown(String string){
        Log.d(tag,"Exception Thrown on Progress changed = " + string);
    }

    // This function calculates the total bytes to be played by soundtouch
    // sound touch accepts only bytes. converting the asset file to byte file, include shruthi
    private void calculateBytesOutputFile() {

        Thread t2 = new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    //populate shruthi
                    populateShruthi(getBaseContext(), pitch);
                    Utils.populateOutputFile(absolutePathOfOutputFile, getBaseContext(),
                            dataIndexAromap, dataIndexAvomap, shruthiValue);
                    Utils.setLengthOutputFile(absolutePathOfOutputFile);

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }//run()
        });
        t2.start();

        try {
            t2.join(); // to make sure that the player does not start until the createfile is done
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

