package com.examples.anu.sqliteexamplewithjson.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter;
import com.examples.anu.sqliteexamplewithjson.util.MyIntentService;
import com.examples.anu.sqliteexamplewithjson.util.ServiceResultReceiver;

import java.util.ArrayList;

/**
 * Created by Anu on 6/9/15.
 */
// This class is for arraylist of ragas: I'm using cursor not array list
// refer: FavoritesActivity1
public class FavoritesActivity extends BaseActivity{

    private RagaListAdapter favoritesAdapter;
    private ListView lvFavorites;
    private ServiceResultReceiver mReceiver;
    public static final String FAV_RAGA = "FAVORITE_RAGA";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This framelayout is from baseActivity where the favorites activity will be loaded
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame_fragment);;
        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_favorites, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);
        lvFavorites = (ListView) findViewById(R.id.lvFavorites);
        favoritesAdapter = new RagaListAdapter(this,new ArrayList<Raga>());
        lvFavorites.setAdapter(new RagaListAdapter(this,null));

        setupServiceReceiver();
        setupIntentService();
    }

    //Intent service
    private void setupIntentService() {
        Log.d(tag, "inside setupIntentService");

        Intent intent = new Intent(this,
                MyIntentService.class);
        intent.putExtra("ragaName", getIntent().getStringArrayExtra(FAV_RAGA));
        intent.putExtra(MyIntentService.RECEIVER_KEY, mReceiver);
        intent.putExtra(MyIntentService.COMMAND_KEY, MyIntentService.SEARCH_DATABASE_ACTIVITY);
        this.startService(intent);
    }

    public void setupServiceReceiver() {
        mReceiver = new ServiceResultReceiver(new Handler());
        Log.d(tag, "inside setupServiceReceiver");
        // This is where we specify what happens when data is received from the service
        mReceiver.setReceiver(new ServiceResultReceiver.Receiver() {

            @Override
            public void onReceiveResult(int resultCode, Bundle resultData) {

                switch (resultCode) {
                    case MyIntentService.STATUS_RUNNING:
                        break;
                    case MyIntentService.STATUS_SUCCESS:
                    //    Log.d(tag, "inside the status success");
                        boolean success = resultData.getBoolean(MyIntentService.SERVICE_WAS_SUCCESS_KEY);
                      //  Log.d(tag, "success value = " + success);

                        if (success) {
                            Toast.makeText(getBaseContext(),
                                    "The service was a success from favorites", Toast.LENGTH_LONG).show();
                            lvFavorites.setAdapter(favoritesAdapter);
                            favoritesAdapter.clear();
                            Raga favRagaObject = new Raga();
                            favRagaObject = resultData.getParcelable("SelectedRaga");
                            Log.d(tag, "fav list raga name =" + favRagaObject.getRagaName());
                            favoritesAdapter.add(favRagaObject);
                            favoritesAdapter.notifyDataSetChanged();

                        } else {
                            // Show not success message
                            Toast.makeText(getBaseContext(),
                                    "The service was a failure", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case MyIntentService.STATUS_FINISHED:
                        Toast.makeText(getBaseContext(), "The service was finished",
                                Toast.LENGTH_LONG).show();
                        break;
                    case MyIntentService.STATUS_ERROR:
                        Toast.makeText(getBaseContext(), "The service has an error",
                                Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up ripple_button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
