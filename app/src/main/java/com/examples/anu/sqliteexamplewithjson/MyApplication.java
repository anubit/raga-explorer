package com.examples.anu.sqliteexamplewithjson;

import android.app.Application;
import android.content.Context;
import android.util.Log;

/**
 * Created by Anu on 9/2/15.
 */
// http://www.devahead.com/blog/2011/06/extending-the-android-application-class-and-dealing-with-singleton/
// This class contains the singleton
// why is the singleton initialized in MyApplication???
// Isn’t it supposed to be just a singleton so the instance can be initialized
// everywhere in the application like in an Activity?"

// an activity could be destroyed and created many times, while the application process remains the same.
// sometimes some static variables bound to activities happened to be uninitialized even though they’ve previously been initialized!
// singleton requires the use of static variables.
// the static variables instances are bound to the class loader of the class that first initialized them.

// his means that if a static variable inside any class has been initialized by an activity,
// when that activity is destroyed also its class might be unloaded and so the variable
// becomes uninitialized again!
// While if the variable is initialized by the application class, it’s life is the same as the
// application process so we’re sure that
// it will never become uninitialized again.
// That’s why I chose to initialize all the singletons in the MyApplication class.

// Thread safe: Make sure the player is thread safe?
// if the singleton is used by a single thread at a time
// (like by the active activity in its UI thread) you don’t need the synchronization,
// if multiple threads are accessing the singleton’s methods,
// then you’ve got to make sure they’re properly synchronized,
// The initialization of the singleton through its initInstance() method is actually thread-safe
// because that method is called only
// by the MyApplication class when the app is created (onCreate() method) in that single thread
// and nowhere else in the application.

// The getInstance() method of the MySingleton class is thread-safe as well because it just
// returns the instance and that instance is never modified in the application after its initialization.

//  The singleton instance is just one and will live as long as the application process is alive.
// when the app is closed and destroyed by the system I won’t need anymore the singleton instance so everything will be garbage collected.
public class MyApplication extends Application {

    static {
        System.loadLibrary("soundtouch-jni");
    }

    private static Context appContext; // I added this to get the applicationContext() for getFilesDir();

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the singletons so their instances
        // are bound to the application process.
        // The singletons will be throughout the application not just bound by activity
        Log.d("MyApplication", "inside onCreaate - initSingletons()");
        appContext = this;
        initSingletons();
    }

    protected void initSingletons()
    {
        MySingleton.initInstance();
    }
    public void customAppMethod()
    {
        // Custom application method
    }




    public static Context getAppContext() {
        return appContext;
    }
}
