package com.examples.anu.sqliteexamplewithjson.db;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Anu on 5/9/15.
 */

// Model: Raga
// SerializedNames should be exact names as given in the json document
// Notes are stored as a comma separated string
public class Raga implements Parcelable{

    @SerializedName("name")
    private String name;
    @SerializedName("ragaid")
    private String ragaNum;
    @SerializedName("aronotes")
    private String aroNotes;
    @SerializedName("avonotes")
    private String avoNotes;
    @SerializedName("melakartha")
    private String melakarthaName;
    @SerializedName("melakartha_id")
    private String melakarthaNo;
    @SerializedName("last_played_time")
    private String lastPlayedTime;
    private int favourites;

    private int[] aroNotesArray = new int[20];
    private int[] avoNotesArray = new int[20];
    private int aroNo;
    private int avoNo;


    public Raga()
    {
    }

    public Raga(String name) {
        this.name = name;
    }

    public Raga(String name,String num,String melaName,String melaNo,String aronotes,
                String avonotes,String lastPlayedTime,int favourites)
    {
        setRagaName(name);
        setRagaNum(num);
        setMelakartha(melaName);
        setMelakarthaNo(melaNo);
        setAroNotes(aronotes);
        setAvoNotes(avonotes);
        setLastPlayedTime(lastPlayedTime);
        setFavourites(favourites);
    }

    /**
     * Use when reconstructing Raga object from parcel
     * This will be used only by the 'CREATOR'
     * @param in a parcel to read this object
     *
     */
    public Raga(Parcel in) {
        name = in.readString();
        ragaNum = in.readString();
        melakarthaName = in.readString();
        melakarthaNo = in.readString();
        aroNotes = in.readString();
        avoNotes = in.readString();
        lastPlayedTime = in.readString();
        favourites = in.readInt();
    }

    public void setRagaName(String ragaName) {
        this.name = ragaName;
    }
    public void setRagaNum(String ragaNo) {
        this.ragaNum = ragaNo;
    }
    public void setMelakarthaNo(String melakarthaNum) { this.melakarthaNo = melakarthaNum;}
    public void setAroNo(int aronum)
    {
        aroNo = aronum;
    }
    public void setAvoNo(int avonum)
    {
        avoNo = avonum;
    }
    public void setMelakartha(String name) {
        this.melakarthaName = name;
    }
    public void setAroNotes(String givenAroNotes)
    {
        this.aroNotes = givenAroNotes;
    }
    public void setAvoNotes(String givenAvoNotes)
    {
        this.avoNotes = givenAvoNotes;
    }
    public void setLastPlayedTime(String givenLastPlayedTime) {
        this.lastPlayedTime = givenLastPlayedTime;
    }

    public void setFavourites(int favourites) { this.favourites = favourites;}

    private void initializeAroNotesArray(int[] aroNotesArray)
    {
        for(int i = 0; i < aroNotesArray.length; i++){
            aroNotesArray[i] = -1;
        }
    }
    private void initializeAvoNotesArray(int[] avoNotesArray)
    {
        for(int i = 0; i < avoNotesArray.length; i++){
            avoNotesArray[i] = -1;
        }
    }
    // creating a list of notes from string aronotes, which is separated by comma
    public void setAroNotesArray() {

        initializeAroNotesArray(aroNotesArray);
        List<String> numbers = new ArrayList<String> (Arrays.asList(aroNotes.split(",")));

        int i = 0;
        for (String number : numbers) {
          //  Log.d("raga class", " the aronotes Array =" + Integer.valueOf(number));
            aroNotesArray[i++] = Integer.valueOf(number);
        }

    }
    public void setAvoNotesArray() {

        initializeAvoNotesArray(avoNotesArray);
        List<String> numbers = new ArrayList<String>(Arrays.asList(avoNotes.split(",")));
        int i = 0;
        for (String number : numbers) {
            avoNotesArray[i++] = Integer.valueOf(number);
        }
    }

    public String getAroNotes() {
        return aroNotes;
    }
    public String getAvoNotes() { return avoNotes;}
    public String getRagaNum() {
        return ragaNum;
    }
    public String getRagaName() {
        return name;
    }
    public int getFavourites() { return favourites;}
    public int getAroNo() { return aroNo; }
    public int getAvoNo() { return avoNo; }
    public int[] getAroNotesArray() {
        return aroNotesArray;
    }
    public int[] getAvoNotesArray() {
        return avoNotesArray;
    }
    public String getMelakartha() {
        return melakarthaName;
    }
    public String getMelakarthaNo() {
        return melakarthaNo;
    }
    public String getLastPlayedTime() { return lastPlayedTime;}
  /*  public int getFavourites() { return  favourites;} */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags)  {
        out.writeString(name);
        out.writeString(ragaNum);
        out.writeString(melakarthaName);
        out.writeString(melakarthaNo);
        out.writeString(aroNotes);
        out.writeString(avoNotes);
        out.writeString(lastPlayedTime);
        out.writeInt(favourites);
    }

    /**
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays
     *
     * If you don’t do that, Android framework will through exception
     * Parcelable protocol requires a Parcelable.Creator object called CREATOR
     */
    public static final Parcelable.Creator<Raga> CREATOR
            = new Parcelable.Creator<Raga>() {
        @Override
        public Raga createFromParcel(Parcel in) {
            return new Raga(in);
        }

        @Override
        public Raga[] newArray(int size) {
            return new Raga[size];
        }
    };

}
