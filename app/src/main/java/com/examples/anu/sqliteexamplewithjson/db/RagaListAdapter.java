package com.examples.anu.sqliteexamplewithjson.db;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.fragment.SearchFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 5/17/15.
 */
// Not used in this app:This custom adapter displays raga name and melakartha name
// Array Adapter works fine if you want to just display the array list
// Implements Filterable is to used in AutocompleteTextView in SearchFragment

public class RagaListAdapter extends ArrayAdapter<Raga> implements Filterable{

    private static final int MAX_RESULTS = 10; // this is used in autocomplete text view
    public static String tag = "RagaListAdapter";
    ArrayList<Raga> fullListRagas = new ArrayList<>();
    ArrayList<Raga> original = new ArrayList();
    private ArrayFilter mFilter;

    public RagaListAdapter(Context context, List<Raga> ragas) {
        super(context, 0, ragas);
        if (ragas == null)
        {
            this.fullListRagas = (ArrayList) SearchFragment.listRagaRef;
        }
        else this.fullListRagas = (ArrayList)ragas;
        original = (ArrayList) new ArrayList<Raga>(fullListRagas);
    }

    private class ViewHolder {
        TextView tvRagaName;
        TextView tvMelakarthaName;
    }

  //  @Override
    public int getCount() {
        return fullListRagas.size();
    }

    @Override
    public Raga getItem(int position) {

        return fullListRagas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // getView() method to describe the translation between the data item and the View to display.
    // getView() is the method that returns the actual view used as a row within
    // the ListView at a particular position.

    // Making calls to findViewById() is really slow in practice,
    // and if your adapter has to call it for each View in your row
    //
    //for every single row then you will quickly run into performance issues.
    // What the ViewHolder class does is cache the call to findViewById().
    // Once your ListView has reached the max amount of rows it can display on a screen,
    // Android is smart enough to begin recycling those row Views.
    // We check if a View is recycled with if (convertView == null).
    // If it is not null then we have a recycled View and can just change its values,
    // otherwise we need to create a new row View.
    // The magic behind this is the setTag() method which lets us attach an arbitrary object
    // onto a View object, which is how we save the already inflated View for future reuse.


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Raga raga = getItem(position);
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();

            holder.tvRagaName = (TextView) convertView
                    .findViewById(R.id.tvRagaName);
            holder.tvMelakarthaName = (TextView) convertView
                    .findViewById(R.id.tvMelakarthaName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvMelakarthaName.setText(raga.getMelakartha());

        holder.tvRagaName.setText(raga.getRagaName());
        return convertView;
    }

    public Filter getFilter() {
        if (mFilter == null)
            mFilter = new ArrayFilter();
        return mFilter;

    }

    // THis is used in autoCompleteTextView in searchFragment
    // get all the raga first and display only the result that has the raganame
    // starts with the constraints
    // the lock is needed because we are calling the raga adapter from
    // different objects. what happens if i don't use the lock
   private class ArrayFilter extends Filter {
        private Object lock = new Object();

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();
            // this is to ensure that original that we are comparing to actually exists
            if (original == null) {
                synchronized (lock) {
                    original = new ArrayList<Raga>(fullListRagas);
                }
            }
            //show the entire list if prefix is null
            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    ArrayList<Raga> list = new ArrayList<Raga>(original);
                    results.values = list;
                    results.count = list.size();
                }
            } else { // do the filtering
                final String prefixString = prefix.toString().toUpperCase();

                ArrayList<Raga> values = original;
                int count = values.size();

                ArrayList<Raga> resultList = new ArrayList<Raga>(count);

                for (Raga raga : original) {
                    if(raga.getRagaName().toUpperCase().contains(prefixString)){
                        resultList.add(raga);
                    }
                }

                results.values = resultList;
                results.count = resultList.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if(results.values!=null){
                fullListRagas = (ArrayList<Raga>) results.values;
            }else{
                fullListRagas = new ArrayList<Raga>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

}
