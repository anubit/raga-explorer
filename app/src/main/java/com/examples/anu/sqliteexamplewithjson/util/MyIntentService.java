package com.examples.anu.sqliteexamplewithjson.util;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.examples.anu.sqliteexamplewithjson.ReturnDataFromAsyncTask;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;
import com.examples.anu.sqliteexamplewithjson.db.RagaList;
import com.google.gson.GsonBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 5/30/15.
 */
//http://javatechig.com/android/creating-a-background-service-in-android#2-intentservice-limitations
// Intent service to load the database from json document.
public class MyIntentService extends IntentService {

    private Context context;
    public static String tag = "MyIntentService";
    RagaDBDAO ragaDAO;
    ArrayList<Raga> ragaListToDisplay;
    public ReturnDataFromAsyncTask returnData = null;
    WeakReference<Activity> weakActivity;
    public String fileName = "assetsFile";


    //NOTE: If you want these constants to be referenced by more than one class
    // Status Constants
    public static final int STATUS_RUNNING = 0x1;
    public static final int STATUS_FINISHED = 0x2;
    public static final int STATUS_SUCCESS = 0x3;
    public static final int STATUS_ERROR = 0x4;
    // Command Constants
    public static final int LOAD_DATABASE_ACTIVITY = 0x5;
    public static final int SEARCH_DATABASE_ACTIVITY = 0x6;

    public static final String COMMAND_KEY = "service_command";
    public static final String RECEIVER_KEY = "service_receiver";
    public static final String SERVICE_WAS_SUCCESS_KEY = "service_was_success";

    private ResultReceiver receiver;

    public MyIntentService() {
        super(MyIntentService.class.getName());
    }

    // I'm using Intent service only to load the database the first time and not
    // search operation. Even though I had the idea of using search using Intent service
    // it got quite complicated to get the results back and store it in an array to
    // display the results. Dealing with cursor directly and displaying the result proved efficient
    @Override
    protected void onHandleIntent(Intent intent) {

        // you’re setting your ResultReceiver to a value passed in via the intent object.
        // Then you’re getting the command from the same intent.
        // you’re sending a message back to the receiver telling it that your service is running.

        Bundle extras = intent.getExtras();

        // Load Database activity
        this.receiver = intent.getParcelableExtra(RECEIVER_KEY);
        int command = extras.getInt(COMMAND_KEY);
        context = getBaseContext();

        this.receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        switch (command)
         {
            case LOAD_DATABASE_ACTIVITY:
                doLoadIntoDB();
                break;
            case SEARCH_DATABASE_ACTIVITY:
                // NOT USED
                // display the raga name and the melakartha name
                String ragaNameToSearch = intent.getStringExtra("ragaName");

                break;
            default:
                receiver.send(STATUS_FINISHED, Bundle.EMPTY);
        }
    }

    /* 1. read the document.json file from assets folder
       2. create a json string, pass that json string to GSON Builder to create an object
       3. That GSOn created objects contains an array of ragas
       4. From that arraylist, load the ragas into sqlite database via insert command
     */
    private void doLoadIntoDB() {
        int resourceID = 0;
        List<Raga> ragaArrayNames = new ArrayList<Raga>();
        ragaDAO = new RagaDBDAO(context);
        // Load from asset folder to json
        String json = loadAssetsToJson();
        //convert the json string to ragalist array
        RagaList newRagaObject = new GsonBuilder().create().fromJson(json, RagaList.class);
        // RagaObject contains an array of ragas.

        ragaArrayNames = newRagaObject.listOfRagas;
        // Load this to DB
        if (ragaArrayNames != null) {
            ragaDAO.loadRagas((ArrayList) ragaArrayNames);
        }
        // delete this temp object as we have already loaded into db
        newRagaObject.listOfRagas.clear();

        if (false) { // error
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
            this.stopSelf();
            receiver.send(STATUS_FINISHED, Bundle.EMPTY);
        } else {
            Log.d(tag,"send the success bundle to receiver after creating the db");
            Bundle b = new Bundle();
            b.putBoolean(SERVICE_WAS_SUCCESS_KEY, true);
            receiver.send(STATUS_SUCCESS, b);
            this.stopSelf();
            receiver.send(STATUS_FINISHED, Bundle.EMPTY);
        }

    }

    // Function that reads from Assets and builds a JSon string
    // 1. open the assets folder document.json
    // 2. read from the asset using inputstream
    // 3. create a json string to pass it to GSONBuilder ( that converts json to java objects)
    private String loadAssetsToJson() {
        InputStream inputStream;
        String json;
        FileOutputStream fos;

        // getCacheDir() is useful if you want to cache some data instead of persist
        // outputStream = openFileOutput("tempFileOutput", Context.MODE_PRIVATE);
        // File cacheFile = new File(context.getCacheDir(), "document.json");
        // outputStream = new FileOutputStream(cacheFile);
        try {
            inputStream = context.getAssets().open("document.json");
            fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            fos.write(buffer);
            inputStream.close();
            fos.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
