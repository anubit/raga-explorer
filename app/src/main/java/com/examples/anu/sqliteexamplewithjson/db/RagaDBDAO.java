package com.examples.anu.sqliteexamplewithjson.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.util.Log;

import java.util.ArrayList;

import static com.examples.anu.sqliteexamplewithjson.db.RagaDbHelper.MELAKARTHA_NAME;
import static com.examples.anu.sqliteexamplewithjson.db.RagaDbHelper.RAGA_NAME;
import static com.examples.anu.sqliteexamplewithjson.db.RagaDbHelper.TABLE_RAGA;

/**
 * Created by Anu on 5/9/15.
 */

// RagaDbHelper class just creates the database tables without any data.
// THis class inserts the data in the database.

// Class for: data Access object (crud) for Raga
// This DAO(data access object creates Java models)
// It maintains the database connection and supports adding new ragas and fetching all ragas

//  We will use a data access object (DAO) to manage the data for us.
// The DAO is responsible for handling the database connection and for accessing and modifying the data.
// It will also convert the database objects into real Java Objects,
// so that our user interface code does not have to deal with the persistence layer.

// NOTE: Using a DAO is not always the right approach. A DAO creates Java model objects;
// using a database directly or via a ContentProvider is typically more resource efficient
// as you can avoid the creation of model objects.

// 1. create an instance of RegistrationOpenHelper
// 2. get the writable database to insert data.
// 3. the first time, oncreate() called to create the db
// 4. To Insert the data from json to this database, create contentValues object
// 5. ContentValues is a simple datastruture consists of name-value pairs to
// map db table names to values

public class RagaDBDAO {

    private SQLiteDatabase ragaDatabaseInstance;
    RagaDbHelper ragaDbHelper;
    Context context;
    Cursor cursor;

    public static String tag = "RegistrationRagaDBAdapter";
    // Col names to retrieve from the database
    private final String[] PROJECTION =
            new String[] {ragaDbHelper.KEY_RAGA_ID,ragaDbHelper.KEY_RAGA_NUM,ragaDbHelper.RAGA_NAME,
                    ragaDbHelper.MELAKARTHA_NUM,ragaDbHelper.MELAKARTHA_NAME,
                    ragaDbHelper.ARONOTES,ragaDbHelper.AVONOTES,ragaDbHelper.LASTPLAYEDTIME,
                    ragaDbHelper.FAVORITES};
    // How you want the results sorted in the resulting Cursor
    private String sortOrder =
            ragaDbHelper.RAGA_NAME + " ASC";
    public static String NUMDISPLAY = "5";

    public RagaDBDAO(Context c) {
        context = c;
    }

    // to read from database
    public RagaDBDAO opnToRead() {
        ragaDbHelper = ragaDbHelper.getHelper(context);
        ragaDatabaseInstance = ragaDbHelper.getReadableDatabase();
        return this;
    }

    // write to database
    public RagaDBDAO opnToWrite() {
        ragaDbHelper = ragaDbHelper.getHelper(context);
        ragaDatabaseInstance = ragaDbHelper.getWritableDatabase();
        return this;
    }

    // Insert ragas into database.
    public void loadRagas(ArrayList<Raga> ragas)
    {
        for (Raga r: ragas) {
            insert(ragaDatabaseInstance,r);
        }
    }

    public void close() {
        ragaDatabaseInstance.close();
    }

    // Insert into the database by passing the contentValues object to insert method
    private long insert(SQLiteDatabase db, Raga item) {

        long ragaId;
        opnToWrite();
        ContentValues contentValues = new ContentValues(8);
        contentValues.put(ragaDbHelper.KEY_RAGA_NUM, item.getRagaNum());
        contentValues.put(ragaDbHelper.RAGA_NAME, item.getRagaName());
        contentValues.put(ragaDbHelper.MELAKARTHA_NUM, item.getMelakarthaNo());
        contentValues.put(ragaDbHelper.MELAKARTHA_NAME, item.getMelakartha());
        contentValues.put(ragaDbHelper.ARONOTES, item.getAroNotes());
        contentValues.put(ragaDbHelper.AVONOTES, item.getAvoNotes());
        contentValues.put(ragaDbHelper.LASTPLAYEDTIME, item.getLastPlayedTime());
        contentValues.put(ragaDbHelper.FAVORITES,item.getFavourites());

        ragaId = ragaDatabaseInstance.insert(ragaDbHelper.TABLE_RAGA, null,
                contentValues);

        close();
        return ragaId;

    }

    public void update(String name, int favorites) { updateDB(name,favorites);}

    public void  update(String name,long secs) {
        updateDB(name, secs);
    }

    private long updateDB(String name, long secs) { //  update(ragaDatabaseInstance,)
        opnToWrite();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ragaDbHelper.LASTPLAYEDTIME, secs); // (column name, new row value)
        String selection = ragaDbHelper.RAGA_NAME + " LIKE ?"; // where ragaName column = name (that is, selectionArgs)
        String[] selectionArgs = { name };

        long id = ragaDatabaseInstance.update(ragaDbHelper.TABLE_RAGA, contentValues, selection,
                selectionArgs);
      //  close();
        return id;
    }
    // update favorites
    private int updateDB(String name,int favorites){
      //  Log.d(tag,"inside update db of favorites " + favorites);
        opnToWrite();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ragaDbHelper.FAVORITES, favorites); // (column name, new row value)
        String selection = ragaDbHelper.RAGA_NAME + " LIKE ?"; // where ragaName column = name (that is, selectionArgs)
        String[] selectionArgs = { name };

         int id = ragaDatabaseInstance.update(ragaDbHelper.TABLE_RAGA, contentValues, selection,
                selectionArgs);
        return id;
    }

    public synchronized Cursor getAllRagas()
    {
        opnToRead(); // opening the db

      //  Cursor cursor = ragaDatabaseInstance.rawQuery(sql, null);
        //query(tablename,projectioncols,selection,selectionargs,groupby,having,orderby
        Cursor cursor =
        ragaDatabaseInstance.query(ragaDbHelper.TABLE_RAGA,PROJECTION,
                null, null, null, null,
                sortOrder);
                //ragaDbHelper.RAGA_NAME + " asc ");
       // Log.d(tag, "cursor = " + cursor);
        return cursor;
    }

    // if an object is visible to more than on thread, that object is accessed
    // via synchronized methods
    // cursor object is accessed by multiple threads
    public synchronized Cursor getRagaCursorByName(CharSequence constraint)
    {
        opnToRead(); // opening the db
        String whereClause =  RAGA_NAME +" like '' || ? || '%'";
        String [] whereArgs = {constraint.toString()};

        Cursor cursor
                = ragaDatabaseInstance.query(ragaDbHelper.TABLE_RAGA,
                    PROJECTION,whereClause,whereArgs,null,null,null);

        cursor.moveToFirst(); // this will point to the first row.
        while(cursor.moveToNext()) {
        }

        cursor.moveToPosition(-1);
        return cursor;


    }

    public synchronized Cursor getRagaCursorByRecentlyPlayed()
    {
        opnToRead(); // opening the db
      //  Log.d(tag, "get raga by recent list");
        // show only the top 5 records

        //query(tablename, columnname (String[]),
        // where clause KEY_ID+ ">" 0 or "=" + id (selection)
        // selectionargs[], groupby, havingby, orderby

        String orderByClause =  ragaDbHelper.LASTPLAYEDTIME +" DESC";
        String whereByClause = ragaDbHelper.LASTPLAYEDTIME ;
        String[] selectionArgs = null;
        String groupByClause = null;
        String havingByClause = null;
        Cursor cursor
                = ragaDatabaseInstance.query(ragaDbHelper.TABLE_RAGA,
                  PROJECTION,whereByClause + ">" + 0, selectionArgs,groupByClause,havingByClause,
                orderByClause,NUMDISPLAY);
        cursor.moveToFirst(); // I think I don't need this.
        while(cursor.moveToNext()) {

        }

        cursor.moveToPosition(-1);
        return cursor;



    }

    public synchronized Cursor getCursorByFavorites(){

        opnToRead(); // opening the db

        // show only the top 5 records
        //query(tablename, columnname (String[]),
        // where clause KEY_ID+ ">" 0 or "=" + id (selection)
        // selectionargs[], groupby, havingby, orderby

        String orderByClause =  ragaDbHelper.FAVORITES +" ASC";
        String whereByClause = ragaDbHelper.FAVORITES ;

        String[] selectionArgs = null;
        String groupByClause = null;
        String havingByClause = null;
        Cursor cursor
                = ragaDatabaseInstance.query(ragaDbHelper.TABLE_RAGA,
                PROJECTION,whereByClause + ">" + 0, selectionArgs,groupByClause,havingByClause,orderByClause);

        cursor.moveToFirst(); // I think I don't need this.
        while(cursor.moveToNext()) {
           //   Log.d("RagaListAdapter1", "raga name = " + cursor.getColumnIndex(RAGA_NAME));
        }

        cursor.moveToPosition(-1);
        return cursor;
    }


    public Raga getRagaObjectFromCursor(Cursor cursor)
    {
        Raga r = null;
        try
        {
            int idIndex = cursor.getColumnIndexOrThrow(ragaDbHelper.KEY_RAGA_ID);
            String ragName = cursor.getString(cursor.getColumnIndexOrThrow(ragaDbHelper.RAGA_NAME));
            String ragNum = cursor.getString(cursor.getColumnIndex(ragaDbHelper.KEY_RAGA_NUM));
            String melaNo = cursor.getString(cursor.getColumnIndex(ragaDbHelper.MELAKARTHA_NUM));
            String melaName = cursor.getString(cursor.getColumnIndexOrThrow(ragaDbHelper.MELAKARTHA_NAME));
            String aronotes = cursor.getString(cursor.getColumnIndex(ragaDbHelper.ARONOTES));
            String avonotes = cursor.getString(cursor.getColumnIndexOrThrow(ragaDbHelper.AVONOTES));
            String lastPlayedTime = cursor.getString(cursor.getColumnIndexOrThrow(ragaDbHelper.LASTPLAYEDTIME));
            int favorites = cursor.getColumnIndexOrThrow(ragaDbHelper.FAVORITES);
            r = new Raga(ragName,ragNum,melaName,melaNo,aronotes,avonotes,lastPlayedTime,favorites);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return r;
    }

    // static inner class
    public static class RagaCursor extends SQLiteCursor {

        public RagaCursor(SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(driver, editTable, query);

        }

        private static final String QUERY =
                "SELECT " + RAGA_NAME + ","
                + MELAKARTHA_NAME + " FROM "
                + TABLE_RAGA;
        private static final String QUERY1 =
                "SELECT  * FROM "
                        + TABLE_RAGA;

    }
}


