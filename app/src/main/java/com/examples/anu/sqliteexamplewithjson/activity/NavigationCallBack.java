package com.examples.anu.sqliteexamplewithjson.activity;

/**
 * Created by Anu on 6/16/15.
 * This is overridden in base activity
 *
 */
public abstract interface NavigationCallBack {

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */

        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);

}


