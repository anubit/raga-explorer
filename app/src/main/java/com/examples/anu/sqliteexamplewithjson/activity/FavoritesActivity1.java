package com.examples.anu.sqliteexamplewithjson.activity;

/**
 * Created by Anu on 8/15/15.
 */

import android.content.Context;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.app.LoaderManager;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.widget.AdapterView;
//import android.support.LoaderManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.util.SwipeDismissListViewTouchListener;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.util.LoaderRagaData;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;
import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter;
import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter1;
import com.examples.anu.sqliteexamplewithjson.util.SwipeToDeleteCursorWrapper;

// I have the loader here because if I delete or update
// certain raga names or mela names ( in the source) you want that update to show
// in the fav activities as well
public class FavoritesActivity1 extends BaseActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {


    // The adapter that binds our data to the ListView
    private RagaListAdapter1 mAdapter;
    private Context mContext;
    private RagaListAdapter favoritesAdapter;
    private ListView lvFavorites;
    private SharedPreferences pref;
    private PreferencesManager prefManager;
    private NavigationView navigationView;
    public static RagaDBDAO ragaDBInstance;
    public static final String FAV_RAGA = "FAVORITE_RAGA";
    public static final String ITEM_RAGA = "SELECTED_RAGA";

    // The loader's unique id. Loader ids are specific to the Activity or
    // Fragment in which they reside.
    private static final int LOADER_ID = 3;


    // The callbacks through which we will interact with the LoaderManager.
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    // I have the one singleton context
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = MySingleton.mContext;

        // This framelayout is from baseActivity where the favorites activity will be loaded
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame_fragment);

        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_favorites, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);
        mAdapter = new RagaListAdapter1(mContext,null,0);
        lvFavorites = (ListView) findViewById(R.id.lvFavorites);
        lvFavorites.setAdapter(mAdapter);

        ragaDBInstance = MySingleton.getRagaDBInstance();
        lvFavorites.setOnItemClickListener(listItemClickListener);

        prefManager = MySingleton.getPrefManagerInstance();
        pref = prefManager.getPref();
        navigationView = (NavigationView) findViewById(R.id.nvView);
        // prepare to initialize the loader
        // mCallbacks - A LoaderManager.LoaderCallbacks implementation, which the LoaderManager calls to report loader events.
        // If the loader specified by the ID does not exist, initLoader()
        // triggers the LoaderManager.LoaderCallbacks method onCreateLoader()
     /*   getLoaderManager().initLoader(LOADER_ID, null, mCallbacks);*/


        // Initialize the adapter. Note that we pass a 'null' Cursor as the
        // third argument. We will pass the adapter a Cursor only when the
        // data has finished loading for the first time (i.e. when the
        // LoaderManager delivers the data to onLoadFinished). Also note
        // that we have passed the '0' flag as the last argument. This
        // prevents the adapter from registering a ContentObserver for the
        // Cursor (the CursorLoader will do this for us!).
        // mAdapter = new RagaListAdapter1(this, R.layout.list_item,
        //       null, dataColumns, viewIDs, 0);
        //TODO: Instead of getAllRagas, if I pass the raganame in the bundle, cursor


        // Associate the (now empty) adapter with the ListView.
        // Refers to RagaListAdapter1
        //   lvFavorites.setAdapter(mAdapter);
        // Initialize the Loader with id '1' and callbacks 'mCallbacks'.
        // If the loader doesn't already exist, one is created. Otherwise,
        // the already created Loader is reused. In either case, the
        // LoaderManager will manage the Loader across the Activity/Fragment
        // lifecycle, will receive any new loads once they have completed,
        // and will report this new data back to the 'mCallbacks' object.
        // LoaderManager lm = getLoaderManager();
        // lm.initLoader(LOADER_ID, null, mCallbacks);

        /**
         * SETUP FOR SWIPEABLE LIST ITEMS:
         * copied from
         * https://github.com/romannurik/Android-SwipeToDismiss/
         * cursor wrapper ref:
         * ref: http://stackoverflow.com/questions/15468100/cursoradapter-backed-
         * listview-delete-animation-flickers-on-delete
         * ref: http://chopapp.com/#ifj74ch4
         */

        ListView listView = lvFavorites;
        // Create a ListView-specific touch listener. ListViews are given special treatment because
        // by default they handle touches for their list items... i.e. they're in charge of drawing
        // the pressed state (the list selector), handling list item clicks, etc.


        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        lvFavorites,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {

                                   // Log.d(tag,"inside the swipe dismiss list view touch listener");
                                    Cursor cursor;

                                    // 1. get the cursor, update the db, save it and then delete
                                    // the item using cursor wrapper class
                                    cursor = mAdapter.getCursor();

                                    Raga r = ragaDBInstance.getRagaObjectFromCursor(cursor);
                                    //Log.d(tag, "position = " + position);
                                    //Log.d(tag,"the raga to be removed = " + r.getRagaName());

                                    //2.update the db, save it
                                    ragaDBInstance.update(r.getRagaName(),-1);


                                    //TODO: for testing delete this if statement
                                    if (cursor == null)
                                        Log.d(tag,"cursor is null");
                                    else Log.d(tag,"cursor is not null cursor = " );

                                    // 3. Delete the ite,
                                    // The cursor position is swapped and the position is deleted
                                    SwipeToDeleteCursorWrapper swipeToDeleteCursorWrapper =
                                            new SwipeToDeleteCursorWrapper(cursor,position);

                                    int count = swipeToDeleteCursorWrapper.getCount();
                                    //Log.d(tag,"the count changed to " + count);
                                    MySingleton.favCount--;

                                    // If the fav count is 0 or -ve, we want to change
                                    // shared preferences of favorites to false as this
                                    // is set to reflect on Navigatin drawer.
                                    if (MySingleton.favCount <= 0) {
                                        prefManager.savePreferences("favoritesNotNull",false);
                                    }
                                    mAdapter.swapCursor(swipeToDeleteCursorWrapper);
                                    lvFavorites.setAdapter(mAdapter);

                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        });
        lvFavorites.setOnTouchListener(touchListener);
        // Setting this scroll listener is required to ensure that during ListView scrolling,
        // we don't look for swipes. basically you don't want to perform swipe operation
        // when the user is scrolling
        lvFavorites.setOnScrollListener(touchListener.makeScrollListener());
    }


    @Override
    protected void onStart() {
        super.onStart();
       // Log.d(tag,"on start of favorites activity");
        try {
            mCallbacks = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(getParent().toString()
                    + " must implement OnContactsInteractionListener");
        }
        Log.d(tag, "initialize the loader to load data to an activity asynchronously");
        // prepare to initialize the loader
        // mCallbacks - A LoaderManager.LoaderCallbacks implementation, which the LoaderManager calls to report loader events.
        // If the loader specified by the ID does not exist, initLoader()
        // triggers the LoaderManager.LoaderCallbacks method onCreateLoader()
        getLoaderManager().initLoader(LOADER_ID, null, mCallbacks);
    }


    @Override
    protected void onResume() {
        super.onResume();
       // Log.d(tag,"inside the onResume of Fav Activity 1");
        String item =  this.getIntent().getStringExtra(FAV_RAGA);
       // Log.d(tag," the data to load = " + item);
        Cursor cursor;

        if (item == null) {
            cursor = ragaDBInstance.getCursorByFavorites();
        }else
            cursor = ragaDBInstance.getRagaCursorByName(item);
        if (cursor == null)
            Log.d(tag,"cursor is null");
        else Log.d(tag,"cursor is not null cursor = " + cursor);
        // Log.d(tag,"cursor data inside on resume of favorites = " + cursor.getColumnIndex(cursor.getColumnName(8)));
        mAdapter = new RagaListAdapter1(mContext,cursor,0);
        lvFavorites.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        // Changing the pref will show the "FAVORITES" title in navigation drawer
        prefManager.savePreferences("favoritesNotNull",true);


        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
       // android.app.ActionBar ab = getActionBar();

        // Enable the Up ripple_button
        // This will add a left facing caret along the app icon and enables
        // it as an action ripple_button. when the user presses it call is made
        // to onOptionsSelected()
        // The ID for the action is android.R.id.home.
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        ab.setTitle("Favorites");

    }



    @Override
    protected void onRestart() {
        super.onRestart();
       // Log.d(tag,"on Restart of favorites activity");
    }

    // If I want use the loader for the search activity, update the UI, preserve the orientation
    //
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new LoaderRagaData(mContext);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursorData) {
        // A switch-case is useful when dealing with multiple Loaders/IDs
        // switch (loader.getId()) {
        // case LOADER_ID:
        // Log.d(tag,"the loader id is = " + loader.getId());
        // Log.d(tag," the id = " + LOADER_ID);
        // mAdapter.swapCursor(cursorData);
        mAdapter.notifyDataSetChanged();
        prefManager.savePreferences("favoritesNotNull",true);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.d(tag, "on pause of favorites activity");
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.d(tag, "on stop of favorites activity");

    }

    // On Back pressed (at the bottom) should take you to previous screen not navigation drawer
    @Override
    public void onBackPressed(){
        // Log.d(tag,"On back pressed of favorites activity");
        // Log.d(tag,"mdrawer layout in search = " + this.mDrawer.toString());
        // This should take the up ripple_button navigation to main screen as mentioned in the
        // manifest.xml
        NavUtils.navigateUpFromSameTask(this);
        super.onBackPressed();
    }

    // call the detail activity
    public AdapterView.OnItemClickListener listItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(tag, "inside the on item selected part, call the detail, pass the selected raga");
                    // Gets the Cursor object currently bound to the ListView

                    final Cursor cursor = mAdapter.getCursor();
                    // Moves to the Cursor row corresponding to the ListView item that was clicked
                    cursor.moveToPosition(position);
                    // Creates a raga lookup
                    Raga dataToSend = new Raga();
                    dataToSend = ragaDBInstance.getRagaObjectFromCursor(cursor);
                    Intent detailActivityIntent1 = new Intent(getApplicationContext(),DetailActivity.class);
                    Bundle b = new Bundle();
                    b.putParcelable(ITEM_RAGA,dataToSend);
                    //attaching a bundle which is a raga object. Raga is a parcelable object
                    detailActivityIntent1.putExtras(b);
                    startActivity(detailActivityIntent1);

                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    // Up ripple_button is android.R.id.home
    // The superclass method responds to the Up selection by navigating to the parent activity,
    // as specified in the app manifest.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                // return true;
                break;
            case R.id.action_settings:
                break;
        }
        //Not sure if
        /*
        if (prefManager.getKeyFav()) {
            Log.d(tag, "inside selectDrawerItem - enable the menu = " + prefManager.getKeyFav());
            Log.d(tag,"navigation view menu item = "+ navigationView.getMenu().getItem(2).toString());
            navigationView.getMenu().getItem(2).setEnabled(true);
        }
        else navigationView.getMenu().getItem(2).setEnabled(false);
        */

        return super.onOptionsItemSelected(item);
    }






}

