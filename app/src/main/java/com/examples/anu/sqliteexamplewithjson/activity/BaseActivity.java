package com.examples.anu.sqliteexamplewithjson.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
//import android.widget.Toolbar;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.R;

/**
 * Created by Anu on 6/2/15.
 */
// Navigation Activity
// ref: http://blog.grafixartist.com/easy-navigation-drawer-with-design-support-library/
public  class BaseActivity extends AppCompatActivity
        implements NavigationCallBack {

    public static String tag = "Base Activity";
    public static boolean searchFirstTime = false;

    public static final String CLOSE_NAV_DRAWER = "CLOSE_NAV_DRAWER";

    private android.support.v7.app.ActionBar actionBar;
    public DrawerLayout mDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    private NavigationView nvDrawerView;
    private SharedPreferences pref;
    private PreferencesManager prefManager;
    private boolean mCloseNavDrawer;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;

    //http://antonioleiva.com/navigation-view/

    // steps for navigation view
    //1 create a menu in menu( see drawer_view.xml) - only one menu is selected at a time
    //2 add the navigation view to activity layout. ( activity_base.xml with drawerlayout group)
    //3  add the tool bar as action bar
    //4. initialize the drawer

    /* I have used the menu to display the list of navigation items.
     * supportActionBar to support prev action bar version. V19 and above has tool bar
     */
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        // get the saved value of navigation drawer
        if (savedInstanceState != null) {
            this.onBackPressed();
            mCloseNavDrawer = savedInstanceState.getBoolean(CLOSE_NAV_DRAWER);
        }

        // 3. As we're using a Toolbar, we should retrieve it and set it
        // to be our ActionBar
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        Log.d(tag,"inside on create of base activity title = " + getTitle());
        // current title
        mTitle = mDrawerTitle = getTitle();
        setTitle(mTitle);

        //enable the app icon to navigate one level up when pressed. we can
        // monitor this in onOptionsItemSelected() method
        if (actionBar != null ) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            // we enable the app icon so it appears in the action bar
            actionBar.setHomeButtonEnabled(true);
        }


       // Now retrieve the DrawerLayout so that we can set the status bar color.
       // This only takes effect on Lollipop, or when using translucentStatusBar
       // on KitKat.
        // In order for the hamburger icon to animate to indicate the drawer
        // is being opened and closed, we need to use the ActionBarDrawerToggle class.
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = setupDrawerToggle();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDrawer.setStatusBarBackgroundColor(getResources().getColor(R.color.primary_light));
        }
            // Ties DrawerLayout events to the ActionBarToggle
        mDrawer.setDrawerListener(mDrawerToggle);

        //find our drawer view - menu_
        nvDrawerView = (NavigationView) findViewById(R.id.nvView);

        // Setup drawer view and make home as the default highlighted item
        onNavigationViewListener(nvDrawerView);
        nvDrawerView.getMenu().getItem(0).setChecked(true);

        prefManager = MySingleton.getPrefManagerInstance();
        pref = prefManager.getPref();

        mDrawer.closeDrawers();
    }

    private void onNavigationViewListener(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    //http://stackoverflow.com/questions/26833741/hide-navigation-drawer-when-user-presses-back-ripple_button

    public void setDrawerClosed(View drawerView,int gravity) {

        if (mDrawer.isDrawerVisible(gravity) && (mDrawer.isDrawerOpen(gravity)) )
        {
            Log.d(tag,"drawer view is visible and open");
            mDrawer.closeDrawer(GravityCompat.START);
        }
        else {
            throw new IllegalArgumentException("View " + drawerView + " is not a sliding drawer");
        }

        // move drawer directly to the closed position
        mDrawerToggle.onDrawerSlide(drawerView, 0.f);
        mDrawerToggle.onDrawerClosed(drawerView);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        actionBar.setTitle(mTitle);
    }

    // menu items set in drawer_view.xml
    public void selectDrawerItem(MenuItem menuItem) {
        switch(menuItem.getItemId()) {

            case R.id.nav_main_fragment:

                mDrawerTitle = getTitle();
                setTitle(mDrawerTitle);
                Intent mainActivityIntent = new Intent(this,
                        MainActivity.class);
                startActivity(mainActivityIntent);
                break;
            case R.id.nav_first_fragment:
                searchFirstTime = true;
                // This "  mDrawer.closeDrawers()" leaves the animation
                //  ref: http://stackoverflow.com/questions/26833741/hide-navigation-drawer-when-user-presses-back-ripple_button
               // mDrawer.closeDrawers();
                Intent searchActivityIntent = new Intent(this,
                        SearchActivity.class);
                mDrawerTitle = "Search";
                // This sets the mTitle as well tht preserves the current title when closed
                setTitle(mDrawerTitle);
                startActivity(searchActivityIntent);

                break;
            case R.id.nav_second_fragment:

                Intent favoritesActivityIntent = new Intent(this,
                        FavoritesActivity1.class);
                mDrawerTitle = "Favorites";
                setTitle(mDrawerTitle);
                startActivity(favoritesActivityIntent);
                break;
            case R.id.nav_third_fragment:
                // settings
                Intent settingsActivityIntent = new Intent(this,
                        SettingsActivity.class);
                mDrawerTitle = "Settings";
                setTitle(mDrawerTitle);
                startActivity(settingsActivityIntent);
                break;

            default:
                break;
        }

        // Highlight the selected item, update the title(done in each item above) and close the drawer
       menuItem.setChecked(true);
       mDrawer.closeDrawer(nvDrawerView);

    }


    public void restoreActionBar() {
        Log.d(tag,"inside restore action bar called onCreateOptionsMenu()");
        actionBar.setDisplayShowTitleEnabled(true);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(CLOSE_NAV_DRAWER, mCloseNavDrawer);
        super.onSaveInstanceState(savedInstanceState);
    }


    // set a boolean in your activity when you navigate away, indicating the drawer should be closed:
    @Override
    protected void onResume() {
        super.onResume();
        mTitle = mDrawerTitle = getTitle();
        setTitle(mTitle);
        }


    // this function inflates the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return super.onCreateOptionsMenu(menu);
    }



    // The action bar items are selected here.
    // I have added the search icon on the main screen, settings screen
    // we have enabled the app icon to be used as a home ripple_button, we can listen for
    // when it's clicked here
    // we are using the action bar drawer toggle so we call the actionbardrawer's toggle
    // on optionsItemSelected
    // return true means that the app icon on the action bar had been pressed and everything
    // has been taken care of. leave
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.closeDrawer(nvDrawerView);
                mDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_search:
                searchFirstTime = true;
                mDrawerTitle = "Search";
                setTitle(mDrawerTitle);
                Intent searchActivityIntent = new Intent(this,
                        SearchActivity.class);
                startActivity(searchActivityIntent);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   //we need to make sure we synchronize the state whenever the screen is restored or
   // there is a configuration change (i.e screen rotation):
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // it synchronizes the state of the drawer indicator and the drawer its' linked to
        mDrawerToggle.syncState();
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
       mDrawerToggle =  new ActionBarDrawerToggle(this, /* host activity */
                mDrawer,/* DrawerLayout object */
               R.string.drawer_open,/* "open drawer" description */
                R.string.drawer_close) /* "close drawer" description */

            {
                /**
                 * Called when a drawer has settled in a completely open state.
                 */
                //changed getActionBar() to getSupportActionBar for 21
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    setTitle(mTitle);
                    if (prefManager.getKeyFav() && (MySingleton.favCount > 0)) {
                        nvDrawerView.getMenu().getItem(2).setEnabled(true); // enable the favorites if fav screen has value
                    }
                }

                /**
                 * Called when a drawer has settled in a completely closed state.
                 */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    setTitle(mTitle);
                    if (prefManager.getKeyFav()  && (MySingleton.favCount > 0 ) ) {
                        nvDrawerView.getMenu().getItem(2).setEnabled(true); // enable the favorites if fav screen has value
                    }
                }

        };
        return mDrawerToggle;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        nvDrawerView.getMenu().findItem(position).setChecked(true);

    }
}