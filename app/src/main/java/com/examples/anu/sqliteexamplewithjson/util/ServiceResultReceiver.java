package com.examples.anu.sqliteexamplewithjson.util;

/**
 * Created by Anu on 12/14/15.
 */

import android.os.Parcel;
import android.os.ResultReceiver;
import android.os.Bundle;
import android.os.Handler;


/**
 * Created by Anu on 5/30/15.
 */

// a generic receiver that receives messages(status) from IntentService
// Result Receiver implements Parceable.
public class ServiceResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public ServiceResultReceiver(Handler handler) {
        super(handler);

    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }



    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultBundle);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        //super.onReceiveResult(resultCode, resultData);
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }

    }
}
