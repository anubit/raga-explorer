package com.examples.anu.sqliteexamplewithjson.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.examples.anu.sqliteexamplewithjson.MyApplication;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * This class is our utility belt. It contains all static functions which do not
 * fit anywhere else.
 *
 *
 */
public class Utils {

    protected static final String TAG = Utils.class.getSimpleName();
    public static int[] resourceIDArray;

    private  static int totalBytes = 0;
    private static int shruthiTotalBytes = 0;
    public static int totalBytesRestOfTrack = 0;
    public static int WAVEFILES = 25;

    // 1 sample is 1 short which is 2 bytes
    private static AssetManager assetManager;
    private static Float[] microSecNote = new Float[WAVEFILES];
    private static int [] lengthWavFiles = new int[WAVEFILES]; // no of notes atmost 20
    private static short[] shruthiShortByteBuffer;
    private static short[] restOfNotesShortByteBuffer; // adding 1 sample of restofnotes to shruthi sample
    //  private static boolean shruthiOn =
    private static String[] wavFileNames;
    private static  byte[] finalBufferData,finalBuffer;
    private static byte[] shruthiBufferData;



    // The cumulative Dur and its count is static so that aro and avarog are calculated
    // based on one array
    private static int countCumulativeDur = 0;
    private static float[] cumulativeDur = new float[20];
    private static int  prevLenDurArray = 0;
    private static float prevDuration = 0;

    public static LinkedHashMap hashMapDuration; // hashmap with duration and the filename(note)


    // TODO: do I make one larger buffer data of all the notes in short
    // todo: do I create just add dynamically

    /*
     * Create an output file from raw resources.
     *
     * @param outputFile
     * @param context
     * @param inputRawResources
     * @throws IOException
     */




    // public static void populateOutputFile(String filename,
    //                                        Context baseContext, int[] inputRawResources)

    // open the shruthi file and extract the bytes
    // convert from byte buffer to short buffer --- done
    // extract bytes from the rest of the notes into byte buffer -- done
    // convert the rest of the notes into short byte buffer -- done
    // create an output buffer, where you read the shruthi buffer and notes buffer and add them -- done


    // open up shruthi wav file and copy into a shruthi file
    // helps to find the total bytes of shruthi

    public static void populateShruthi(Context context,String pitch) {
        InputStream shruthiInputStream;

        int bytesRead = 0;
        shruthiTotalBytes = 0;
        int i = 0;

        assetManager = context.getAssets();
        String filename = pitch.toLowerCase() + "shruti.wav";

        try {
            shruthiInputStream = assetManager.open(filename);
            int shruthiSize = shruthiInputStream.available();
            shruthiBufferData = new byte[shruthiSize];
            shruthiInputStream.skip(46); //skip header data

            //  read the rest of the bytes
            bytesRead = shruthiInputStream.read();
            while (bytesRead  !=  -1) {
                shruthiBufferData[i] = (byte)bytesRead;
                i++;
                bytesRead = shruthiInputStream.read();

            }// while
        } catch (IOException e) {
            Log.d("Utils", " not able to open shruthi.wav");
            Log.d("Utils", "not able to convert byte to short");
        }

        shruthiTotalBytes = i;
        shruthiShortByteBuffer = new short[shruthiTotalBytes / 2];
        shruthiShortByteBuffer = convertByteToShort(shruthiBufferData);
    }



    public static void populateOutputFile(String filename, Context context,
                                          LinkedHashMap dataAroMap,
                                          LinkedHashMap dataAvoMap,boolean shruthiValue)
            throws IOException {

        RandomAccessFile f = new RandomAccessFile(filename, "rws");
        f.setLength(0);
        assetManager = context.getAssets();

        int bytesRead = 0;
        byte[] headerData = new byte[46];
        boolean firstTime = true;

        String[] listFileNames;
        InputStream inputStream;
        int j = 0;

        finalBufferData = new byte[5000000]; // 5MB
        if (dataAroMap != null) {
            LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>(dataAroMap);
            //tree map preserves the order
            //  TreeMap<Integer, String> treeMapSorted = new TreeMap<Integer, String>(dataAroMap);
            Set<Map.Entry<Integer, String>> mappings = map.entrySet();

            for (Map.Entry<Integer, String> entry : mappings) {

                int index = entry.getKey();
                String fileName = entry.getValue();
                inputStream = assetManager.open(fileName + ".wav");
                int largerDataSize = inputStream.available();

                // copy header data 46 bytes for the first track
                // for the rest of the track skip 46 bytes
                // copy the data into the output file
                // length of the file in bytes is little endian: 40,41,42,43
                try {
                    if (firstTime) {
                        inputStream.read(headerData);
                        f.write(headerData);
                        firstTime = false;
                    } else {
                        inputStream.skip(46);
                    }
                } catch (IOException e) {
                    Log.d("Utils", "cannot read the header data");
                }

                bytesRead = inputStream.read();

                while (bytesRead  !=  -1) {
                    finalBufferData[j++] = (byte) bytesRead;
                    totalBytes++;
                    bytesRead = inputStream.read();
                }
                inputStream.close();


            }// for (Map.Entry<Integer, String> entry : mappings)
            totalBytes = j;
            //  Log.d("Utils","total bytes read so far in aro map = " + totalBytes);
        }//  if (dataAroMap != null)

        // repeat the same for avomap

        if (dataAvoMap != null) {
            // For Avo I don't want sort by keys, I'm using LinkedHashMap
            LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>(dataAvoMap);
            Set<Map.Entry<Integer, String>> mappings = map.entrySet();
            // for (Map.Entry<Integer, String> entry : treeMapSorted.entrySet()) {
            for (Map.Entry<Integer, String> entry : mappings) {

                int index = entry.getKey();
                String fileName = entry.getValue();

                inputStream = assetManager.open(fileName + ".wav");
                int largerDataSize = inputStream.available();

                // skip header data of 46 bytes as we are going to overwrite the size
                // of the header data in our finalOutputBuffer
                // copy the data into the output file
                // length of the file in bytes is little endian: 40,41,42,43

                inputStream.skip(46);

                bytesRead = inputStream.read();
                while ((bytesRead != -1 )) {
                    finalBufferData[j++] = (byte) bytesRead;
                    totalBytes++;
                    bytesRead = inputStream.read();
                }
                inputStream.close();
            } // //   for (Map.Entry<Integer, String> entry : mappings)
        }

      //  f.seek(46); // ANu added now to test
      //  f.write(finalBufferData);
        restOfNotesShortByteBuffer = new short[totalBytes / 2];
        restOfNotesShortByteBuffer = convertByteToShort(finalBufferData);
        finalBuffer = new byte[totalBytes];

        if (shruthiValue) {
            // add the shruthi and rest of notes
            finalBuffer = new byte[totalBytes *2];
            short[] shruthiCombined = new short[totalBytes];
            shruthiCombined = addShruthiToNotes();
            finalBuffer = convertShortToByte(shruthiCombined);

        } else {
            finalBuffer = convertShortToByte(restOfNotesShortByteBuffer);

        }
        f.seek(46);
        f.write(finalBuffer);
    }



    public static byte[] readDataFromOutputFile(String filename)
    {
        Log.d("Utils","read data from output file function, file = " + filename);

        byte[] tempBufferData = new byte[totalBytes];
        RandomAccessFile f = null;
        int i = 0;

        Log.d("Utils", "inside read data from output file");
        try {
            f = new RandomAccessFile(filename, "rws");
            //skip header
            f.seek(46);
        }catch (IOException e) {
            Log.d("Utils", "inside readData - cannot read the header data");
        }


        while (f != null) {
            try {
                tempBufferData[i++] = (byte) f.read();

            }catch (IOException e) {
                Log.d("Utils", "inside readData - cannot read a byte");
            }
        }
        return tempBufferData;


    }


    // The microsec Array is useful for highlighting part
    // read each wav file.
    // skip the header for each wav file. ie. go to 40th byte, read the 4 bytes, get its file size.
    // combine all the wav files length as one integer -

    // This function will calculate the microsec duration of all the wav files not
    // just user's data
    public static void genericDurationMicroSec() {

        wavFileNames = new String[WAVEFILES];
        String[] assetFileNames = new String[WAVEFILES];
        InputStream inputStream;

        AssetManager assetManager = MyApplication.getAppContext().getAssets();
        try {

            assetFileNames = assetManager.list("");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int counter = 0;
        byte[] buffer = new byte[6]; // read only 4 bytes 40,41,42,43
        // open each wav file
        // skip the header and go directly to the part that contains
        // the length of the file. The length is in 40, 41,42,43

        // get the file size length for each wav file and store in an array
        int wavFileLength = 0;
        String extension;
        for (String fileName : assetFileNames) {
            // extract only the wavfiles
            extension = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (extension.equalsIgnoreCase("wav")) {
                wavFileNames[counter] = fileName;
                try {
                    inputStream = assetManager.open(fileName);
                    inputStream.skip(40); // skip 40 bytes
                    int bytesRead = inputStream.read(buffer);
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // extract each byte from the buffer and convert to an integer
                wavFileLength = (int) (buffer[3] & 0xff);
                wavFileLength =  (wavFileLength << 8)  | (int) (buffer[2] & 0xff);
                wavFileLength = (wavFileLength << 8) | (int) (buffer[1] & 0xff);
                wavFileLength = (wavFileLength << 8) | (int)(buffer[0] &0xff);
            }
            lengthWavFiles[counter] = wavFileLength;
            counter++;
        } // end of for (String fileName : wavFileNames)

        // go through the bytes file 2 bytes per sample
        // 1 sec = 16100 khz = samples as per my nexus 7 recording
        // bytes per sample = no of samples
        // convert  from sec to micro sec for duration, because sound touch
        // takes micro sec

        // convert the secs to micro secs 1 sec = 10^6 micro sec
        // convert the no of samples /sec = samples /micro secs
        // 1 sec = 16100 samples
        // 1/16100 sec = 1 sample
        // 1/16100 * 10^6 micro secs = 1 sample
        // 1000/16 = 62.5 micro sec = 1 sample

        // calculating microsec part
        microSecNote = new Float[lengthWavFiles.length];
        int numSamples = 0;
        // convert the bytes to samples, then from samples/sec to samples/microsec
        // for each wav file, get the bytes, calculate samples
        for (int i = 0; i < wavFileNames.length; i++) {
            //  for (String wavFile:wavFileNames)
            if (wavFileNames[i] != null) {
                // 2bytes per sample
                numSamples = (int) lengthWavFiles[i] / 2;
                microSecNote[i] = (float) (numSamples * 62.5);

            }
        }
    }

    // Microsec array contains the duration in microsec for each note
    // map the duration to a note to highlight the note
    // For Highlighting: Function that creates a hashamap(duration, filename)
    public static LinkedHashMap createDurationIndexHashMap() {
        hashMapDuration = new LinkedHashMap(microSecNote.length);
        for (int i = 0; i < wavFileNames.length; i++) {
            if (wavFileNames[i] != null) {
                String fileName = wavFileNames[i].substring(0,(wavFileNames[i].lastIndexOf("."))  );
                hashMapDuration.put(fileName,new Float(microSecNote[i]));
            }
        }
        return hashMapDuration;
    }

    // Aromaps and AvoroMaps are represented as : aromap (index,string)=> aromap(0,sa)
    // microsecmap(string,float duration)=> microsecMap(sa,duration)
    public static float[] calculateCumulativeDuration(LinkedHashMap hashMap,boolean done) {

        float duration = 0;
        // Tree map to get the order.
        TreeMap<Integer,String> treeMapSorted = new TreeMap<Integer,String>(hashMap);
        HashMap<String, Float> microSecMap = hashMapDuration; // THis map contains the generic duration

        // for each note from aromap or avomap
        // calculate cumulative duration
        // From microsecHashMap retrieve the microsec duration and add it
        // to final duration
        if (treeMapSorted != null) {

            for (Map.Entry<Integer, String> entry : treeMapSorted.entrySet()) {
                duration = microSecMap.get(entry.getValue());

                if (countCumulativeDur == 0) {
                    cumulativeDur[countCumulativeDur] = duration;

                } else {
                    cumulativeDur[countCumulativeDur] = prevDuration + duration;
                }
                prevDuration = cumulativeDur[countCumulativeDur];
                countCumulativeDur++;
            }
        }
        prevLenDurArray = prevLenDurArray + countCumulativeDur;
        if (done == true){
            countCumulativeDur = 0;
        }
        return cumulativeDur;
    }
    public static int big2littleEndianConverter(int x) {
        int byte0, byte1, byte2, byte3, output;

        byte0 = x & 0xff;
        byte1 = (x >> 8) & 0xff;
        byte2 = (x >> 16) & 0xff;
        byte3 = (x >> 24) & 0xff;

        output = byte0 << 24 + byte1 << 16 + byte2 << 8 + byte3;
        return output;
    }

    //  This will rewrite the length  of the outputfile in the header.
    // I calculated the total data length minus the header data in totalbytes
    // write that length in byte 40 of the output file
    public static void setLengthOutputFile(String filename) {
        int byte0, byte1, byte2, byte3, output;

        try {
            RandomAccessFile f = new RandomAccessFile(filename, "rws");
            f.seek(40);

            // take the totalBytes which is an int and we want to extract the byte by byte
            // from this int.
            // writeInt function didn't give us the correct format and we got our song looping.
            // we decided to extract byte by byte
            byte0 = (totalBytes & 0xff);
            f.writeByte(byte0);
            byte1 = (totalBytes >> 8) & 0xff;
            f.writeByte(byte1);
            byte2 = (totalBytes >> 16) & 0xff;
            f.writeByte(byte2);
            byte3 = (totalBytes >> 24) & 0xff;
            f.writeByte(byte3);

            // f.writeInt(big2littleEndianConverter(totalBytes));
            f.close();
        } catch (IOException e) {
            Log.d("Utils", e.toString());
        }

    }


    // changed the logic from storing the resourceid in an array to filenames
    public  static String[] listAssetFilesNames() {
        String[] list = null;
        try {
            // list(String path) - relative folder to the path within the assets
            list = assetManager.list("");
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    // little endian
    // String.format is used to print the value in hex - there was no need to do the masking with 0xff
    // decimal to hex: divide by 16. 2's complemnet for negative nos/
    // 2's complement - ve nos ( convert the given number 1 to 0s and 0s to 1s and add +1 at the end)
    // group 4 at a time gives you hex.
    // to convert hex to decimal multiply by 16 powers
    private static short[] convertByteToShort(byte[] byteBuffer) {
        short[] shortArray = new short[byteBuffer.length/2];
        for (int i = 0, j = 0; i < byteBuffer.length; i += 2, j++) {
            //  Log.d("Utils", "bytebuffer[" + i + "] = " + (byteBuffer[i] & 0xFF) + " bytebuffer[i+1] & 0xff =" + (byteBuffer[i + 1]));

            short lsb = (short) byteBuffer[i];
            short msb = (short) byteBuffer[i + 1];
            //Log.d("Utils", " after convertion lsb value = " + lsb + " msb =  " +
            //      (msb));
            shortArray[j] = (short) ( (lsb & 0xff) | (msb << 8));
            //  Log.d("Utils", "the convert byte to short the value " + "j = " + shortArray[j]);
        }
        return shortArray;
    }

    // 1. get the short 16 bits
    // 1st 8 bits - msb
    private static byte[] convertShortToByte(short[] shortByteBuffer)
    {

        byte[] byteArray = new byte[shortByteBuffer.length *2];
        for (int i=0, j = 0; j< shortByteBuffer.length; i+=2, j++)
        {
            // Log.d("Utils"," short buffer = " + shortByteBuffer[j]);
            byte lsb = (byte) (shortByteBuffer[j] & 0xff);
            //  Log.d("Utils", "lsb = " + lsb);
            byteArray[i] = lsb;
            byte msb = (byte) ((shortByteBuffer[j]>>8) & 0xff);
            byteArray[i+1] = msb;
        }
        return byteArray;

    }

    private static short[] addShruthiToNotes( )
    {

        int lenShruthi = shruthiShortByteBuffer.length;
        int lenRest = restOfNotesShortByteBuffer.length;

        //  Log.d("Utils", " rest of notes length: inside add shruthi= " + lenRest);
        //Allocation is based on the largest byte array size:
        // either shruthi byte array can be bigger or the rest of the notes
        int size = 0;
        size = lenRest;

        // Log.d("Utils", " full size length = " + size);
        short[] shruthiWithNotes = new short[size];

        for (int  j = 0;  j < lenRest; j++)
        {
            shruthiWithNotes[j] = (short) ( ( shruthiShortByteBuffer[j % lenShruthi] >> 1) +
                    ( restOfNotesShortByteBuffer[j] >> 1) );
        }
        return shruthiWithNotes;

    }

    public static float pitchToFreq(String pitch) {
        float freq = 0;
        switch(pitch) {
            case "A": freq = 3.0f;
                break;
            case "C": freq=262;
                break;
            default: freq = 440;
                break;
        }
        return freq;
    }
}













