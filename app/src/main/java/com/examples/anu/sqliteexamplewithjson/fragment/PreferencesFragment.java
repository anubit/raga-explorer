package com.examples.anu.sqliteexamplewithjson.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.examples.anu.sqliteexamplewithjson.GetRagaTask;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.ReturnDataFromAsyncTask;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 5/21/15.
 */

//TODo: even though it is called preferences fragment this should be favorites
// TODO; handle data is still attached to activity, change that to implement otto
// http://www.androidhive.info/2013/11/android-sliding-menu-using-navigation-drawer/
// mistakes learned: since i'm using drawer layout with fragments, drawerlayout
// can have only two children; one is the listview drawer, and the other one
// is main content view, which in my case is the fragment.
// so  the fragment contains the actual layout to be displayed. and you want
// create a root view which ref to the fragment.xml, in turn use the root view
// to get the elements in the view.

// I'm using the async task to load the items in the database. I need a ref to context
// in the async task. to get the context, get the activity and assign it to context.
// pass it to the fragment, and the async task can use it.
// mistakes: it took some time to figure that if you have a frame layout and relative
// layout and the list view for the drawer, then they are 3 children.

// TODO: load the database part into intent service
// todo:
public class PreferencesFragment extends Fragment implements ReturnDataFromAsyncTask {
    private TextView sampleText;
    public static String tag = "preferences fragment";

    private ListView lvNames;
    private RagaListAdapter mAdapter;
    private ArrayList<Raga> listRaga;
    public ReturnDataFromAsyncTask returnData = null;
    GetRagaTask getAsyncTask;
    private Context context;


    public PreferencesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      // this context is used in async task
        context = getActivity();
        setHasOptionsMenu(true);
    }

    // params(layout file, parent viewgroup, if the xml is inflated directly
    // or via the code sometime later)
    // the parent viewgroup is inside the activity that will host the fragment view.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_preferences, container, false);
       // inflater.inflate(R.layout.fra)

        lvNames = (ListView) rootView.findViewById(R.id.lvRagaNames);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listRaga = new ArrayList<>();
        // the first parameter changed from context to activity
        mAdapter = new RagaListAdapter(getActivity(),listRaga);
        lvNames.setAdapter(mAdapter);

        // changed from activity to preference fragment
        getAsyncTask = (GetRagaTask) new GetRagaTask(context);
        getAsyncTask.returnData = this;
        getAsyncTask.execute();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    // @Subscribe
    @Override
    public void handleReturnData(List<Raga> list){
      //  Log.d(tag, " the list size in handleReturnData " + list.size());
        //  listRaga = list; //assign new data
      //  Log.d(tag," the list size in handleReturnData " + list.size());
        mAdapter.addAll((ArrayList)list);
        mAdapter.notifyDataSetChanged();

    }


}
