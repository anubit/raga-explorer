package com.examples.anu.sqliteexamplewithjson.db;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 5/13/15.
 */
// This class is for the GSon can dump all the data from json
// the second paramenter to GSOn is a class name
public class RagaList  {
    // the serializedName is mentioned in document.json document
    @SerializedName("raga_array")
    public List<Raga> listOfRagas;

    public RagaList()
    {
        listOfRagas = new ArrayList<Raga>();
    }
}
