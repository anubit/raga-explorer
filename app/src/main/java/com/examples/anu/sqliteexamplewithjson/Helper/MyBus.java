package com.examples.anu.sqliteexamplewithjson.Helper;

import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Anu on 5/18/15.
 */
// Purpose of otto:
//For most applications, you will only need to use one Bus object throughout yo
// Once a new instance of the Bus() is created; we can start posting and subscribe
// throughout application.

//Intents are a strict means of communicating between these components.
//The Otto Event Bus allows us to send and receive event messages throughout all
//without the restriction of Intents or direct object references.
//In addition, we often make use of interfaces for Fragment-to-Activity or Fragm
//communication.
//Using an Event Bus can free us from the strict object coupling created by imple
//in our components.
//http://www.cardinalsolutions.com/blog/2015/02/event-bus-on-android



//the AsyncTask has an implicit reference to the enclosing Activity.
// If a configuration change happens the Activity instance that started the AsyncTask
// would be destroyed, but not GCd until the AsyncTask finishes.
// Since Activities are heavy this could lead to memory issues if several AsyncTasks are started.
// Another issue is that the result of the AsyncTask could be lost,
// if it's intended to act on the state of the Activity.

// solves 2 issues:
// This leads to two issues we have to fix:
// - Ensuring the Activity isn't kept in memory when destroyed by the framework
//- Ensuring the result of the AsyncTask is delivered to the current Activity instance

public class MyBus
{
    private static Bus instance = null;

    private MyBus()
    {
        instance = new Bus(ThreadEnforcer.ANY);
    }

    public static Bus getInstance()
    {
        if(instance == null)
        {
            instance = new Bus(ThreadEnforcer.ANY);
        }
        return instance;
    }
}



