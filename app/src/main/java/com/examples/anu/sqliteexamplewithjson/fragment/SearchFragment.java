package com.examples.anu.sqliteexamplewithjson.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.activity.DetailActivity;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;
import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter1;
import com.examples.anu.sqliteexamplewithjson.util.DelayAutoCompleteTextView;
import com.examples.anu.sqliteexamplewithjson.util.LoaderRagaData;
import com.examples.anu.sqliteexamplewithjson.util.ServiceResultReceiver;

import java.util.ArrayList;

import static android.graphics.Color.TRANSPARENT;

/**
 * Created by Anu on 5/30/15.
 */

/* Search Fragment class has a cursorloader.
 * cursorLoader loads data asynchoronously from a content provider(sqlite database)
 * They can monitor the source of the data and deliver new results when the content changes.
 * If the result is retrieved by the Loader after the object has been disconnected from its parent
 * (activity or fragment), it can cache the data.
 * They also persist data between configuration changes.
 * methods: extends Fragment or fragmentActivity implements Loader<callbacks>
 * initLoader in onCreate or onCreateView
 * onCreateLoader is called once the initLoader starts the query: result: cursor from onCreateLoader
 * display the cursor data via cursorAdapter. (RagaListAdapter1)
 *
 * Initialize the Loader with id '1' and callbacks 'mCallbacks'.
 * If the loader doesn't already exist, one is created. Otherwise,
 * the already created Loader is reused. In either case, the LoaderManager will manage the Loader
 * across the Activity/Fragment lifecycle, will receive any new loads once they have completed,
 * and will report this new data back to the 'mCallbacks' object.
 * initLoader(int id, Bundle args, LoaderCallbacks< D > callback)
 */

// ref to autocomplete text view
// http://makovkastar.github.io/blog/2014/04/12/android-autocompletetextview-with-suggestions-from-a-web-service/
// bug fix took time to figure it out:
// autocompletetext view: since I'm using a list I need an adapter.
// In that adapter I want to use the filter based on the constraint
// If there is delay between the previously typed and the current
// the search will be performed on the current. custom "delayautocompletetextview"


// Changed from array adapter to cursor adapter. You can directly load from the database to list
    // view using cursor.
// I have a static ref of arraylist that contains melakartha and raganame - listRagaRef.
// In the adapter: I will load the list (entire listRagaRef or the filtered list)
// But in the adpater you have pass an empty arraylist to actually display the filtered
// list. THis took me some time to figure this out


// I need to clear the arraylist in search every single time
// also after the delay has to be reduced
// clear the text box
// on resume of the search fragment should bring back to main activity not base activity
// add the navigation bar on search activity
// on click of list item of search


public class SearchFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor>{

    private DelayAutoCompleteTextView actv;
    private Button btnSearch;
    public static String tag = "search fragment";

    public static RagaDBDAO ragaDBInstance;
    private ListView lvNames;
    // The adapter that binds our cursor data to the ListView
    private RagaListAdapter1 mAdapter;
    public static  ArrayList<Raga> listRagaRef;

    private Context mContext;
    private ServiceResultReceiver mReceiver;
    public static final String ITEM_RAGA = "SELECTED_RAGA";

    // The callbacks through which we will interact with the LoaderManager.
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;
    private String mSearchTerm;
    private Raga singleRaga;
    private ArrayList<String> ragaArrayNames = new ArrayList<String>(); // arraylist of raga names
    // the raga arraynames is used only to store the list
    public static ArrayList<Raga> ragaArrayList = new ArrayList<Raga>();

    public static int[][]notesList ;
    public int[] notesPerRaga = new int[12];
    // The loader's unique id. Loader ids are specific to the Activity or
    // Fragment in which they reside.
    private static final int LOADER_ID = 1;
    private static String fontPath = "fonts/Roboto-Regular.ttf";

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // this context is used in async task
        mContext = MySingleton.mContext;
        //getActivity(); // anu changed from getActivity() to mContext.
        setHasOptionsMenu(true);
        // Initialize the adapter. Note that we pass a 'null' Cursor as the
        // third argument. We will pass the adapter a Cursor only when the
        // data has finished loading for the first time (i.e. when the
        // LoaderManager delivers the data to onLoadFinished). Also note
        // that we have passed the '0' flag as the last argument. This
        // prevents the adapter from registering a ContentObserver for the
        // Cursor (the CursorLoader will do this for us!)
        
        mAdapter = new RagaListAdapter1(mContext,null,0);
    }

    // params(layout file, parent viewgroup, if the xml is inflated directly
    // or via the code sometime later)
    // the parent viewgroup is inside the activity that will host the fragment view.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        actv = (DelayAutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextViewSearch);
       // actv.getBackground().clearColorFilter();
       // actv.setBackgroundResource(android.R.color.transparent);

        actv.setThreshold(1);//will start working from first character
        actv.setText("");
        actv.setSingleLine();
        actv.setFocusableInTouchMode(true);

        Typeface face = Typeface.createFromAsset(mContext.getAssets(), fontPath);
        actv.setTypeface(face);

        actv.requestFocus();

        btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
        btnSearch.setText("Search");
        btnSearch.setTypeface(face);
        lvNames = (ListView) rootView.findViewById(R.id.lvRagaNames);
        // create a ref to db to show the list
        // THis is to update the adapter for the suggestionslist of autocompletetextview
        ragaDBInstance = MySingleton.getRagaDBInstance();
        getLoaderManager().initLoader(LOADER_ID, null, mCallbacks);
        // Inflate the layout for this fragment
        return rootView;
    }
    // On click of autoText I have the cursor show all the list first
    // I have attached the filter which gets called every single time
    //TODO: question: do I need to do the cursor will all first?

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       // Log.d(tag,"onActivity created");

        // Set up ListView, assign adapter and set some listeners. The adapter was previously
        // created in onCreate().

        // Associate the (now empty) adapter with the ListView.
        actv.setAdapter(mAdapter);
        // Associate the (now empty) adapter with the ListView.
        // I want to show an empty list view, only on click of search ripple_button the list should appear
      
        actv.setOnItemClickListener(autoCompleteItemClickListener);
        btnSearch.setOnClickListener(searchListener);
        lvNames.setOnItemClickListener(listItemClickListener);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(tag,"onAttach");

        try {
            // Assign callback listener which the holding activity must implement. This is used
            // so that when a contact item is interacted with (selected by the user) the holding
            // activity will be notified and can take further action such as populating the contact
            // detail pane (if in multi-pane layout) or starting a new activity with the contact
            // details (single pane layout).
            // The Activity (which implements the LoaderCallbacks<Cursor>
            // interface) is the callbacks object through which we will interact
            // with the LoaderManager. The LoaderManager uses this object to
            // instantiate the Loader and to notify the client when data is made
            // available/unavailable.
            mCallbacks = this;

//            mOnContactSelectedListener = (OnContactsInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnContactsInteractionListener");
        }
    }

   private AdapterView.OnItemClickListener autoCompleteItemClickListener =
           new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   //TODO: Added the following:
                   //TODO: everytime the autocomplete textview change runQuery
                   // is executed. The call back for FilterQueryProvider
                 //  Log.d(tag, "inside the auto on click listener");
                   mAdapter.getFilter().filter(actv.getText().toString());
                   //Everytim
                   mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
                       @Override
                       public Cursor runQuery(CharSequence charSequence) {
                           return ragaDBInstance.
                                   getRagaCursorByName(charSequence);
                       }
                   });
                   // Gets the Cursor object currently bound to the ListView
                   final Cursor cursor = mAdapter.getCursor();
                   // Moves to the Cursor row corresponding to the ListView item that was clicked
                   cursor.moveToPosition(position);
                   // Creates a raga lookup
                   Raga r = ragaDBInstance.getRagaObjectFromCursor(cursor);

                   if (r != null) {
                       actv.getBackground().clearColorFilter();
                       actv.setText(r.getRagaName());
                   }
                   else
                       actv.setText("");
               }
    };

    // TO hide the 3 vertical dots and add the following code in onCre
    // setHasOptionsMenu(true);
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    // on click of search you want a service to get the raga name and display
    private View.OnClickListener searchListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d(tag, "inside search ripple_button on click");
                    String searchTermFilter = actv.getText().toString();
                    if (searchTermFilter == null) {
                       ragaDBInstance.getAllRagas();
                    }
                    else
                        ragaDBInstance.getRagaCursorByName(actv.getText().toString());

                    lvNames.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            };

    /**
     * Called when ListView selection is cleared, for example
     * when search mode is finished and the currently selected
     * contact should no longer be selected.
     */
    private void onSelectionCleared() {
        // Uses callback to notify activity this contains this fragment
      //  mOnContactSelectedListener.onSelectionCleared();

        // Clears currently checked item
        //getListView().clearChoices();
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
    // call the detail activity
    public AdapterView.OnItemClickListener listItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   // Log.d(tag, "inside the on item selected part, call the detail, pass the selected raga");
                    // Gets the Cursor object currently bound to the ListView
                    final Cursor cursor = mAdapter.getCursor();
                    // Moves to the Cursor row corresponding to the ListView item that was clicked
                    cursor.moveToPosition(position);
                    // Creates a raga lookup
                    Raga dataToSend = new Raga();
                    dataToSend = ragaDBInstance.getRagaObjectFromCursor(cursor);

                    Intent detailActivityIntent1 = new Intent(getActivity(),
                            DetailActivity.class);
                    Bundle b = new Bundle();
                    b.putParcelable(ITEM_RAGA,dataToSend);
                    //attaching a bundle which is a raga object. Raga is a parcelable object
                    detailActivityIntent1.putExtras(b);
                    startActivity(detailActivityIntent1);
                }
            };

    /* This method is called when you start the query via a call initLoader() method.
     * The createLoader id is the same as mentioned in initLoader(id)
     * returns a cursor Loader
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new LoaderRagaData(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursorData) {
        // A switch-case is useful when dealing with multiple Loaders/IDs
        switch (loader.getId()) {
            case LOADER_ID:
                /* Moves the query results into the adapter, causing the
                 * ListView fronting this adapter to re-display
                 * */
                //mAdapter.changeCursor(cursor);
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    /*
    * Invoked when the CursorLoader is being reset. For example, this is
    * called if the data in the provider changes and the Cursor becomes stale.
    */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}







