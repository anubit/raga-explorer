package com.examples.anu.sqliteexamplewithjson.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.activity.DetailActivity;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;

import com.examples.anu.sqliteexamplewithjson.db.RagaListAdapter1;
import com.examples.anu.sqliteexamplewithjson.util.LoaderRagaData;

/**
 * Created by Anu on 6/7/15.
 */
// this class displays the recently searched ragas
// The detail Activity class updates the last played raga in the database
// using a cursor, query the database for the recently played raga
// and using the adapter to display the data.

public class RecentlyDisplayedFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView lvRecentlyViewedItems;
    public static String tag = "recently displayed fragment";
    private Context mContext;
    public static RagaDBDAO ragaDBInstance;
    private RagaListAdapter1 mAdapter;
    public static final String ITEM_RAGA = "SELECTED_RAGA";


    // The loader's unique id. Loader ids are specific to the Activity or
    // Fragment in which they reside.
    private static final int LOADER_ID = 2;
    // The callbacks through which we will interact with the LoaderManager.
    private LoaderManager.LoaderCallbacks<Cursor> mCallbacks;

    //required constructor
    public RecentlyDisplayedFragment()
    {

    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
      //  Log.d(tag," on attach ");
        try {
            mCallbacks = (LoaderManager.LoaderCallbacks<Cursor>) this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnContactsInteractionListener");
        }

        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Log.d(tag," on create ");
        mContext = MySingleton.mContext;
        ragaDBInstance = MySingleton.getRagaDBInstance();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
   //    Log.d(tag," on create view of recently displayed fragment");
       mContext = getActivity();
       View rootView = inflater.inflate(R.layout.fragment_recentlyviewed, container, false);
       lvRecentlyViewedItems = (ListView) rootView.findViewById(R.id.lvRecentlyViewedItems);
       mAdapter = new RagaListAdapter1(mContext,null,0);

      //  Log.d(tag, "initialize the loader to load data to an activity asynchronously");
        getLoaderManager().initLoader(LOADER_ID, null, mCallbacks);

       return rootView;

    }

    //final tweaks to UI
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Associate the (now empty) adapter with the ListView.
        // Refers to RagaListAdapter1
        lvRecentlyViewedItems.setAdapter(mAdapter);
        lvRecentlyViewedItems.setOnItemClickListener(listItemClickListener);

    }

    // Called after the containing Activity has been resumed
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
     //   Log.d(tag,"inside on resume");
        Cursor cursor = ragaDBInstance.getRagaCursorByRecentlyPlayed();
        mAdapter = new RagaListAdapter1(mContext,cursor,0);
        lvRecentlyViewedItems.setAdapter(mAdapter);

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(tag, "on pause of favorites fragment");

    }

    @Override
    public void onStop() {
        super.onStop();

        //ragaDBInstance.close();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
      //  mListener = null;
    }

    // call the detail activity
    public AdapterView.OnItemClickListener listItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //    Log.d(tag, "inside the on item selected part, call the detail, pass the selected raga");
                    // Gets the Cursor object currently bound to the ListView
                    final Cursor cursor = mAdapter.getCursor();
                    // Moves to the Cursor row corresponding to the ListView item that was clicked
                    cursor.moveToPosition(position);
                    // Creates a raga lookup

                    Raga dataToSend = new Raga();
                    dataToSend = ragaDBInstance.getRagaObjectFromCursor(cursor);
                    Log.d(tag, "print the raga object from search onitem click:" + dataToSend.getRagaName());
                    Log.d(tag, "print the raga object aronotes from search onitem click:" + dataToSend.getAroNotes());

                    Intent detailActivityIntent1 = new Intent(getActivity(),
                            DetailActivity.class);
                    Bundle b = new Bundle();
                    b.putParcelable(ITEM_RAGA,dataToSend);
                    //attaching a bundle which is a raga object. Raga is a parcelable object
                    detailActivityIntent1.putExtras(b);
                    //    detailActivityIntent.putExtra(ITEM_RAGA, r.getRagaName());
                    // Log.d(tag, "print the raga object aronotes from search onitem click:" + r.getAroNotes());
                    startActivity(detailActivityIntent1);
                }
            };

    // If I want use the loader for the search activity, update the UI, preserve the orientation
    //
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new LoaderRagaData(mContext);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursorData) {
        // A switch-case is useful when dealing with multiple Loaders/IDs
        switch (loader.getId()) {
            case LOADER_ID:
                Log.d(tag,"the loader id is = " + loader.getId());
                Log.d(tag," the id = " + LOADER_ID);
                mAdapter.notifyDataSetChanged();
                break;
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


}
