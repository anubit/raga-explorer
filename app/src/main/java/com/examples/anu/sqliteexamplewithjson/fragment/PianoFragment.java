package com.examples.anu.sqliteexamplewithjson.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.examples.anu.sqliteexamplewithjson.Helper.MyBus;
import com.examples.anu.sqliteexamplewithjson.Interface.OnUnHighLightedNoteEvent;
import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.Interface.OnFragmentDataEvent;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.Interface.OnHighLightedNoteEvent;
import com.squareup.otto.Subscribe;

import java.util.Map;

/**
 * Created by Anu on 6/29/15.
 */
public class PianoFragment extends Fragment{

    public static final int KEYS = 13;

    private TextView[] tvKey = new TextView[KEYS];
    private View view;
    private PreferencesManager prefManager;
    private String pitch;

    // must default constructor
    public PianoFragment() {

    }

    // initialize bus, prefmanager instance.
    // Preferences Manager - shared preferences for settings
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyBus.getInstance().register(this); //registering the fragment for the event
        prefManager = MySingleton.getPrefManagerInstance();
        pitch = prefManager.getKeyPitchValue();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int id = 0;
        switch (pitch) {
            case "C": id = R.layout.fragment_piano_cscale;
                      break;
            case "A": id = R.layout.fragment_piano_ascale;
                      break;
            default: id = R.layout.fragment_piano_gscale;

        }
        view = View.inflate(getActivity(), id, null);
        setupTextViews(view);
        return view;
    }


    // the bundle is handled here not in createview because the
    // activity on created creates the bundle and only after the activity is created,
    // the bundle is available in this method. it's not available in onCreateView()
    @Subscribe
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    // Progressupdate listener will fire the event in his highlight method
    // This method will listen for the event "OnFragmentDataEvent"
    // when the event happens this method is fired.
    @Subscribe
    public void updateDataToPianoFragment(final OnFragmentDataEvent event) {
        Map<Integer, String> map = event.map;
        if (map != null) {
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                int index = entry.getKey();
                String value = entry.getValue();
                tvKey[index].setText(value);
            }
        }
    }


    // This method is called when the progress update listener calls the highlight method
    // which in turn fires the event OnHighLightedNoteEvent
    @Subscribe
    public void highLightPianoText(final OnHighLightedNoteEvent event) {

        final Spannable spanText = Spannable.Factory.getInstance().newSpannable(event.text);
        String note = event.note;
        final int endIndex = note.length();
        final int index = event.index; // final because to access the inner class

        // This check is important to make sure that activity still exists to run the method
        // runOnUiThread
        if (getActivity() == null)
            return;

        // This runOnUiThread is an inner class. To access the span text and the endIndex
        // from this inner class, you have to make the variables final
        // You need this on UIThread to highlight the text
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                spanText.setSpan(new BackgroundColorSpan(0xff0000ff), 0,endIndex,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvKey[index].setText(spanText);
            }
        });
    }

    // This method is called when the progress update listener calls the highlight method
    // which in turn fires the event OnHighLightedNoteEvent
    @Subscribe
    public void unHighLightPianoText(final OnUnHighLightedNoteEvent event) {

        final Spannable spanText = Spannable.Factory.getInstance().newSpannable(event.text);
        String note = event.note;
        final int endIndex = note.length();
        final int index = event.index;

        // This runOnUiThread is an inner class. To access the span text and the endIndex
        // from this inner class, you have to make the variables final
        // You need this on UIThread because the highlight
        if (getActivity() == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                spanText.setSpan(new BackgroundColorSpan(0xFF), 0,endIndex,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvKey[index].setText(spanText);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void setupTextViews(View v) {
        // array of textviews to display the notes dynamically based on the raga
        tvKey [0] = (TextView) v.findViewById(R.id.wkey0);
        tvKey [2] = (TextView) v.findViewById(R.id.wkey2);
        tvKey [3] = (TextView) v.findViewById(R.id.wkey3);
        tvKey [5] = (TextView) v.findViewById(R.id.wkey5);
        tvKey [7] = (TextView) v.findViewById(R.id.wkey7);
        tvKey [8] = (TextView) v.findViewById(R.id.wkey8);
        tvKey [10] = (TextView) v.findViewById(R.id.wkey10);
        tvKey [12] = (TextView) v.findViewById(R.id.wkey12);

        // black keys
        tvKey [1] = (TextView) v.findViewById(R.id.bkey1);
        tvKey [4] = (TextView) v.findViewById(R.id.bkey4);
        tvKey [6] = (TextView) v.findViewById(R.id.bkey6);
        tvKey [9] = (TextView) v.findViewById(R.id.bkey9);
        tvKey[11] = (TextView) v.findViewById(R.id.bkey11);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyBus.getInstance().unregister(true);
    }
}
