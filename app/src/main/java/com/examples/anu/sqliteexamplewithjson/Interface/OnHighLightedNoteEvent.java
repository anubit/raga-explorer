package com.examples.anu.sqliteexamplewithjson.Interface;

import android.text.Spannable;

import java.util.HashMap;

/**
 * Created by Anu on 10/29/15.
 */
public class OnHighLightedNoteEvent {
    public final String note;
    public final int index;
    public Spannable text;

    public OnHighLightedNoteEvent(String note,int index, Spannable text)
    {
        this.note = note;
        this.index = index;
        this.text = text;
    }
    public OnHighLightedNoteEvent(String note ,int index)
    {
        this.note = note;
        this.index = index;
    }


}
