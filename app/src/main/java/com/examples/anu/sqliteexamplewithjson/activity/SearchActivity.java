package com.examples.anu.sqliteexamplewithjson.activity;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.fragment.RecentlyDisplayedFragment;
import com.examples.anu.sqliteexamplewithjson.fragment.SearchFragment;

// This activity class is just to inflate the fragments dynamically
public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // content_frame_fragment from base activity
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame_fragment);
        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_search, null,false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);

        // load dynamically search fragment in the place holder(R.id.search_fragment)
        // you get the placeholder from the xml. placeholder is the container
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.search_fragment, new SearchFragment())
                    .addToBackStack("SearchFragment")
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        ab.setTitle("Search");
    }

    // On Back pressed (at the bottom) should take you to previous screen not navigation drawer
    @Override
    public void onBackPressed(){
        //Log.d(tag,"On back pressed of search activity should take you to main");
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_search, container, false);
            return rootView;
        }
    }
}
