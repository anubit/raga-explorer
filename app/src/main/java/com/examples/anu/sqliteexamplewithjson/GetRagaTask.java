package com.examples.anu.sqliteexamplewithjson;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.examples.anu.sqliteexamplewithjson.Helper.MyBus;
import com.examples.anu.sqliteexamplewithjson.db.Raga;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 5/18/15.
 */

// THis class reads from Json, loads in db and returns an arraylist
// Array list will have raga name and Melakartha.
// http://stackoverflow.com/questions/18297378/weakreference-asynctask-pattern-in-android




public class GetRagaTask extends AsyncTask<Void,Void,List<Raga>> {

    private Context context;
    public static String tag = "GetRagaTask";
    RagaDBDAO ragaDAO;
    ArrayList<Raga> ragaListToDisplay;
    public ReturnDataFromAsyncTask returnData = null;
    WeakReference<Activity> weakActivity;

   // The parameter context is used in resource
    public GetRagaTask(Context context) {


        this.context = context;
        this.ragaDAO = new RagaDBDAO(context);
    }

    // open the json, create a Raga Object using GSON,
    // load that object into the sqlite database.
    @Override
    protected List<Raga> doInBackground(Void... arg0) {
        int resourceID = 0;
        List<Raga> ragaArrayNames = new ArrayList<Raga>();

        return ragaListToDisplay;
    }

    @Override
    protected void onPostExecute(List<Raga> ragas) {
        super.onPostExecute(ragas);
        MyBus.getInstance().register(this);
        Log.d(tag,"the array size = " + ragas.size());
        if ((ragas != null) && (ragas.size() != 0)) {

           returnData.handleReturnData(ragas);
        }

    }


}
