package com.examples.anu.sqliteexamplewithjson.util;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;

// This Loader is used to load the data from RecentlyDisplayedFragment
/**
 * Created by Anu on 8/21/15.
 */
// THis class is for searching
//why extending Loader?

// cursor loader is for content provider, if you want other apps to access your data.then
// content provider works

// I don't want other app to access my raga database. then extend the loader. Async to provide
// background loading part

// http://www.androiddesignpatterns.com/2012/08/implementing-loaders.html
//The Loader class allow you to load data asynchronously in an activity or fragment.
// They can monitor the source of the data and deliver new results when the content changes.
// They also persist data between configuration changes.
// If the result is retrieved by the Loader after the object has been disconnected
// from its parent (activity or fragment), it can cache the data
// The CursorLoader class reconnect the Cursor after a configuration change.

// Android provides a Loader default implementation to handle SQlite database connections, the CursorLoader class.
// For a ContentProvider based on an SQLite database you would typically use the CursorLoader class.
// This Loader performs the database query in a background thread so that the application is not blocked.

// If the Cursor becomes invalid, the onLoaderReset() method is called on the callback class.

// The CursorLoader class reconnect the Cursor after a configuration change.

// http://www.androiddesignpatterns.com/2012/08/implementing-loaders.html

public class LoaderRagaData extends AsyncTaskLoader<Cursor>{
    private Cursor mCursor;
    public static RagaDBDAO ragaDBInstance;
    private static String tag = "LoaderRagaData";
    // Loaders may be used across multiple Activitys (assuming they aren't
    // bound to the LoaderManager), so NEVER hold a reference to the context
    // directly. Doing so will cause you to leak an entire Activity's context.
    // The superclass constructor will store a reference to the Application
    // Context instead, and can be retrieved with a call to getContext().
    //TODO: CHange this to application context instead of activity context.

    public LoaderRagaData(Context context) {
        super(context);
      //  Log.d(tag, "inside LoaderRaga constructor");
      //  Log.d(tag,"get the ref of instance of RagaDBDao from singleton");
        //ragaDBInstance = new RagaDBDAO(context);
        ragaDBInstance = MySingleton.getRagaDBInstance();
       // Log.d(tag,"created the ragaDBInstance inside the loader = " + ragaDBInstance.toString());
        //ragaDBInstance= ragaDBInstance.opnToRead();

    }


    /* Runs on a worker thread */

    /****************************************************/
    /** (1) A task that performs the asynchronous load **/
    /****************************************************/

    @Override
    public Cursor loadInBackground() {

        // get raga data by search
        // TODO: Perform the query here.
      //  Log.d(tag,"inside loadInBackground - get all ragas");
        return ragaDBInstance.getAllRagas();
    }

    /* Runs on the UI thread */
    /********************************************************/
    /** (2) Deliver the results to the registered listener **/
    /********************************************************/

    @Override
    public void deliverResult(Cursor cursor) {

       // Log.d(tag,"inside deliver result");
       // Log.d(tag,"cursor = " + cursor.toString());
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
          //  Log.d(tag," the loader is reset, releaseResources");
            releaseResources(cursor);
            return;
        }

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
       // Log.d(tag,"what's the old cursor: mcursor = " + mCursor);
       // Cursor oldCursor = mCursor;
       // mCursor = cursor;

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
         //   Log.d(tag,"the loader is in started state");
            super.deliverResult(cursor);
        }
        // Invalidate the old data as we don't need it any more.
     //   if (oldCursor != null && oldCursor != cursor && !oldCursor.isClosed()) {
       //     releaseResources(cursor);
       // }
    }

    /*********************************************************/
    /** (3) Implement the Loader’s state-dependent behavior **/
    /*********************************************************/

    /**
     * Starts an asynchronous load of data. When the result is ready the callbacks
     * will be called on the UI thread. If a previous load has been completed and is still valid
     * the result may be passed to the callbacks immediately.
     * <p/>
     * Must be called from the UI thread
     */
    @Override
    protected void onStartLoading() {
        // Deliver any previously loaded data immediately.
      //  Log.d(tag,"on start loading");
        if (mCursor != null) {
         //   Log.d(tag,"previous cursor not null, load previous data");
            deliverResult(mCursor);
        }

        // When the observer detects a change, it should call onContentChanged()
        // on the Loader, which will cause the next call to takeContentChanged()
        // to return true. If this is ever the case (or if the current data is
        // null), we force a new load.
        if (takeContentChanged() || mCursor == null) {
            Log.d(tag," no previous data - start loading with forceLoad()");
         //   forceLoad();
        }
    }

    private void releaseResources(Cursor cursor) {
        // For a simple List, there is nothing to do. For something like a Cursor, we
        // would close it in this method. All resources associated with the Loader
        // should be released here.
        // An async query came in while the loader is stopped
        if (cursor != null) {
            cursor.close();
        }
        return;

    }


    /**
     * Must be called from the UI thread
     */
    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        Log.d(tag,"on stop loading");
       // ragaDBInstance.close();
        cancelLoad();


        // Note that we leave the observer as is. Loaders in a stopped state
        // should still monitor the data source for changes so that the Loader
        // will know to force a new load if it is ever started again.
    }


    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader has been stopped.
        onStopLoading();

        // At this point we can release the resources associated with 'mData'.
        if (mCursor != null && !mCursor.isClosed()) {
           releaseResources(mCursor);
        }

        //TODO: question: why do we stop monitoring the changes here
        //todo: in onStopLoading() we are still monitoring the changes
        // The Loader is being reset, so we should stop monitoring for changes.
        if (mObserver != null) {
            // TODO: unregister the observer
            mObserver = null;
        }

    }
    @Override
    public void onCanceled(Cursor cursor) {
        // Attempt to cancel the current asynchronous load.
        super.onCanceled(cursor);
        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        releaseResources(cursor);
    }

    /*********************************************************************/
    /** (4) Observer which receives notifications when the data changes **/
    /*********************************************************************/

    // NOTE: Implementing an observer is outside the scope of this post (this example
    // uses a made-up "SampleObserver" to illustrate when/where the observer should
    // be initialized).

    // The observer could be anything so long as it is able to detect content changes
    // and report them to the loader with a call to onContentChanged(). For example,
    // if you were writing a Loader which loads a list of all installed applications
    // on the device, the observer could be a BroadcastReceiver that listens for the
    // ACTION_PACKAGE_ADDED intent, and calls onContentChanged() on the particular
    // Loader whenever the receiver detects that a new application has been installed.
    // Please don’t hesitate to leave a comment if you still find this confusing! :)
    private MyObserver mObserver; //TODO: delete this

}
