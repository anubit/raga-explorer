package com.examples.anu.sqliteexamplewithjson;

import com.examples.anu.sqliteexamplewithjson.db.Raga;

import java.util.List;

/**
 * Created by Anu on 5/18/15.
 */
public interface ReturnDataFromAsyncTask {
    void handleReturnData(List<Raga> r);
}

