package com.examples.anu.sqliteexamplewithjson.Interface;

import android.text.Spannable;

/**
 * Created by Anu on 11/9/15.
 */
// This event is to post to piano fragment
public class OnUnHighLightedNoteEvent {

        public final String note;
        public final int index;
        public Spannable text;

        public OnUnHighLightedNoteEvent(String note, int index,Spannable text)
        {
            this.note = note;
            this.index = index;
            this.text = text;
        }

}
