package com.examples.anu.sqliteexamplewithjson.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Anu on 5/9/15.
 */
// Contract class where you define the constants
// Database tables, columns, URI creation and version
// This is a singleton where only one ref of db is created.

// This is both the contract class as well as the class that extends SQLiteOpenHelper
// that helps to create and maintain database.
// You have created an instance of this class so you can access to Database via getHelper()
// getHelper will give you access to an database.
// Notice the constructor private RagaDbHelper is created in SQLIteOpenHelper - feature of singleton
// singleton cannot be subclassed because the constructor is private
// notice the private static variable "  private static RagaDbHelper instance"
// you are accessing the object through getHelper function which makes sure only one instance of DB


// Raga(id,ragaNo,ragaName, melaNo,melakarthaname,aronotes,avonotes)
//(auto,11,Bhupali,123,sankarabharam,"[1,2,3,4,5]","[2,3,4,6,7]"
public class RagaDbHelper extends SQLiteOpenHelper {

    private static RagaDbHelper instance;

    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "ragaDB.db";

    //  table name
    public static final String TABLE_RAGA = "Raga";

    // Table Columns names
    protected static final String KEY_RAGA_ID = "_id";
    protected static final String KEY_RAGA_NUM = "ragaNo";
    protected static final String RAGA_NAME = "ragaName";
    protected static final String MELAKARTHA_NUM = "melaNo";
    protected static final String MELAKARTHA_NAME = "melakarthaName";
    protected static final String  ARONOTES = "aroNotes";
    protected static final String  AVONOTES = "avoNotes";
    protected static final String LASTPLAYEDTIME ="lastPlayedTime";
    protected static final String FAVORITES="favorites";


    public RagaDbHelper(Context context, String name,
                                  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);
    }
    public RagaDbHelper(Context c)
    {
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    //    Log.d("RagaDBHelper", " on create ");
        onOpen(db);
        createTables(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
       // Log.d("RagaDBHelper", "opOpen() method ");
        if (!db.isReadOnly()) {
                // Enable foreign key constraints
                db.execSQL("PRAGMA foreign_keys=ON;");

        }
    }

    public static synchronized RagaDbHelper getHelper(Context context) {
        if (instance == null)
            instance = new RagaDbHelper(context);
        return instance;
    }

    // For cursorAdapter to work, you need to
    // [_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
    // storing time as millisecs, integer easy manipulation
    private void createTables(SQLiteDatabase db) {

        // Construct tabels: Melakartha,RagaNotes,MusicRecords
        String CREATE_RAGA_TABLE = "CREATE TABLE " + TABLE_RAGA + "("
                +  KEY_RAGA_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_RAGA_NUM + " INTEGER,"
                + RAGA_NAME + " TEXT,"
                + MELAKARTHA_NAME + " TEXT,"
                + MELAKARTHA_NUM + " INTEGER,"
                + ARONOTES+ " TEXT,"
                + AVONOTES+ " TEXT,"
                + LASTPLAYEDTIME + " INTEGER,"
                + FAVORITES + " INTEGER" +
                ")";

       db.execSQL(CREATE_RAGA_TABLE);
    }

    // Upgrading the database between versions
    // This method is called when database is upgraded like modifying the table structure,
    // adding constraints to database, etc
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      //  if (newVersion == 1) {
            // Wipe older tables if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RAGA);
            // Create tables again
            onCreate(db);
       // }
    }
}

