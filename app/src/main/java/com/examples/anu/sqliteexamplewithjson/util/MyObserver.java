package com.examples.anu.sqliteexamplewithjson.util;

import android.database.ContentObserver;
import android.os.Handler;

//TODO: TO BE DELETED - Aug 23. I need this only if I allow the user to add or modify the ragas
// for ex: in contacts If I do the edits via website, then I need to do the sync
// An observer to receive notifications when the data source has changed.
// you can either use ContentObserver or Broadcast receiver.

//  When a change is detected, the observer should call Loader#onContentChanged(),
// which will either
// (a) force a new load if the Loader is in a started state or,
// (b) raise a flag indicating that a change has been made so that
// if the Loader is ever started again, it will know that it should reload its data.

// To use the ContentObserver you have to take two steps:
// Implement a subclass of ContentObserver
// Register your content observer to listen for changes
public class MyObserver extends ContentObserver {
   public MyObserver(Handler handler) {
      super(handler);
   }
}