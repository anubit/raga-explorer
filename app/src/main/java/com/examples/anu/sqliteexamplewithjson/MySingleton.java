package com.examples.anu.sqliteexamplewithjson;

import android.content.Context;

import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.db.RagaDBDAO;

import java.io.File;

/**
 * Created by Anu on 9/2/15.
 */

// declaring singleton variables:pre
public class MySingleton {

    private static MySingleton singletonInstance;
    private static PreferencesManager prefManagerInstance;

    public static String outputFile = "myData";

    public static String absolutePathOfOutputFile;
    public static RagaDBDAO ragaDBInstance;
    public static int favCount;

    public static Context mContext = MyApplication.getAppContext();
    // The initInstance method lets the internal instance be initialized only once
    // and this is done in the MyApplication class
    public static void initInstance()
    {
        if (singletonInstance == null)
        {
            singletonInstance = getInstance();
            absolutePathOfOutputFile = mContext.getFilesDir().getAbsolutePath()+ File.separator +
                outputFile;

            ragaDBInstance = new RagaDBDAO(mContext);
            prefManagerInstance = new PreferencesManager(mContext);
            favCount = 0;
        }
    }

    // The getInstance method allows to access the instance from every part of our application
    public static MySingleton getInstance()
    {
        // Return the instance
        if (singletonInstance == null) {
            //Always pass in the Application Context
            singletonInstance = new MySingleton();
        }

        return singletonInstance;
    }

    public static int getFavCount() { return  favCount;}



    public static RagaDBDAO getRagaDBInstance() {
        return ragaDBInstance;
    }

    public static synchronized PreferencesManager getPrefManagerInstance() {
        return prefManagerInstance;
    }

    public static String getAbsolutePathOfOutputFile()
    {
        return absolutePathOfOutputFile;
    }




    //the MySingleton constructor is kept private because we don’t want
    // to allow the creation of multiple instances.
    private MySingleton()
    {
       // initInstance();
    }
}
