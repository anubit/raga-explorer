package com.examples.anu.sqliteexamplewithjson.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.R;

/**
 * Created by Anu on 6/9/15.
 */
// save the selection of the user in shared preferences
// use singleton to make it global
// on progress changed of the pitch, save the pitch,tempo and shruthi on/off in shared preferences
// reason for shared preferences vs global storage: shared pref saved even after the user
// switches off the app and reboots again.
// get the saved shared pref in a bundle and pass it to detail page to the play part.

// to the playing page which is the detailed page
public class SettingsActivity extends BaseActivity {
    
    TextView tvPitchValue,tvTempoValue,tvShruthiCaption,tvPitchCaption,tvTempoCaption;
    SeekBar tempoBar,pitchBar;
    Switch aSwitch;
    int value,tempoValue;

    private String text = null; // default
    PreferencesManager myPreferenceManager; // storing the prefs manager as a part of the activity

    public String pitchValues[] = {"D#", "D", "E#","E","F#", "F", "G#", "G", "A#", "A", "B#", "B",
        "C#","C"};
    public static String tag = "settings";
    private static String fontPath = "fonts/Roboto-Regular.ttf"; // font path


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setup();

        // load dynamically search fragment in the place holder(R.id.search_fragment)
        // you get the placeholder from the xml. placeholder is the container
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .addToBackStack("Settings")
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // get Pref() - returns sharedPreferences
        // pref manager does not store a strong reference to listerner.
        // so store the listener as a part of the object in the activity .
        // Instead of the following
        // myPreferenceManager.getPref().registerOnSharedPreferenceChangeListener(this);
        // do this where the listener is a part of the activity and it exists as long as
        // the activity exists

        text = myPreferenceManager.getKeyPitchValue(); // show previous values
        value = myPreferenceManager.getKeyProgressBarValue();
        tempoValue =  myPreferenceManager.getKeyTempoValue();
        tvPitchValue.setText(text);
        pitchBar.setProgress(value);
        //tvTempoValue.setText(String.valueOf(tempoValue));

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up ripple_button
        // This will add a left facing caret along the app icon and enables
        // it as an action ripple_button. when the user presses it call is made
        // to onOptionsSelected()
        // The ID for the action is android.R.id.home.
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        ab.setTitle("Settings");
        setupListener();
    }

    // On Back pressed (at the bottom) should take you to previous screen not navigation drawer
    @Override
    public void onBackPressed(){
       // Log.d(tag,"inside on BackPressed() of settings activity");
        myPreferenceManager.savePreferences("PITCH",text );
        myPreferenceManager.savePreferences("TEMPO",tempoValue);
        myPreferenceManager.savePreferences("PROGRESS_BAR_VALUE",value);
        NavUtils.navigateUpFromSameTask(this);

        super.onBackPressed();

    }
    private void setup() {

        // This framelayout is from baseActivity where the settings activity will be loaded
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.content_frame_fragment);


        // inflate the custom activity layout
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_settings, null, false);
        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);


        //set the pref manager to access the methods.
        // creating the shared pref filename to save the data
        myPreferenceManager = MySingleton.getPrefManagerInstance();
        Log.d(tag, "my pref instance = " + myPreferenceManager.toString());
        tvPitchCaption = (TextView) findViewById(R.id.tvPitchCaption);
        tvPitchValue = (TextView) findViewById(R.id.tvPitchValue);
        tvTempoCaption = (TextView) findViewById(R.id.tvTempoCaption);
        tvShruthiCaption = (TextView) findViewById(R.id.tvShruthiCaption);
        tvTempoValue = (TextView)findViewById(R.id.tvTempoValue);
        tempoBar = (SeekBar) findViewById(R.id.tempoBar);
        pitchBar = (SeekBar) findViewById(R.id.pitchBar);
        aSwitch = (Switch) findViewById(R.id.switch1);

        // setting font family
        Typeface face = Typeface.createFromAsset(getBaseContext().getAssets(), fontPath);
        tvPitchValue.setTypeface(face);
        tvPitchCaption.setTypeface(face);
        tvShruthiCaption.setTypeface(face);
        tvTempoCaption.setTypeface(face);
        tvTempoValue.setTypeface(face);

        setDefaultValues();
    }


    private void setDefaultValues() {

        pitchBar.setMax(100);
        pitchBar.setProgress(20);
        tempoBar.setProgress(20);
        tempoBar.setMax(100);
      
        //set the switch to ON
        aSwitch.setChecked(true);
        // aSwitch.setText("on");

    }
    private void setupListener() {
        tempoBar.setOnSeekBarChangeListener(customListener);
        pitchBar.setOnSeekBarChangeListener(customListener);
        aSwitch.setOnCheckedChangeListener(customSwitchListener);
    }



    private Switch.OnCheckedChangeListener customSwitchListener =
            new Switch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        Toast.makeText(getBaseContext(),"turn on the shruthi",Toast.LENGTH_SHORT).show();
                        aSwitch.setText("On");
                        // save the shruthi in preferences
                        myPreferenceManager.savePreferences("shruthi",true);
                    }else{
                        Toast.makeText(getBaseContext(),"turn off the shruthi",Toast.LENGTH_SHORT).show();
                        //Log.d("inside settingsactivity", " turn off the shruthi");
                        aSwitch.setText("Off");
                        myPreferenceManager.savePreferences("shruthi", false);
                    }
                }
            };


    private SeekBar.OnSeekBarChangeListener customListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            //  display(progress);
            int barId = seekBar.getId();

            switch (barId) {

                case R.id.pitchBar:
                    setPitch();
                    break;
                case R.id.tempoBar:
                    setTempo();
                    break;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private void setTempo() {
        tempoValue = tempoBar.getProgress();
        if (value < 20 ) {
            tempoValue = 10;
        }
        else   if (value < 30 ) {
            tempoValue = 20;
        }  else   if (value < 40 ) {
            tempoValue = 30;
        } else   if (value < 50 ) {
            tempoValue = 40;
        } else   if (value < 60 ) {
            tempoValue = 50;
        } else   if (value < 70 ) {
            tempoValue = 60;
        } else   if (value < 80 ) {
            tempoValue = 70;
        } else   if (value < 90 ) {
            tempoValue = 80;
        } else tempoValue = 90;


        tvTempoValue.setText(String.valueOf(tempoValue)); // changes to UI
        tempoBar.setProgress(tempoValue);
        myPreferenceManager.savePreferences("TEMPO",tempoValue );
       // myPreferenceManager.savePreferences("PROGRESS_BAR_VALUE",value);
    }

    private void setPitch() {

        value = pitchBar.getProgress();

        if (value < 10) {
            text = pitchValues[0];
        }
        else if (value < 20) {
            text = pitchValues[1];
        }
        else if (value < 30) {
            text = pitchValues[2];
        }
        else if (value < 40) {
            text = pitchValues[3];
        }
        else if (value < 50) {
            text = pitchValues[4];
        }
        else if (value < 60) {
            text = pitchValues[5];
        }
        else if (value < 70) {
            text = pitchValues[6];
        }
        else if (value < 80) {
            text = pitchValues[7];

        }
        else if (value < 90) {
            text = pitchValues[8];
        }
        else {
            text = pitchValues[9];

        }
        tvPitchValue.setText(text); // changes to UI
        pitchBar.setProgress(value);
        // save the pitch and its value in shared preferences so it stores your previous values
        myPreferenceManager.savePreferences("PITCH",text );
        myPreferenceManager.savePreferences("PROGRESS_BAR_VALUE",value);
        // myPreferenceManager.getPref().registerOnSharedPreferenceChangeListener(mListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    // Up ripple_button is android.R.id.home
    // The superclass method responds to the Up selection by navigating to the parent activity,
    // as specified in the app manifest.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // when the user press up ripple_button, NavUtils navigateUpFromSameTask
        // is called. The parent actiivty is brought to front and this will
        // receive the onNewIntent()
        switch (item.getItemId() ) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
