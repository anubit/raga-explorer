package com.examples.anu.sqliteexamplewithjson.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anu on 9/11/15.
 */

// This class is for checking the version of the shared preferences
// Shared preferences will not be removed after Application update.
// If I want to load the data from json into db only the first time, later
// I update my json, it will not load the changed data.
public class MigrationManager {

    private final static String KEY_PREFERENCES_VERSION = "key_preferences_version";
    private final static int PREFERENCES_VERSION = 2;

    public static void migrate(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
        checkPreferences(preferences);
    }

    private static void checkPreferences(SharedPreferences thePreferences) {
        final double oldVersion = thePreferences.getInt(KEY_PREFERENCES_VERSION, 1);

        if (oldVersion < PREFERENCES_VERSION) {
            final SharedPreferences.Editor edit = thePreferences.edit();
            edit.clear();
            //edit.putInt(KEY_PREFERENCES_VERSION, currentVersion); need to add this later
            edit.commit();
        }
    }
}

