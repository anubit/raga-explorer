package com.examples.anu.sqliteexamplewithjson.db;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.util.LetterTileProvider;

import java.util.ArrayList;

import static com.examples.anu.sqliteexamplewithjson.db.RagaDbHelper.MELAKARTHA_NAME;
import static com.examples.anu.sqliteexamplewithjson.db.RagaDbHelper.RAGA_NAME;

/**
 * Created by Anu on 8/15/15.
 */

// Android has an excellent Filter class that helps us filter data from an entire data set based on
// some pattern (for example a string) provided. Filters are usually created by classes
// implementing the Filterable interface. It makes sense to use filters
// (implement the Filterable interface) mostly with adapters,
//
// which means Filterable classes are usually Adapter implementations.
// Example of filter : http://codetheory.in/android-filters/

/* CursorAdapter is to bind the data from sqlite directly to listview or gridview or spinner
 * In our case, I'm using list view
 * You have direct access to cursor and you inflate the view in newView()
 * 1. we define a constructor that passes the cursor and context to the superclass.
 * 2. we override the newView method which is used to inflate a new view template.
 * 3.Finally, we override the bindView which is used to bind all data to a given view to
 * populate the template content for the item.
 */
public class RagaListAdapter1 extends CursorAdapter implements Filterable{

        private static final int MAX_RESULTS = 10; // this is used in autocomplete text view
        public static String tag = "RagaListAdapter1";
        ArrayList<Raga> fullListRagas = new ArrayList<>();
        ArrayList<Raga> original = new ArrayList();
        private static String fontPath = "fonts/Roboto-Regular.ttf"; // font path
        private Context context; // to get the assetManager for font family
        private static AssetManager assetManager;

        public static RagaDBDAO ragaDBInstance;

        public static class ViewHolder {
            private TextView tvRagaName;
            private TextView tvMelakarthaName;
            private ImageView img_raga_icon;

            public ViewHolder(View view) {
                tvRagaName = (TextView) view.findViewById(R.id.tvRagaName);
                tvMelakarthaName = (TextView) view
                    .findViewById(R.id.tvMelakarthaName);
                img_raga_icon=(ImageView) view.findViewById(R.id.raga_icon);
            }
        }

        public RagaListAdapter1(Context context, Cursor cursor, int flags) {
            super(context, cursor, 0);
            this.context = context;
            ragaDBInstance = MySingleton.getRagaDBInstance();
        }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    // Note: you don't need to check for convertview == null, base adapter is the base class of cursoradapter
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
            assetManager = context.getAssets();
            return view;
        }

    /*
    This is where we fill-in the views with the contents of the cursor.
    */
    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
      @Override
      public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template

          ViewHolder viewHolder = (ViewHolder) view.getTag();
          final Resources res = context.getResources();
          final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

          final LetterTileProvider tileProvider = new LetterTileProvider(this.context);

          /*
          One more thing to note is that YOU MUST place something into each view (even if it is NULL).
           For instance, had my adapter looked like:
           Then you’ll notice some weird behavior – namely, that you’ll see the names start shifting
           as you scroll up and down the list.
           What happens is that if you don’t instantiate and place something into your TextView (basically to act as a place holder)
           then in your bindView method nothing gets bound to some of the TextViews and thus the shifting.
           So basically, if you see stuff shifting around in your lists,
          then that’s a big flag for make sure you are binding things to all of your views in both your newView and bindView methods.
            */

          String ragaName = cursor.getString(cursor.getColumnIndexOrThrow(RAGA_NAME));
          viewHolder.tvRagaName.setText(ragaName);
          viewHolder.tvRagaName.setTypeface(Typeface.createFromAsset(assetManager, fontPath));
          viewHolder.tvMelakarthaName.setText(cursor.getString(cursor.getColumnIndexOrThrow(MELAKARTHA_NAME)));
          viewHolder.tvMelakarthaName.setTypeface(Typeface.createFromAsset(assetManager,fontPath));

          String key = String.valueOf(ragaName.charAt(0));
          final Bitmap letterTile = tileProvider.getLetterTile(ragaName, key, tileSize, tileSize);

          viewHolder.img_raga_icon.setImageDrawable(new BitmapDrawable(context.getResources(), letterTile));
      }


    /* Converting a cursor input to a charsequence fo display
     * you need to override this to return the string value when
     * selecting an item from the autocomplete suggestions
     * cursor.getstring(whatevercolumn)
     * Autocomplete based on given ragaNum selected
     */
    @Override
    public CharSequence convertToString(Cursor cursor) {
        String value = "";
        value = String.valueOf(cursor.getColumnIndex(ragaDBInstance.ragaDbHelper.KEY_RAGA_NUM));
        return value;
    }

    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        String filter = "";
        if (constraint == null) filter = "";
        else
            filter = constraint.toString();

        //Here i have a static SQLiteOpenHelper instance that returns a cursor.
       // Cursor cursor = MyApplication.getDbHelpers().getRagaCursorByName(filter);
        Cursor cursor = ragaDBInstance.getRagaCursorByName(filter);
        ragaDBInstance.close();
        return cursor;
    }

    // getView() method to describe the translation between the data item and the View to display.
    // getView() is the method that returns the actual view used as a row within
    // the ListView at a particular position.

    // Making calls to findViewById() is really slow in practice,
    // and if your adapter has to call it for each View in your row
    //for every single row then you will quickly run into performance issues.
    // What the ViewHolder class does is cache the call to findViewById().
    // Once your ListView has reached the max amount of rows it can display on a screen,
    // Android is smart enough to begin recycling those row Views.
    // We check if a View is recycled with if (convertView == null).
    // If it is not null then we have a recycled View and can just change its values,
        // otherwise we need to create a new row View.
        // The magic behind this is the setTag() method which lets us attach an arbitrary object
        // onto a View object, which is how we save the already inflated View for future reuse.



    // THis is used in autoCompleteTextView in searchFragment
    // get all the raga first and display only the result that has the raganame
    // starts with the constraints
    //TODO: I think the lock is needed because we are calling the raga adapter from
    // different objects. what happens if i don't use the lock

}


