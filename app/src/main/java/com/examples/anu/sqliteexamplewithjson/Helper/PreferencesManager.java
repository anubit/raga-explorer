package com.examples.anu.sqliteexamplewithjson.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anu on 9/11/15.
 */

// This class sets the shared Preferences values for Shruthi, Loading DB first time
// and pitch
// pitch and shruthi changes from the user is in settings activity
// loading only one time check is in Main activity
public class PreferencesManager  {

    public static final String PREFS_NAME = "SETTINGS_PREFS";
    public static final String FAV_PREFS = "favoritesNotNull";
    public static final String KEY_LOAD_DB = "LOADFIRSTTIME";
    // shruthi  (make variable public to access from outside)
    public static final String KEY_SHRUTHI = "shruthi";
    // pitch value (make variable public to access from outside)
    public static final String KEY_PITCH_VALUE = "PITCH";
    public static final String KEY_TEMPO_VALUE = "TEMPO";
    public static final String KEY_PROGRESS_BAR = "PROGRESS_BAR_VALUE";


    private static PreferencesManager sInstance; // Not sure if I need another ref here?
    private final SharedPreferences mPref;
    private SharedPreferences.Editor editor;

    // getting the shared pref object from a specific file
    // mPref is an instance of the sharedPreferences
    public PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPref()
    {
        return mPref;
    }

    public SharedPreferences.Editor getEditor()
    {
        return editor;
    }
    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
           // initializeInstance();
        }
        return sInstance;
    }

    //overloading savePreferences method
    public void savePreferences(String key,String value) {
        editor = mPref.edit(); //2
        editor.putString(key,value);
        editor.commit(); //4
       // editor.apply();
    }

    // for progressbar value
    public void savePreferences(String key,int value) {
        editor = mPref.edit(); //2
        editor.putInt(key,value);
        editor.commit(); //4
        // editor.apply();
    }

    public void savePreferences(String key,boolean value) {
        editor = mPref.edit(); //2
        editor.putBoolean(key, value); //3  // default  - db not loaded
        editor.commit(); //4
        //editor.apply();
    }

    // These get functions will return the default value, save preferences will be
    // activated based on the user input
    public boolean getKeyShruthi() {
        return mPref.getBoolean(KEY_SHRUTHI,true);
    }

    public boolean getKeyFav() {
        return mPref.getBoolean(FAV_PREFS,false);
    }

    // default set to 'A'
    public int getKeyProgressBarValue() {
        return mPref.getInt(KEY_PROGRESS_BAR,20);
    }
    public String getKeyPitchValue() {
        return mPref.getString(KEY_PITCH_VALUE, "A");
    }

    public int getKeyTempoValue() {
        return mPref.getInt(KEY_TEMPO_VALUE, 10);
    }

    public boolean getKeyLoadDb() {
        boolean loadDBValue;
        loadDBValue = mPref.getBoolean(KEY_LOAD_DB, false); // def - false (not loaded)
        return loadDBValue;
    }

    public void clearSharedPreference(Context context) {
        editor.clear();
        editor.commit();
    }
}

