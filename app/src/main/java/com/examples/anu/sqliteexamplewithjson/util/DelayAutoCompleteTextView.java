package com.examples.anu.sqliteexamplewithjson.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

/**
 * Created by Anu on 6/4/15.
 */
// http://makovkastar.github.io/blog/2014/04/12/android-autocompletetextview-with-suggestions-from-a-web-service/
//With a standard AutoCompleteTextView a filtering is initiated after each entered character.
// If the user types text nonstop, data fetched for the previous request may become invalid on every
// new letter appended to the search string. You get extra expensive and unnecessary network calls,
// chance of exceeding API limits of your web service, stale suggestion results loaded for an
// incomplete search string. The way we go – add a small delay before user types the character
// and a request is sent to the web. If during this time the user enters the next character,
// the request for the previous search string is cancelled and rescheduled for delay time again.
// If the user doesn’t change text during delay time, the request is sent.
// To implement this behaviour we create a custom implementation of AutoCompleteTextView and
// override the method performFiltering(CharSequence text, int keyCode). T
// he variable mAutoCompleteDelay defines time in milliseconds after a request will be sent to a
// server if the user doesn’t change the search string.
public class DelayAutoCompleteTextView extends AutoCompleteTextView {

    private static final int MESSAGE_TEXT_CHANGED = 100;
    private static final int DEFAULT_AUTOCOMPLETE_DELAY = 750;

    private int mAutoCompleteDelay = DEFAULT_AUTOCOMPLETE_DELAY;
    private ProgressBar mLoadingIndicator;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            DelayAutoCompleteTextView.super.performFiltering((CharSequence) msg.obj, msg.arg1);
           // performFiltering((CharSequence) msg.obj,msg.arg1);

        }
    };

    public DelayAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void setLoadingIndicator(ProgressBar progressBar) {
        mLoadingIndicator = progressBar;
    }


    public void setAutoCompleteDelay(int autoCompleteDelay) {
        mAutoCompleteDelay = autoCompleteDelay;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }
     //   Log.d("DelayAutoTextView", "perform filtering");

        mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), mAutoCompleteDelay);
    }

    @Override
    public void onFilterComplete(int count) {
        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.GONE);
        }
        super.onFilterComplete(count);
    }
}
