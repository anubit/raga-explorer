package com.examples.anu.sqliteexamplewithjson.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.app.ActionBar;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.examples.anu.sqliteexamplewithjson.Helper.MyBus;
import com.examples.anu.sqliteexamplewithjson.MySingleton;
import com.examples.anu.sqliteexamplewithjson.Helper.PreferencesManager;
import com.examples.anu.sqliteexamplewithjson.R;
import com.examples.anu.sqliteexamplewithjson.fragment.RecentlyDisplayedFragment;
import com.examples.anu.sqliteexamplewithjson.util.MyIntentService;
import com.examples.anu.sqliteexamplewithjson.util.ServiceResultReceiver;

//import android.support.v7.app.ActionBarActivity;

// I'm using an interface to pass the result back from async task to main
// activity:
// To do that: create an interface
// In main Activity: declare the object and assign null;
// inside on create, pass the value this to that object.
// implment the method in main activity
// eg: ReturnDataFromAsyncTask returnData = null
// in oncreate() returnData = this.

// I kept getting the error: in getCount() part of the adapter.
// in oncreate() set the list view using findviewbyId,
// initialize the arraylist using new
//  that attach the arraylist to the adapter
//  mAdapter = new RagaListAdapter(this,listRaga);
// lvNames.setAdapter(mAdapter);

// setting up an hamburger icon on the navigation tool bar
// a drawer layout, list view - to display items in the layout
// To add an icon on the action bar: create a newActionBarDrawerToggle and override
// onDrawerOpened() and onDrawerClosed();

// steps in creating this hamburger icon.
// in exising main.xml  - create the drawer layout as the root, drawer layout can have
// only 2 children.
// In my case, I have an existing relative layout(only the main content view) and added the new list view to show
// the list of items.
// create members - drawerLayout, listview,adapter for listview, to do toggle on action bar:
// ActionBarDrawerToggle, title to display on the actionbar.
// private ListView mDrawerList;
//private ArrayAdapter<String> mNavAdapter;
//private ActionBarDrawerToggle mDrawerToggle;
//private DrawerLayout mDrawerLayout;
//private String mActivityTitle;


// in oncreate assign the defn to these variables.
/* mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString(); */

// set the list view and the array adapter
// set the listener for listview in onItemClickListener
// set the drawer toggle action bar: create a a new ActionBarToggle and override on
//DrawerOpened() and onDrawerClosed()

// The following 2 steps are very important to enable the actionBarToggle
/*mDrawerToggle.setDrawerIndicatorEnabled(true);
mDrawerLayout.setDrawerListener(mDrawerToggle); */

/* get the hamburger icon to be displayed on the action bar
 // get the hamburger icon displayed on the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

 */

//The issue that took me a long time to fix:
// I wanted to do fragments and also want to separate the set up of the DrawerLayout code
// I need a ref to host activity, and also ref to the DrawerLayout.
// I had confusion on how to display the drawerlayout.


// for version 21 and older, action bar is replaced by tool bar


/*  steps to set up the intent service
 *  1. create a class myIntentService extends intentservice
 *  2. override onHandleIntent where you would the background task
 *  3. create a resultreceiver class that extends receiver to receive the results from intent servic
 *  4. in main activity, create a variable for class receiver to receive the results.
 *  5. override the callback onReceiveResult
 *  6. in mainactivity, create an intent, pass parameters and start the service on that intent
 */
public class MainActivity extends BaseActivity {

    public static String tag = "MainActivity";
    public static final int LOAD_DATABASE_ACTIVITY = 0x5;

    PreferencesManager myPreferenceManager;
    private ServiceResultReceiver mReceiver;

    private ImageView imgVlogo;
    private TextView toolBarTitle;
    private boolean loadDBValue;
    private AssetManager assetManager;
    private ActionBar actionBar;
    private String fontPath = "fonts/Roboto-Regular.ttf"; // font path

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame_fragment);
        LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View activityView = layoutInflater.inflate(R.layout.activity_main, null,false);

        // add the custom layout of this activity to frame layout.
        frameLayout.addView(activityView);

        myPreferenceManager = MySingleton.getPrefManagerInstance();
        loadDBValue = myPreferenceManager.getKeyLoadDb();

        // Find the toolbar view inside the activity layout
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);

        assetManager = getBaseContext().getAssets();

        // load the database
        setupServiceReceiver();
        setupIntent();

        // logo
        imgVlogo = (ImageView)findViewById(R.id.imgVlogo);
        
        // Display the recently played list
        // load dynamically recently displayed fragment in the place holder(R.id.recently_displayed_fragment)
        // you get the placeholder from the xml. placeholder is the container
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.recentlyViewed_fragment, new RecentlyDisplayedFragment())
                    .commit();
        }
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        MyBus.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuInflater inflater = getMenuInflater();

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_recentlyviewed, container, false);
            return rootView;
        }
    }

    public void setupIntent()
    {
        // call the intent to load the database
        Intent intent = new Intent(this, MyIntentService.class);

        //TODO: pass the shruthi to sound touch part
        // load the database the first time only
        // THe reason for the use of singleton for LoadDB value is because I'm the checking
        // if the db is loaded in other activities, to avoid passing the value as in the intent

        if (loadDBValue == false) {
            myPreferenceManager.savePreferences("LOADFIRSTTIME",true);
            intent.putExtra(MyIntentService.COMMAND_KEY, 5);
            intent.putExtra(MyIntentService.RECEIVER_KEY, mReceiver);
        }
        else {
            intent.putExtra(MyIntentService.RECEIVER_KEY, mReceiver);
        }
        startService(intent);
    }


    // Setup the callback for when data is received from the service
    // this service is only for the main activity.
    public void setupServiceReceiver() {
        mReceiver = new ServiceResultReceiver(new Handler());
        // This is where we specify what happens when data is received from the service
        mReceiver.setReceiver(new ServiceResultReceiver.Receiver() {

            @Override
            public void onReceiveResult(int resultCode, Bundle resultData) {
                // super.onReceiveResult(resultCode, resultData);
                switch (resultCode) {
                    case MyIntentService.STATUS_RUNNING:
                        break;
                    case MyIntentService.STATUS_SUCCESS:
                      //  Log.d(tag,"status success");
                        boolean success = resultData.getBoolean(MyIntentService.SERVICE_WAS_SUCCESS_KEY);

                        if (success) {
                            Toast.makeText(getApplicationContext(),
                                    "The service was a success", Toast.LENGTH_LONG).show();
                        } else {
                            // Show not success message
                            Toast.makeText(getApplicationContext(),
                                    "The service was a failure", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case MyIntentService.STATUS_FINISHED:
                       // Toast.makeText(getApplicationContext(), "The service was finished",
                        //        Toast.LENGTH_LONG).show();
                        break;
                    case MyIntentService.STATUS_ERROR:
                        Toast.makeText(getApplicationContext(), "The service had an error",
                                Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }
}




