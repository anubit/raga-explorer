package soundtouch;

/**
 * Created by Anu on 1/22/15.
 */

public class SoundStreamRuntimeException extends RuntimeException
{
    public SoundStreamRuntimeException(String errorMsg)
    {
        super(errorMsg);
    }

    private static final long serialVersionUID = -772966743102678592L;

}