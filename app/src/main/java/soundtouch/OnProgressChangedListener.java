package soundtouch;

/**
 * Created by Anu on 1/22/15.
 */


public interface OnProgressChangedListener
{
    void onProgressChanged(int track, double currentPercentage,
                           long position);

    void onTrackEnd(int track);

    void onExceptionThrown(String string);
}
