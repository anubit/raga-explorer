package soundtouch;

import android.util.Log;

public class ST implements AudioProcessor
{

    private static final int DEFAULT_BUFFER_SIZE =8096;


    private int channels, samplingRate, bytesPerSample;
    private float tempo;
    private float pitchSemi;
    private float rate;
    private int track;


    public static synchronized native final void clearBytes(int track);

    public static synchronized native final void finish(int track, int bufSize);

    public static synchronized native final int getBytes(int track,
                                                          byte[] output, int toGet);

    public static synchronized native final void putBytes(int track,
                                                           byte[] input, int length);

    public static synchronized native  final void setup(int track,
                                                         int channels, int samplingRate, int bytesPerSample, float tempo,
                                                         float pitchSemi);



    public static synchronized native final void setPitchSemi(int track,
                                                               float pitchSemi);

    public static synchronized native final long getOutputBufferSize(int track);

    public static synchronized native final void setSpeech(int track,
                                                            boolean isSpeech);

    public static synchronized native final void setTempo(int track,
                                                           float tempo);

    public static synchronized native final void setRate(int track,
                                                          float rate);

    public static synchronized native final void setRateChange(int track,
                                                                float rateChange);

    public static synchronized native final void setTempoChange(int track,
                                                                 float tempoChange);

    public ST(int track, int channels, int samplingRate,
              int bytesPerSample, float tempo, float pitchSemi)
    {


        this.channels =  channels;
        this.samplingRate = samplingRate;
        this.bytesPerSample = bytesPerSample;
        this.tempo = tempo;
        this.pitchSemi = pitchSemi;
        this.track = track;
        this.rate = 1.0f;

        Log.d("ST", "inside on create");
        Log.d("ST","call the set function ");
        set(track,channels, samplingRate, bytesPerSample, tempo, pitchSemi);
    }

    public void set(int track, int channels,int samplingRate,int bytesPerSample,float tempo,float pitchSemi){
        Log.d("soundtouchclass,ST","inside set up of sound touch, call the c++ set up function");

        setup(track,channels,samplingRate,bytesPerSample,tempo,pitchSemi);
    }



    public int getBytesPerSample()
    {
        return bytesPerSample;
    }

    public int getChannels()
    {
        return channels;
    }

    public long getOutputBufferSize()
    {
        return getOutputBufferSize(track);
    }

    public int getSamplingRate()
    {
        return samplingRate;
    }

    public float getPitchSemi()
    {
        return pitchSemi;
    }

    public float getTempo()
    {
        return tempo;
    }

    public float getRate()
    {
        return rate;
    }

    public int getTrackId()
    {
        return track;
    }

    public void setBytesPerSample(int bytesPerSample)
    {
        this.bytesPerSample = bytesPerSample;
    }

    public void setChannels(int channels)
    {
        this.channels = channels;
    }

    public void setSamplingRate(int samplingRate)
    {
        this.samplingRate = samplingRate;
    }

    public void setSpeech(boolean isSpeech)
    {
        setSpeech(track, isSpeech);
    }


    public void setPitchSemi(float pitchSemi)
    {
        this.pitchSemi = pitchSemi;
        setPitchSemi(track, pitchSemi);
    }

    public void setTempo(float tempo)
    {
        this.tempo = tempo;
        setTempo(track, tempo);
    }
    public void setRate(float rate)
    {
        this.rate = rate;
        setRate(track, rate);
    }

    public void setTempoChange(float tempoChange)
    {
        if (tempoChange < -50 || tempoChange > 100)
            throw new SoundStreamRuntimeException(
                    "Tempo percentage must be between -50 and 100");
        this.tempo = 1.0f + 0.01f * tempoChange;
        setTempoChange(track, tempoChange);
    }

    public void clearBuffer()
    {
        clearBytes(track);
    }

    public void putBytes(byte[] input)
    {
        putBytes(track, input, input.length);
    }

    public int getBytes(byte[] output)
    {
        return getBytes(track, output, output.length);
    }

    // call finish after the last bytes have been written
    public void finish()
    {
        finish(track, DEFAULT_BUFFER_SIZE);
    }




}