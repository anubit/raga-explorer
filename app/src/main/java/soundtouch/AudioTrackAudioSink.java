package soundtouch;
import android.media.AudioTrack;

/**
 * Created by Anu on 1/22/15.
 */



public class AudioTrackAudioSink extends AudioTrack implements AudioSink
{
    public AudioTrackAudioSink(int streamType, int sampleRateInHz,
                               int channelConfig, int audioFormat, int bufferSizeInBytes, int mode)
            throws IllegalArgumentException
    {
        super(streamType, sampleRateInHz, channelConfig, audioFormat,
                bufferSizeInBytes, mode);
    }

    @Override
    public void close()
    {
        stop();
        flush();
        release(); //Anu made the change
    }
}