package soundtouch;

/**
 * Created by Anu on 1/22/15.
 */

//Notes on Handler:
// each handler is associated with a thread and the thread's message queue
// handler post the message to update the UI in the message queue.
// You also have a looper. what is a looper and is purpose.
// Normally the thread can execute only one message
// Add a comment to this line
// if you want to execute multiple message(Runnables) that is passed via handler
// to thread that the handler is associated with that thread, then you
// have to use this Looper class
//http://stackoverflow.com/questions/7597742/what-is-the-purpose-of-looper-and-how-to-use-it


import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.examples.anu.sqliteexamplewithjson.activity.DetailActivity;

import java.io.File;
import java.io.IOException;


// This class initializes a soundTouch(ST class),
public abstract class SoundStreamRunnable implements Runnable
{
    private static final long NOT_SET = Long.MIN_VALUE;
    protected static final int DEFAULT_BYTES_PER_SAMPLE = 2;
    private static final long MAX_OUTPUT_BUFFER_SIZE = 2048; // changed from 1024
    int counter = 0;

    protected Object pauseLock;
    protected Object sinkLock;
    protected Object decodeLock;

    protected volatile long bytesWritten;

    protected ST soundTouch;
    protected volatile AudioDecoder decoder;

    protected Handler handler;

    private String fileName;
    private boolean bypassSoundTouch;

    private volatile long loopStart = NOT_SET;
    private volatile long loopEnd = NOT_SET;
    private volatile AudioSink audioSink;

    private volatile OnProgressChangedListener progressListener;

    private volatile boolean paused;
    protected volatile boolean finished;

    protected int channels = 1; //anu I added
    protected int samplingRate = 16100; // Anu changed from 16100 to 16000

    public static String tag = "SoundStreamRunnable";

    protected abstract AudioSink initAudioSink() throws IOException;

    private void initSoundTouch(int id, float tempo, float pitchSemi)
    {
        Log.d("SoundStreamRunnable","inside initsoundtouch");

        // setting the channels - ANu
        soundTouch = new ST(id, channels, samplingRate,
                DEFAULT_BYTES_PER_SAMPLE, tempo, pitchSemi);
    }

    public long getBytesWritten()
    {
        return bytesWritten;
    }

    public int getChannels()
    {
        return soundTouch.getChannels();
    }

    public long getDuration()
    {
        if (decoder != null)
            return decoder.getDuration();
        else
            return NOT_SET;
    }

    public String getFileName()
    {
        return fileName;
    }

    public long getPlayedDuration()
    {
        synchronized (decodeLock)
        {
            if (decoder != null)
                return decoder.getPlayedDuration();
            else
                return NOT_SET;
        }
    }

    public float getPitchSemi()
    {
        return soundTouch.getPitchSemi();
    }

    public long getPlaybackLimit()
    {
        return loopEnd;
    }

    public int getSamplingRate()
    {
        return soundTouch.getSamplingRate();
    }

    public long getSoundTouchBufferSize()
    {
        return soundTouch.getOutputBufferSize();
    }

    protected int getSoundTouchTrackId()
    {
        return soundTouch.getTrackId();
    }

    public float getTempo()
    {
        return soundTouch.getTempo();
    }

    public float getRate()
    {
        return soundTouch.getRate();
    }

    public long getLoopEnd()
    {
        return loopEnd;
    }

    public long getLoopStart()
    {
        return loopStart;
    }

    public boolean isFinished()
    {
        return finished;
    }

    public boolean isLooping()
    {

        return loopStart != NOT_SET && loopEnd != NOT_SET;
    }

    public boolean isPaused()
    {
        return paused;
    }

    public void setBypassSoundTouch(boolean bypassSoundTouch)
    {
        this.bypassSoundTouch = bypassSoundTouch;
    }

    public void setLoopEnd(long loopEnd)
    {
        long pd = decoder.getPlayedDuration();
        if (loopStart != NOT_SET && pd <= loopStart)
            throw new SoundStreamRuntimeException(
                    "Invalid Loop Time, loop start must be < loop end");
        this.loopEnd = loopEnd;
    }

    public void setLoopStart(long loopStart)
    {
        long pd = decoder.getPlayedDuration();
        if (loopEnd != NOT_SET && pd >= loopEnd)
            throw new SoundStreamRuntimeException(
                    "Invalid Loop Time, loop start must be < loop end");
        this.loopStart = loopStart;
    }

    public void setOnProgressChangedListener(
            OnProgressChangedListener progressListener)
    {
        this.progressListener = progressListener;
    }

    public void setPitchSemi(float pitchSemi)
    {
        soundTouch.setPitchSemi(pitchSemi);
    }

    public void setTempo(float tempo)
    {
        soundTouch.setTempo(tempo);
    }

    public void setTempoChange(float tempoChange)
    {
        soundTouch.setTempoChange(tempoChange);
    }

    public void setRate(float rate)
    {
        soundTouch.setRate(rate);
    }

    public void setRateChange(float rate)
    {
        soundTouch.setRate(rate);
    }

    public SoundStreamRunnable(int id, String fileName, float tempo,
                               float pitchSemi) throws IOException
    {
          Log.d("SoundStreamRunnable","the constructor ");
         Log.d("SoundStreamRunnable","call the init Decoder class ");
        //  Log.d("SoundStreamRunnable", " filename = " + fileName);
        paused = true;
        initDecoder(fileName);
        Log.d("SoundStreamRunnable", "initializing soundtouch ");
        initSoundTouch(id, tempo, pitchSemi);
        //   Log.d("SoundStreamRunnable", "initializing  audiosink");
        audioSink = initAudioSink();
        this.fileName = fileName;
        // Log.d("SoundStreamRunnable","the filename to be played " + fileName);

        //handler is created in the UI thread
        // now, the handler will automatically bind to the
        // Looper that is attached to the current thread
        // You don't need to specify the Looper explicitly
        handler = new Handler();
        //    Log.d(tag,"Handler address = " + handler.toString());

        pauseLock = new Object();
        sinkLock = new Object();
        decodeLock = new Object();
        finished = false;
    }

    public SoundStreamRunnable(int[] resId)
    {


    }
    public SoundStreamRunnable(int id, File file, float tempo,
                               float pitchSemi) throws IOException
    {
        this(id, file.getAbsolutePath(), tempo, pitchSemi);
    }
    private void initDecoder(String fileName) throws IOException
    {
        if (Build.VERSION.SDK_INT >= 16)
        {
            Log.d(tag,"BEFORE CALLING MEDIACODECAUDIODECODER CONSTRUCTOR ");
            decoder = new MediaCodecAudioDecoder(fileName);
            Log.d(tag,"Decoder from the sound stream runnable = " + decoder);

        }
        else
        {
            throw new SoundStreamRuntimeException(
                    "Only API level >= 16 supported.");
        }

        channels = decoder.getChannels();
        // Log.d(tag, " get the channels from the decoder = " + channels);
        samplingRate = decoder.getSamplingRate();
    }

    @Override
    public void run()
    {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        // Normally the thread can execute only one message
        // if you want to execute multiple message(Runnables) that is passed via handler
        // to thread that the handler is associated with that thread, then you
        // have to use this Looper class

        // preparing a looper on current thread
        // the current thread is being detected implicitly
        Looper.prepare();
        Log.d(tag,"inside the run() - soundstream. player().start has called this");

        try
        {
            while (!finished)
            {

                //     Log.d("SoundStreamRunnable", "we are not finished, call the processFile()");
                //  Log.d("SoundStreamRunnable", "we are not finished, call the processFile()");
                //// I want to start doing something really long
                // which means I should run the fella in another thread.
                // I do that by sending a message - in the form of another runnable object

                processFile();
                Looper.loop();
                //    Log.d(tag,"coming out of processFile() and finished value = " + finished + "" +
                //           "progress listener = " + progressListener);
                // paused = true; // Anu commenting this, paused != true. I don't know why this is here
                if (progressListener != null && !finished)
                {
                    Log.d(tag,"progress listener != null and !finished, call progresslistener.onTrackEnd");
                    handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //       Log.d(tag,"soundtouch.getTrackId() = " + soundTouch.getTrackId());
                            progressListener.onTrackEnd(soundTouch.getTrackId());
                        }
                    });
                }
                synchronized (decodeLock)
                {
                    decoder.resetEOS();
                }
                //I noticed that the media codec closes each time after it reads the input
                // if (paused) finished = false; // Anu added this if statement and finished = false
                // if (paused) finished = false; // Anu added this if statement and finished = false

            }
        }
        catch (final IOException e)
        {
            e.printStackTrace();
            if (progressListener != null)
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        progressListener.onExceptionThrown(Log.getStackTraceString(e));
                    }
                });
        }
        catch (final SoundStreamDecoderException e)
        {
            if (progressListener != null)
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        progressListener.onExceptionThrown(Log.getStackTraceString(e));
                    }
                });
        }

        finally {
            // when it comes here that means it finished playing: Anu added this finished = true

            finished = true;
            Log.d(tag," status of finished in the finally block after process file = " + finished);

            //    if (finished == false ) { // Anu added this for pause because when I hit pause, it is closing everything
            soundTouch.clearBuffer();

            synchronized (sinkLock) {
                try {
                    audioSink.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                audioSink = null;
            }
            synchronized (decodeLock) {
                decoder.close();
                decoder = null;
            }
            //   }


            //I noticed that the media codec closes each time after it reads the input
            // if (paused) finished = false; // Anu added this if statement and finished = false
            //else finished = true;
            //finished = true; Anu commented out
            Log.d(tag," status of finished in the finally run() player block = " + finished);

        }
    }

    public void clearLoopPoints()
    {
        loopStart = NOT_SET;
        loopEnd = NOT_SET;
    }

    protected abstract void onStart();

    protected abstract void onPause();

    protected abstract void onStop();

    protected void seekTo(long timeInUs)
    {
    }

    public void start()
    {
        // Log.d(tag,"player.start() function, paused = false, finished = false =");
      //  Log.d(tag,"before counter = " + counter++);
      //  Log.d(tag,"before pauseLock = " + pauseLock.toString());
        synchronized (pauseLock)
        {
         //   Log.d(tag,"after counter = " + counter);
         //   Log.d(tag,"after pauseLock = " + pauseLock.toString());
            //    Log.d(tag,"inside start method- call the onStart method");
            onStart();
            //   Log.d(tag,"after the onStart() is done, finished = false, paused = false");
            paused = false;
            finished = false;
            pauseLock.notifyAll();
        }
    }

    public void pause()
    {
        synchronized (pauseLock)
        {
            Log.d(tag,"inside the pause");
            paused = true;
            finished = false;
            onPause();

        }
    }

    public void stop()
    {
        // Log.d("", "In stop(), paused is " + paused);
        paused = true;
        // Anu added this line you want to stop the track playing; paused = true
        finished = true;
       /* if (paused) //---- Anu moved the paused inside the sync block
        {
            synchronized (pauseLock) {
                // if (paused) {
                Log.d("", "In stop(), paused is " + paused);
                //onStop();
                paused = false;
                pauseLock.notifyAll();



                // }
            } */


        // }
    }

    private void pauseWait()
    {
     //   Log.d(tag,"inside pause wait, get a lock on PauseLock");
        synchronized (pauseLock)
        {
          //  Log.d(tag," paused = "+ paused);
            while (paused)
            {
                try
                {
              //      Log.d(tag,"call the pauselock.wait");
                    pauseLock.wait();
                }
                catch (InterruptedException e)
                {
                }
            }
        }
    }

    // THis processFILE is running in the background, called from the run() method
    // This function checks if looping is true - moves the seek to loop start
    // 2. if soundtouch buffer size < max buffer size, then process the bytes
    // 3. if the bytes are not
    private void processFile() throws IOException,
            SoundStreamDecoderException
    {
        //  Log.d(tag, "processFile() called from the thread's run method ");
        //  Log.d(tag," paused status = " + paused);
        int bytesReceived = 0;
        do
        {
            //     Log.d(tag, "processFile() ");
            //    Log.d("DECODE", "PLAYED: getPlayedDuration " +  String.valueOf(decoder.getPlayedDuration()));
            //    Log.d("DECODE", "PLAYED: getDuration" +  String.valueOf(decoder.getDuration()));
         //   Log.d(tag,"calling the pausewait() ");
            pauseWait();
         //   Log.d(tag,"paused status from run() - paused = " +paused );
            if (finished)
                break;
            //  Log.d(tag,"after the pause wait")
            //    Log.d(tag, "decoder's play duration =" + decoder.getPlayedDuration());
            //    Log.d(tag,"loop end = " + loopEnd);
            if (isLooping() && decoder.getPlayedDuration() >= loopEnd)
            {
                //Log.d("DECODE", "TEST TIME: " + String.valueOf(decoder.getTestTime()));
                //      Log.d(tag," isLooping and decoder.getPlayedduration > = loopEnd");
                //      Log.d("DECODE", "PLAYED: " +  String.valueOf(decoder.getPlayedDuration()));
                //      Log.d("Decode", "Is looping = " + isLooping());
                //     Log.d(tag,"go to loop start - seekTo");
                seekTo(loopStart);
            }
            else  if ( (paused == true ) && ((decoder.getPlayedDuration()) <= decoder.getDuration())) {
                // Anu added these - if not looping & playedduration < = duration
             //   Log.d(tag," paused = true; the player should start from where it left off, no flushing the data");
             //   Log.d(tag," played duration = "+ decoder.getPlayedDuration());
                seekTo(getPlayedDuration());
            }
           // Log.d(tag," played duration = "+ decoder.getPlayedDuration());

            if (soundTouch.getOutputBufferSize() <= MAX_OUTPUT_BUFFER_SIZE) {
                boolean newBytes;

                synchronized (decodeLock) {
                    try {

                      //  Log.d(tag, "call the decoder.decodeChunk() ");

                        newBytes = decoder.decodeChunk();

                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                        throw new SoundStreamDecoderException(
                                "Buggy google decoder");
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                        throw new SoundStreamDecoderException(
                                "Buggy google decoder");
                    }
                }
                if (newBytes) {
                    //Mani
                    //  Log.d(tag,"There are newBytes of chunk 1024 available");
                    //   Log.d(tag,"call the sendProgressUpdate");
                    if (decoder != null) // Anu - added this additional check.
                        // In the finally block decoder is closed, b
                        sendProgressUpdate(); // for progress listener

                  //  Log.d(tag, " call the process chunk to process the chunk" +
                    //        "and there are new bytes");
                    processChunk(decoder.getLastChunk(), true);
                }
            }
            // SoundTouch output buffer is reached
            else {
                // avoiding an extra allocation.
        //        Log.d(tag, "processing last chunk of data and there are no" +
          //              "new bytes");
                processChunk(decoder.getLastChunk(), false);
            }
        } while (!decoder.sawOutputEOS() && getPlayedDuration() <= getDuration());
        // Changed from < to <= Anu
      //  Log.d("SoundStreamRunnable", "finished value = " + finished);
     //   Log.d(tag,"decoder saw endof output stream? = " + decoder.sawOutputEOS());

        //Anu added the check if decoder is end of stream then close
        if (decoder.sawOutputEOS() == true)
            soundTouch.finish();

      //  Log.d(tag,"By pass soundTouch value = " + bypassSoundTouch);
        // Anu added byPassSoundTouch = false, !false = true
        if (!bypassSoundTouch) {
         //   Log.d(tag, "if !bypassSoundTouch = " + !bypassSoundTouch);
          //  Log.d(tag, "end of the played duration: come out");
            do {
            //    Log.d("SoundStreamRunnable", "inside the last null finished value = " + finished);
                //Anu: I noticed that either way if finished = true or false, it doesn't
                // matter, it just comes out
                if (finished)
                    break;
                bytesReceived = processChunk(null, false);
            } while (bytesReceived > 0) ;
            finished = true; // Anu added
            Log.d(tag,"finished value from process file = " + finished);

        }

    }

    private void sendProgressUpdate()
    {

        // After mRunnableOnSeparateThread is done with it's job,
        // I need to tell the user that i'm done
        // which means I need to send a message back to the UI thread
        // MAX_OUTPUT_BUFFER_SIZE);
        if (progressListener != null)
        {
            //  Log.d(tag,"progressListener is not null, update the duration");
            // Log.d(tag, " finished status  = " + finished);
            // This runnable is called on the UI thread
            // handler is used to post the message to the thread that is attached to
            handler.post(new Runnable()
            {

                @Override
                public void run()
                {
              //      Log.d(tag,"inside the send update handler's post");
                    if (!finished)
                    {
                        long pd = decoder.getPlayedDuration();
                     //   Log.d(tag,"played duration " + String.valueOf(pd));
                        long d = decoder.getDuration();
                      //  Log.d(tag,"duration " + String.valueOf(d));
                        double cp = pd == 0 ? 0 : (double) pd / (double) d;
                      //  Log.d(tag,"cp = duration " + String.valueOf(cp));
                      //  Log.d(tag,"updating the progress listener");
                        // Anu added the following code
                        if (cp > 0.95) {
                           // Log.d(tag, "player goes from PLAYING state to FINISHED success");
                            DetailActivity.myPlayerState = DetailActivity.PlayerState.FINISHED;

                        }
                        progressListener.onProgressChanged(
                                soundTouch.getTrackId(), cp, pd);
                    }
                }
            });
        }
    }

    private int processChunk(final byte[] input, boolean putBytes)
            throws IOException
    {
        int bytesReceived = 0;

       // Log.d(tag,"inside process chunk - process the bytes");

        if (input != null)
        {
           // Log.d(tag," bypasssoundtouch = " + bypassSoundTouch);
            if (bypassSoundTouch)
            {
              //  Log.d(tag," bypasssoundtouch = " + bypassSoundTouch);
                bytesReceived = input.length;

              //  Log.d(tag,"bytes received = " + bytesReceived);
            }
            else
            {
                if (putBytes) {
                    //Mani
                //    Log.d(tag," put bytes is true " + putBytes);
                //    Log.d(tag,"going to add the byte to soundtouch via" +
                 //           "putBytes method ");
                    soundTouch.putBytes(input);
                }

              //  Log.d("bytes", String.valueOf(input.length));
                bytesReceived = soundTouch.getBytes(input);
              //  Log.d(tag,"bytes received from sound touch after finishing the put bytes = " + bytesReceived);
            }
            if (bytesReceived > 0)
            {
              //  Log.d(tag, " bytes received > 0 ");
                synchronized (sinkLock)
                {
                    bytesWritten += audioSink.write(input, 0, bytesReceived);

                }
            }
        }
        return bytesReceived;
    }



}