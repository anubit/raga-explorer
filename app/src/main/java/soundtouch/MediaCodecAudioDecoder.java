package soundtouch;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Locale;

@SuppressLint("NewApi")
public class MediaCodecAudioDecoder implements AudioDecoder
{

    private static final long TIMEOUT_US = 0;

    private long durationUs; // track duration in us
    private volatile long currentTimeUs; // total played duration thus far
    private BufferInfo info;
    private MediaCodec codec;
    private MediaExtractor extractor;
    private MediaFormat format;
    private ByteBuffer[] codecInputBuffers;
    private ByteBuffer[] codecOutputBuffers;
    private byte[] lastChunk;
    private volatile boolean sawOutputEOS;
    private ByteBuffer buf;

    private static String tag = "MediaCodecAudioDecoder";
    public static String getExtension(String uri)
    {
        if (uri == null)
        {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0)
        {
            return uri.substring(dot);
        }
        else
        {
            // No extension.
            return "";
        }
    }


    public int getChannels() throws IOException
    {
        if (format.containsKey(MediaFormat.KEY_CHANNEL_COUNT))
            return format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        throw new IOException("Not a valid audio file");
    }

    public long getDuration()
    {
        return durationUs; // track duration
    }

    @Override
    public long getPlayedDuration()
    {
        return currentTimeUs;
    }

    public int getSamplingRate() throws IOException
    {
        if (format.containsKey(MediaFormat.KEY_SAMPLE_RATE))
            return format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        throw new IOException("Not a valid audio file");
    }

    FileOutputStream fis = null;

    // In the constructor you are creating extractor and codec
    // Extractor extracts the bytes from the source file and pass it to codec
    @SuppressLint("NewApi")
    public MediaCodecAudioDecoder(String fullPath) throws IOException
    {
        Locale locale = Locale.getDefault();
/*		if (getExtension(fullPath).toLowerCase(locale).equals(".wma"))
			throw new IOException("WMA file not supported");*/
        RandomAccessFile fis = null;
      //  extractor = new MediaExtractor();
        try
        {
            extractor = new MediaExtractor();
            extractor.setDataSource(fullPath);
        }
        //Sometimes it works and sometimes it don't....
        catch (IOException e)
        {
            try
            {
                Thread.sleep(25);
            }
            catch (InterruptedException e2)
            {
                e2.printStackTrace();
            }
            try
            {
                extractor = new MediaExtractor();
                extractor.setDataSource(fullPath);
            }
            catch (IOException e1)
            {
                try
                {
                    Thread.sleep(25);
                }
                catch (InterruptedException e2)
                {
                    e2.printStackTrace();
                }
                extractor = new MediaExtractor();
                extractor.setDataSource(fullPath);
            }
        }


       // extractor.selectTrack(0);
        format = extractor.getTrackFormat(0);
        String mime = format.getString(MediaFormat.KEY_MIME);
        durationUs = format.getLong(MediaFormat.KEY_DURATION);

        codec = MediaCodec.createDecoderByType(mime);
        codec.configure(
                format,//format of input data ::decoder OR desired format of the output data:encoder
                null,//Specify a surface on which to render the output of this decoder
                null,//Specify a crypto object to facilitate secure decryption
                0 //For Decoding, encoding use: CONFIGURE_FLAG_ENCODE
        );
        codec.start();

        codecInputBuffers = codec.getInputBuffers();
        codecOutputBuffers = codec.getOutputBuffers();

        extractor.selectTrack(0);
        info = new MediaCodec.BufferInfo();

    }

    @Override
    public void close()
    {
        //close the codec and release the codec and extractor"
        try
        {
            codec.stop();
        }
        catch (IllegalStateException e)
        {
            e.printStackTrace();
        }

        extractor.release();
        extractor = null;
        codec.release();
        codec = null;
    }

    // Interface at AudioDecoder.java
    // Called from SoundStreamRunnable.java ProcessFile method
    // The data from the MediaCodecOutputBuffer is read by buf.get method
    // This method will process the bytes and NEWBYTES = true
    // NEWBYTES = false when there are no new bytes
    // Returns true or false

    // Deque the bytes from codecbuffer to play
   @Override
    public boolean decodeChunk()
    {
        boolean newBytes = false;
        advanceInput(); // queues the data into mediaCodecInputEncoder( strips the header and data)

        final int res = codec.dequeueOutputBuffer(info, TIMEOUT_US);
        if (res >= 0)
        {
            //info is MediaCodec.Bufferinfo
            if (info.size - info.offset < 0) // process last chunk
            {
                int outputBufIndex = res;
                ByteBuffer buf = codecOutputBuffers[outputBufIndex];
                if (lastChunk == null || lastChunk.length != info.size)
                {
                    lastChunk = new byte[info.size];
                }
                buf.get(lastChunk);
                buf.clear();
                codec.releaseOutputBuffer(outputBufIndex, false);
                newBytes = true;
            }
            else
            {
              //  Log.d(tag,"info.size > 0 offset - call processBytes(res)");
                processBytes(res);
                newBytes = true;
            }
        }
        if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
        {
            currentTimeUs = durationUs;
            sawOutputEOS = true;
        }
        else if (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED)
        {
            codecOutputBuffers = codec.getOutputBuffers();

        }
        else if (res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED)
        {
            format = codec.getOutputFormat();
           // Log.d("MP3", "Output format has changed to " + format);
        }
        return newBytes;
    }

    // This method called from decodeChunk
    private void processBytes(int res)
    {
        int outputBufIndex = res;
        ByteBuffer buf = codecOutputBuffers[outputBufIndex];
        if (lastChunk == null
                || lastChunk.length != info.size - info.offset)
        {
            lastChunk = new byte[info.size - info.offset];
        }
        if (info.size != 0) currentTimeUs = info.presentationTimeUs;
        buf.position(info.offset);
        buf.get(lastChunk);
        buf.clear();
        codec.releaseOutputBuffer(outputBufIndex, false);
    }

    @Override
    public void resetEOS()
    {
        sawOutputEOS = false;
        info.flags = 0;
    }

    @Override
    public boolean sawOutputEOS()
    {
        return sawOutputEOS;
    }

    // not flushing creates distortion
    @Override
    public void seek(long timeInUs, boolean shouldFlush)
    {
        extractor.seekTo(timeInUs, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
        currentTimeUs = timeInUs;
        // Anu uncommented out, because play after pause is causing to flush
        // should flush is false
        if (shouldFlush)
            codec.flush();
    }

    // This method gets the data from the extractor. queues into MediaCodecInputBuffer
    // The extractor strips the header as the decoder can decode only bytes
    // and it does not care if the raw file is mp3 or wav
    // sets the end of input stream
    // queues into input buffer of codec to process the bytes
    private void advanceInput()
    {
        boolean sawInputEOS = false;

        // Returns the index of an input buffer to be filled with valid data or -1
        // if no such buffer is currently available. This method
        //will return immediately if timeoutUs == 0, wait indefinitely for the availability of an input buffer
        // public int DequeueInputBuffer (long timeoutUs)
        int inputBufIndex = codec.dequeueInputBuffer(TIMEOUT_US);
        ByteBuffer dstBuf;

        if (inputBufIndex >= 0)
        {
           // input buffer is available, extractor read the sample data"
            dstBuf = codecInputBuffers[inputBufIndex];

            //read sample data of size dstBuf buffer
            int sampleSize = extractor.readSampleData(dstBuf, 0);
            long presentationTimeUs = 0;

            if (sampleSize < 0)
            {
                sawInputEOS = true;
                sampleSize = 0;
            }
            else
            {
              //  Log.d(tag,"end of input is false, more samples to read - ");
                presentationTimeUs = extractor.getSampleTime();
             //   Log.d(tag," the sample time ? = " + presentationTimeUs);
                //currentTimeUs = presentationTimeUs;
            }

            // After filling a range of the input buffer at the specified index submit it to the component.
            //BUFFER_FLAG_END_OF_STREAM - end of stream
            // Once an input buffer is queued to the codec,
            // it MUST NOT be used until it is later retrieved by MediaCodec.GetInputBuffer(int)
            // or NoResponse call back

            codec.queueInputBuffer(inputBufIndex, 0, sampleSize,
                    presentationTimeUs,
                    sawInputEOS ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);
            if (!sawInputEOS)
            {
                extractor.advance();
            }
        }
    }



    @Override
    public byte[] getLastChunk()
    {
       // Log.d(tag,"get the last chunk");
        return lastChunk;
    }

}