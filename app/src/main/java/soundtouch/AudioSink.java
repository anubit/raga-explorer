package soundtouch;

import java.io.IOException;

/**
 * Created by Anu on 1/22/15.
 */

public interface AudioSink
{
    int write(byte[] input, int offSetInBytes, int sizeInBytes) throws IOException;
    void close() throws IOException;
}