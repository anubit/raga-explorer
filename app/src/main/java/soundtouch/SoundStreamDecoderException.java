package soundtouch;

/**
 * Created by Anu on 1/22/15.
 */

public class SoundStreamDecoderException extends Exception
{
    public SoundStreamDecoderException(String errorMsg)
    {
        super(errorMsg);
    }
}
