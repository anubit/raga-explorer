

/* cpp for class soundtouch_ST */

#ifndef _Included_soundtouch_ST
#define _Included_soundtouch_ST

#include "soundtouch_ST.h"
#include "soundtouch-jni.h"
#include <android/log.h>


/*
 * Class:     soundtouch_ST
 * Method:    clearBytes
 * Signature: (I)V
 */

#ifdef __cplusplus
extern "C" {
#endif

 vector<SoundTouchStream> stStreams(MAX_TRACKS);


extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_clearBytes
  (JNIEnv *env, jclass obj, jint track) {

 	SoundTouchStream& soundTouch = stStreams.at(track);

 	const int BUFF_SIZE = 8192;

 	queue<jbyte>* byteBufferOut = soundTouch.getStream();

 	SAMPLETYPE* fBufferIn = new SAMPLETYPE[BUFF_SIZE];
 	soundTouch.clear();

 	delete[] fBufferIn;
 	fBufferIn = NULL;

 	while (!byteBufferOut->empty()) {
 		byteBufferOut->pop();
 	}
 }

/*
 * Class:     soundtouch_ST
 * Method:    finish
 * Signature: (II)V
 */


extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_finish
  (JNIEnv *env, jclass obj, jint track, jint length) {

	SoundTouchStream& soundTouch = stStreams.at(track);

	const int bytesPerSample = soundTouch.getBytesPerSample();
	const int BUFF_SIZE = length / bytesPerSample;

	queue<jbyte>* byteBufferOut = soundTouch.getStream();

	SAMPLETYPE* fBufferIn = new SAMPLETYPE[BUFF_SIZE];
	process(soundTouch, fBufferIn, byteBufferOut, BUFF_SIZE, true); //audio is finishing

	delete[] fBufferIn;
	fBufferIn = NULL;
}
/*
 * Class:     soundtouch_ST
 * Method:    getBytes
 * Signature: (I[BI)I
 */
 extern "C" JNIEXPORT jint JNICALL Java_soundtouch_ST_getBytes
   (JNIEnv *env, jclass thiz, jint track, jbyteArray get, jint toGet) {


  //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_st.cpp- ", "%s","inisde getBytes");

 	queue<jbyte>* byteBufferOut = stStreams.at(track).getStream();

 	jbyte* res = new jbyte[toGet];

 	jint bytesWritten;

 	jboolean isCopy;
 	jbyte* ar = (jbyte*) env->GetPrimitiveArrayCritical(get, &isCopy);

 	bytesWritten = copyBytes(ar, byteBufferOut, toGet);

 	env->ReleasePrimitiveArrayCritical(get, ar, JNI_ABORT);

 	delete[] res;
 	res = NULL;

 	return bytesWritten;
}

/*
 * Class:     soundtouch_ST
 * Method:    putBytes
 * Signature: (I[BI)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_putBytes
  (JNIEnv *env, jclass thiz, jint track, jbyteArray input, jint length)
  {

  //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_st.cpp-inside putbytes ","%s","put bytes function");

 	SoundTouchStream& soundTouch = stStreams.at(track);
 //    __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_st.cpp-inside putbytes ","%s","create a sound touch object of type soundtouchstream");
 	const int bytesPerSample = soundTouch.getBytesPerSample();
 //	 __android_log_print(ANDROID_LOG_DEBUG, "bytes per sample", "%d",bytesPerSample);
 	const int BUFF_SIZE = length / bytesPerSample;
  //   __android_log_print(ANDROID_LOG_DEBUG, "length of the audio", "%d",length);
 	queue<jbyte>* byteBufferOut = soundTouch.getStream();

 	jboolean isCopy;
 	jbyte* ar = env->GetByteArrayElements(input, &isCopy);

 	SAMPLETYPE* fBufferIn = new SAMPLETYPE[BUFF_SIZE];

 	convertInput(ar, fBufferIn, BUFF_SIZE, bytesPerSample);
  //   __android_log_print(ANDROID_LOG_DEBUG, "call the process method ", "%s","process the 1st set of bytes");
 	process(soundTouch, fBufferIn, byteBufferOut, BUFF_SIZE, false); //audio is ongoing.

 	env->ReleaseByteArrayElements(input, ar, JNI_ABORT);

 	delete[] fBufferIn;
 	fBufferIn = NULL;
 }

/*
 * Class:     soundtouch_ST
 * Method:    setup
 * Signature: (IIIIFF)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setup
  (JNIEnv *env, jclass obj, jint track, jint channels, jint samplingRate, jint bytesPerSample, jfloat tempo, jfloat pitchSemi) {
 // sts->setup(env,obj,a,b,c,d,f,g)
 	SoundTouchStream& soundTouch = stStreams.at(track);
    // prints "test" to the LogCat output with "skyscanner_sdk_jni" as the tag

 //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_ST.cpp", "%s","call the set up again");
  //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_ST.cpp, the channels = ", "%d",channels);
 	setup(soundTouch, channels, samplingRate, bytesPerSample, tempo, pitchSemi);
 }

/*
 * Class:     soundtouch_ST
 * Method:    setPitchSemi
 * Signature: (IF)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setPitchSemi
   (JNIEnv *env, jclass thiz, jint track, jfloat pitchSemi) {

	SoundTouchStream& soundTouch = stStreams.at(track);
	setPitchSemi(soundTouch, pitchSemi);
}
/*
 * Class:     soundtouch_ST
 * Method:    getOutputBufferSize
 * Signature: (I)J
 */
 extern "C" JNIEXPORT jlong JNICALL Java_soundtouch_ST_getOutputBufferSize
   (JNIEnv *env, jclass thiz, jint track) {

	SoundTouchStream& soundTouch = stStreams.at(track);
	queue<jbyte>* byteBufferOut = soundTouch.getStream();
	return byteBufferOut->size();
}

/*
 * Class:     soundtouch_ST
 * Method:    setSpeech
 * Signature: (IZ)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setSpeech(
                                JNIEnv *env, jclass thiz, jint track, jboolean speech) {
	SoundTouchStream& soundTouch = stStreams.at(track);
	if (speech) {
		// use settings for speech processing
		soundTouch.setSetting(SETTING_SEQUENCE_MS, 40);
		soundTouch.setSetting(SETTING_SEEKWINDOW_MS, 15);
		soundTouch.setSetting(SETTING_OVERLAP_MS, 8);
		//fprintf(stderr, "Tune processing parameters for speech processing.\n");
	}
	else
	{
		soundTouch.setSetting(SETTING_SEQUENCE_MS, 0);
		soundTouch.setSetting(SETTING_SEEKWINDOW_MS, 0);
		soundTouch.setSetting(SETTING_OVERLAP_MS, 8);
	}
}

/*
 * Class:     soundtouch_ST
 * Method:    setTempo
 * Signature: (IF)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setTempo(
                                 JNIEnv *env, jclass thiz, jint track, jfloat tempo) {
	SoundTouchStream& soundTouch = stStreams.at(track);
	setTempo(soundTouch, tempo);
}

/*
 * Class:     soundtouch_ST
 * Method:    setRate
 * Signature: (IF)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setRate(
                    JNIEnv *env, jclass thiz, jint track, jfloat rate) {
	SoundTouchStream& soundTouch = stStreams.at(track);
	setRate(soundTouch, rate);
}

/*
 * Class:     soundtouch_ST
 * Method:    setRateChange
 * Signature: (IF)V
 */
extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setRateChange(
                          JNIEnv *env, jclass thiz, jint track, jfloat rateChange) {
	SoundTouchStream& soundTouch = stStreams.at(track);
	setRateChange(soundTouch, rateChange);
}
/*
 * Class:     soundtouch_ST
 * Method:    setTempoChange
 * Signature: (IF)V
 */
 extern "C" JNIEXPORT void JNICALL Java_soundtouch_ST_setTempoChange
   (JNIEnv *env, jclass thiz, jint track, jfloat tempoChange) {

	SoundTouchStream& soundTouch = stStreams.at(track);
	setTempoChange(soundTouch, tempoChange);
}
#ifdef __cplusplus
}
#endif
#endif // MY_INLUDE_H
