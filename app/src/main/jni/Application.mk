# $Id: Application.mk 165 2012-12-28 19:55:23Z oparviai $
#
# Build both ARMv5TE and ARMv7-A machine code.
#
#APP_CPPFLAGS += -ggdb3 -O0 -gdwarf-2 -Wall -DDEBUG;
# armeabi-v7a x86 mips arm64-v8a x86_64  mips64

APP_ABI := armeabi armeabi-v7a x86
APP_OPTIM := debug
APP_STL := gnustl_shared
APP_PLATFORM := android-22

