#include "soundtouch-jni.h"
#include <android/log.h>

SoundTouchStream::SoundTouchStream() {
		byteBufferOut = new queue<jbyte>();
		sampleRate = bytesPerSample = 0;

}

SoundTouchStream::SoundTouchStream(const SoundTouchStream& other) {
		byteBufferOut = new queue<jbyte>();
		sampleRate = bytesPerSample = 0;

}


queue<jbyte>* SoundTouchStream::getStream() {
    return byteBufferOut;
}

int SoundTouchStream::getSampleRate() {
		return sampleRate;
}
    
int SoundTouchStream::getBytesPerSample() {
		return bytesPerSample;
}
    
void SoundTouchStream::setSampleRate(int sampleRate) {
		SoundTouch::setSampleRate(sampleRate);
		this->sampleRate = sampleRate;
}
    
void SoundTouchStream::setBytesPerSample(int bytesPerSample) {
		this->bytesPerSample = bytesPerSample;
}
    
uint SoundTouchStream::getChannels() {
		return channels;
}
    



//Anu: This puts the bytes into the write function that plays the tracks
// This is called by putBytes function  and finish() of SoundTouch_ST.cpp
int process(SoundTouchStream& soundTouch, SAMPLETYPE* fBufferIn,
                   queue<jbyte>* byteBufferOut, const int BUFF_SIZE, bool finishing) {

 //   __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","inside process function");
	const uint channels = soundTouch.getChannels();
	const int buffSizeSamples = BUFF_SIZE / channels;
	const int bytesPerSample = soundTouch.getBytesPerSample();
    
	int nSamples = BUFF_SIZE / channels;
    
	int processed = 0;
    
	if (finishing) {
		soundTouch.flush();
	} else {
//	      __android_log_print(ANDROID_LOG_DEBUG, "call the putSamples of soundTouchStream", "%d",nSamples);
		soundTouch.putSamples(fBufferIn, nSamples);
	}
    
	do {
		nSamples = soundTouch.receiveSamples(fBufferIn, buffSizeSamples);
	//	__android_log_print(ANDROID_LOG_DEBUG, "nSamples from receiveSamples", "%d",nSamples);
	//	__android_log_print(ANDROID_LOG_DEBUG, "call the write function", "%s","write function to be called from process");
		processed += write(fBufferIn, byteBufferOut, nSamples * channels,
                           bytesPerSample);
	} while (nSamples != 0);
    
	return processed;
}

void* getConvBuffer(int sizeBytes) {
	int convBuffSize = (sizeBytes + 15) & -8;
	// round up to following 8-byte bounday
	char *convBuff = new char[convBuffSize];
	return convBuff;
}


int copyBytes(jbyte* arrayOut, queue<jbyte>* byteBufferOut, int toGet) {
	int bytesWritten = 0;

	for (int i = 0; i < toGet; i++) {
		if (byteBufferOut->size() > 0) {
			arrayOut[i] = byteBufferOut->front();
			byteBufferOut->pop();
			++bytesWritten;
		} else {
			break;
		}
	}

	return bytesWritten;
}

int write(const float *bufferIn, queue<jbyte>* bufferOut, int numElems,
                 int bytesPerSample) {
	int numBytes;
    
	int oldSize = bufferOut->size();
    
	if (numElems == 0)
		return 0;

  //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","inside the write function");
	numBytes = numElems * bytesPerSample;
	short *temp = (short*) getConvBuffer(numBytes);
    
	switch (bytesPerSample) {
        case 1: {
            unsigned char *temp2 = (unsigned char *) temp;
            for (int i = 0; i < numElems; i++) {
                temp2[i] = (unsigned char) saturate(bufferIn[i] * 128.0f + 128.0f,
                                                    0.0f, 255.0f);
            }
            break;
        }
            
        case 2: {
            short *temp2 = (short *) temp;
            for (int i = 0; i < numElems; i++) {
                short value = (short) saturate(bufferIn[i] * 32768.0f, -32768.0f,
                                               32767.0f);
                temp2[i] = value;
            }
            break;
        }
            
        case 3: {
            char *temp2 = (char *) temp;
            for (int i = 0; i < numElems; i++) {
                int value = saturate(bufferIn[i] * 8388608.0f, -8388608.0f,
                                     8388607.0f);
                *((int*) temp2) = value;
                temp2 += 3;
            }
            break;
        }
            
        case 4: {
            int *temp2 = (int *) temp;
            for (int i = 0; i < numElems; i++) {
                int value = saturate(bufferIn[i] * 2147483648.0f, -2147483648.0f,
                                     2147483647.0f);
                temp2[i] = value;
            }
            break;
        }
        default:
            //should throw
            break;
	}
	for (int i = 0; i < numBytes / 2; ++i) {
		bufferOut->push(temp[i] & 0xff);
		bufferOut->push((temp[i] >> 8) & 0xff);
	}
	delete[] temp;
	temp = NULL;
	return bufferOut->size() - oldSize;
}

void setPitchSemi(SoundTouchStream& soundTouch, float pitchSemi) {
	soundTouch.setPitchSemiTones(pitchSemi);
}

void setTempo(SoundTouchStream& soundTouch, float tempo) {
	soundTouch.setTempo(tempo);
}
void setRate(SoundTouchStream& soundTouch, float rate) {
	soundTouch.setRate(rate);
}
void setTempoChange(SoundTouchStream& soundTouch, float tempoChange) {
	soundTouch.setTempoChange(tempoChange);
}
void setRateChange(SoundTouchStream& soundTouch, float rateChange) {
	soundTouch.setRateChange(rateChange);
}
void setup(SoundTouchStream& soundTouch, int channels, int sampleRate,
                  int bytesPerSample, float tempoChange, float pitchSemi) {

  //  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","checking if the channels are set");
 //    __android_log_print(ANDROID_LOG_DEBUG, "channels = ", "%d",channels);
 //   __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","inside set up again before calling setBytesPerSample");
	soundTouch.setBytesPerSample(bytesPerSample);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","inside set up again after setBytesPerSample");

 //     __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s"," before setSampleRate");
	soundTouch.setSampleRate(sampleRate);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setsamplerate");

//	 __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling setChannels");
	soundTouch.setChannels(channels);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setChannels");
	  // Added for testing the channels
	//  int x = soundTouch.getChannels();
    //  __android_log_print(ANDROID_LOG_DEBUG, "print the channels", "%d", x);
 //     __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling  setTempo(");
	soundTouch.setTempo(tempoChange);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling  setTempo(");

//	   __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling  setpitchSemiTones");
	soundTouch.setPitchSemiTones(pitchSemi);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setpitchSemiTones");
//	    __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling setRateChange");
	soundTouch.setRateChange(0);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setRateChange");

  //    __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling setSettingwith QUICKSEEK");
	soundTouch.setSetting(SETTING_USE_QUICKSEEK, false);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setSettingwith QUICKSEEK");
//	    __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","before calling setsetting");
	soundTouch.setSetting(SETTING_USE_AA_FILTER, true);
//	  __android_log_print(ANDROID_LOG_DEBUG, "soundtouch_jni.cpp", "%s","after calling setsetting");
    
}

void convertInput(jbyte* input, float* output, const int BUFF_SIZE,
                         int bytesPerSample) {
	switch (bytesPerSample) {
        case 1: {
            unsigned char *temp2 = (unsigned char*) input;
            double conv = 1.0 / 128.0;
            for (int i = 0; i < BUFF_SIZE; i++) {
                output[i] = (float) (temp2[i] * conv - 1.0);
            }
            break;
        }
        case 2: {
            short *temp2 = (short*) input;
            double conv = 1.0 / 32768.0;
            for (int i = 0; i < BUFF_SIZE; i++) {
                short value = temp2[i];
                output[i] = (float) (value * conv);
            }
            break;
        }
        case 3: {
            char *temp2 = (char *) input;
            double conv = 1.0 / 8388608.0;
            for (int i = 0; i < BUFF_SIZE; i++) {
                int value = *((int*) temp2);
                value = value & 0x00ffffff;             // take 24 bits
                value |= (value & 0x00800000) ? 0xff000000 : 0; // extend minus sign bits
                output[i] = (float) (value * conv);
                temp2 += 3;
            }
            break;
        }
        case 4: {
            int *temp2 = (int *) input;
            double conv = 1.0 / 2147483648.0;
            assert(sizeof(int) == 4);
            for (int i = 0; i < BUFF_SIZE; i++) {
                int value = temp2[i];
                output[i] = (float) (value * conv);
            }
            break;
        }
	}
}
inline int saturate(float fvalue, float minval, float maxval) {
	if (fvalue > maxval) {
		fvalue = maxval;
	} else if (fvalue < minval) {
		fvalue = minval;
	}
	return (int) fvalue;
}
