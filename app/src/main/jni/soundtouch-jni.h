#include <jni.h>
#include <android/log.h>
#include <queue>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include "soundtouch/include/SoundTouch.h"
#include "soundtouch_ST.h"

using namespace soundtouch;
using namespace std;

#define LOGV(...)   __android_log_print((int)ANDROID_LOG_INFO, "SOUNDTOUCH", __VA_ARGS__)
#define DLL_PUBLIC __attribute__ ((visibility ("default")))

const int MAX_TRACKS = 16;

class SoundTouchStream: public SoundTouch {

private:
	queue<jbyte>* byteBufferOut;
	int sampleRate;
	int bytesPerSample;




public:

	queue<jbyte>* getStream();
	int getSampleRate();
	int getBytesPerSample();
	void setSampleRate(int sampleRate);
	void setBytesPerSample(int bytesPerSample);
	uint getChannels();
	SoundTouchStream();
	SoundTouchStream(const SoundTouchStream& other);

	
};

void* getConvBuffer(int);
int write(const float*, queue<jbyte>*, int, int);
void setup(SoundTouchStream&, int, int, int, float, float);
void convertInput(jbyte*, float*, int, int);
inline int saturate(float, float, float);
void* getConvBuffer(int);
int process(SoundTouchStream&, SAMPLETYPE*, queue<jbyte>*, int, bool);
void setPitchSemi(SoundTouchStream&, float);
void setTempo(SoundTouchStream&, float);
void setRate(SoundTouchStream&, float);
void setTempoChange(SoundTouchStream&, float);
void setRateChange(SoundTouchStream&, float);
int copyBytes(jbyte*, queue<jbyte>*, int);

