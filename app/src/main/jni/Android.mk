# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# $Id: Android.mk 165 2012-12-28 19:55:23Z oparviai $

# LOCAL_PATH contains the "path to the Android.mk make file", in most case
# it would be present working directory.
# my-dir', provided by the build system, is used to return the path of the current directory
# (i.e. the directory containing the Android.mk file itself).
# $(call my-dir) is calling macro function "my-dir", defined in /build/core/definitions.mk,
# you can check the definition of the macro, It finds the directory for ndk-build script to start working.

# The first one (LOCAL_PATH:= $(call my-dir)) retrieves the current local path of the Android.mk file,
# so that all the variables in the same file can generate absolute path based on this local path.

LOCAL_PATH := $(call my-dir)
APP_STL := stlport_shared

# this clear_Vars clears all the local variables Local_xx
# except local path

include $(CLEAR_VARS)


MY_SOURCE_DIR:=soundtouch

# Local_Module is the module I want to build, this will automatically build
#libsoundtouch-jni.so
LOCAL_MODULE:=soundtouch-jni
LOCAL_C_INCLUDES:=$(LOCAL_PATH)/soundtouch/include
LOCAL_SRC_FILES:=  soundtouch_ST.cpp\
                   soundtouch-jni.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/AAFilter.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/FIFOSampleBuffer.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/FIRFilter.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/cpu_detect_x86.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/RateTransposer.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/SoundTouch.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/TDStretch.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/BPMDetect.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/PeakFinder.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/InterpolateLinear.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/InterpolateShannon.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/InterpolateCubic.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/mmx_optimized.cpp\
                   $(MY_SOURCE_DIR)/source/SoundTouch/sse_optimized.cpp


LOCAL_MULTILIB := 32
LOCAL_ARM_MODE:=arm

ldLibs += "log"
ldLibs += "android"
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog



#TARGET_ABI := android-22-arm64-v8a
#TARGET_ABI := armeabi


#-I option is to specify an alternate include directory

LOCAL_CFLAGS += -Wall -fvisibility=hidden -I soundtouch/source/../include -D ST_NO_EXCEPTION_HANDLING -fdata-sections -ffunction-sections

# this variable (Build_Shared) will point to the GNU Makeshift
#  Makefile script that collects all the information you defined
# in LOCAL_XXX variables since the most recent include
include $(BUILD_SHARED_LIBRARY)
